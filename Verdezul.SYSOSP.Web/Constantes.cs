﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using Verdezul.SYSOSP.Web.Entities;

namespace Verdezul.SYSOSP.Web
{
    public static class Constantes
    {
        public const string ROL_ADMIN = "Administrador";
        public const string ROL_GESTI = "Gestión_Administrativa";
        public const string ROL_SECRE = "Secretaría_OSP";
        public const string ROL_CALID = "Responsable_Calidad";
        public const string ROL_AREAS = "Area";
        public const string ROL_FINAN = "Financiero";
        public const string ROL_VISUA = "Visualización";
        public const string ROL_GEREN = "Gerencial";
        public const string USR_SUPAD = "verdezul";
        public const string CAR_ARCHI = "~/Uploads";
        public const string CAR_LOGS = "~/Log";
        public const string CAR_REP_INT = @"Reportes\Listados\";
        public const string CAR_REP_PRO = @"Reportes\Proformas\";
        public const string CAR_REP_IMP = @"Reportes\Impresiones\";
        public const string ROL_ELIM_ARCHIVOS = ROL_GESTI + "," + ROL_SECRE + "," + ROL_AREAS;
        public static TimeZoneInfo TimeZoneEcuador = TimeZoneInfo.FindSystemTimeZoneById("SA Pacific Standard Time");
        public const string SiteName_Pro = "sysosp-001-site1";
        public const string SiteName_Test = "sysosp-001-site2";
        public const string SiteName_Des = "Verdezul.SYSOSP.Web";
        public static readonly IList<string> ROLLIST = new ReadOnlyCollection<string>(new List<string> { ROL_ADMIN, ROL_GESTI, ROL_SECRE, ROL_CALID, ROL_AREAS, ROL_FINAN });
        public static readonly List<EstadoPedido> ListaEstadosPedidoNoCerrados = new List<EstadoPedido>
        {
            EstadoPedido.Ingresado,
            EstadoPedido.Muestras,
            EstadoPedido.Parametrizado,
            EstadoPedido.Tiempos,
            EstadoPedido.Aprobado,
            EstadoPedido.Aceptado,
            EstadoPedido.Pagado,
            EstadoPedido.Entregado
        };
        public static readonly List<EstadoPedido> ListaEstadosPedidoCerrados = new List<EstadoPedido>
        {
            EstadoPedido.Cerrado
        };
        public static readonly List<EstadoPedido> ListaEstadosNotificables = new List<EstadoPedido>
        {
            EstadoPedido.Aprobado,
            EstadoPedido.Pagado
        };
        public const int CANTIDAD_MUESTRAS = 10000;
        public const string TextoTodos = "(Todos)";
    }
}

﻿using Microsoft.AspNet.Identity.EntityFramework;
using System.Data.Entity;
using Verdezul.SYSOSP.Web.Entities;

namespace Verdezul.SYSOSP.Web.DAL
{
    public class ApplicationDbContext : IdentityDbContext<ApplicationUser>
    {
        public ApplicationDbContext() : base(Web.Environment.Database.GetConnectionTypeByServer(), throwIfV1Schema: false) //Desarrollo //Testing //Produccion
        {

        }

        public static ApplicationDbContext Create()
        {
            return new ApplicationDbContext();
        }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
        }

        public DbSet<Cliente> Clientes { get; set; }
        public DbSet<Pedido> Pedidos { get; set; }
        public DbSet<OrdenTrabajo> OrdenesTrabajo { get; set; }
        public DbSet<PagoContrato> PagoContratos { get; set; }

        public DbSet<Bioquimico> Bioquimicos { get; set; }
        public DbSet<Area> Areas { get; set; }
        public DbSet<Parametro> Parametros { get; set; }

        public DbSet<GrupoAnalisis> GruposAnalisis { get; set; }
        public DbSet<Analisis> Analisis { get; set; }
        public DbSet<SubAnalisis> SubAnalisis { get; set; }
        public DbSet<AnalisisMuestra> AnalisisMuestras { get; set; }
        public DbSet<Muestra> Muestras { get; set; }
        public DbSet<FechaPedido> FechasPedido { get; set; }
        public DbSet<Archivo> Archivos { get; set; }
        public DbSet<LogDB> LogDBs { get; set; }
        public DbSet<EnvioCorreo> EnviosCorreo { get; set; }
        public DbSet<Plantilla> Plantillas { get; set; }
        public DbSet<DetallePlantilla> DetallesPlantilla { get; set; }
        public DbSet<VistaSeguimientoInforme> SeguimientosInformes { get; set; }
    }
}
﻿using System;
using Verdezul.SYSOSP.Web.Entities;
using Verdezul.SYSOSP.Web.Log;

namespace Verdezul.SYSOSP.Web.DAL
{
    public class EnvioCorreoDAL
    {
        protected static ApplicationDbContext db = new ApplicationDbContext();

        public static void GrabarEnvioCorreo(TipoNotificacion tipoNotificacion, int? IdPedido, int? IdArchivo, string nombre, string correoElectronico)
        {
            try
            {
                var envio = new EnvioCorreo
                {
                    Direccion = correoElectronico,
                    Destinatario = nombre,
                    TipoNotificacion = tipoNotificacion,
                    PedidoID = IdPedido,
                    ArchivoID = IdArchivo
                };

                db.EnviosCorreo.Add(envio);
                db.SaveChanges();
            }
            catch (Exception ex)
            {
                SYSOSPLog.Grabar(new EnvioCorreoDAL(), ex);
            }
        }
    }
}
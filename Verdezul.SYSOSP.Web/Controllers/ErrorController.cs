﻿using System.Web.Mvc;

namespace Verdezul.SYSOSP.Web.Controllers
{
    public class ErrorController : Controller
    {
        public ActionResult Error()
        {
            return View();
        }

        public ActionResult Error404()
        {
            return View();
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Verdezul.SYSOSP.Web.DAL;
using Verdezul.SYSOSP.Web.Entities;
using Verdezul.SYSOSP.Web.Helpers;
using Verdezul.SYSOSP.Web.Log;
using Verdezul.SYSOSP.Web.Models;

namespace Verdezul.SYSOSP.Web.Controllers
{
    public class ResultadoController : Controller
    {
        private readonly ApplicationDbContext db = new ApplicationDbContext();

        public ActionResult Index(String i)
        {
            if (i == null)
                throw new HttpException(404, "No existe Pedido");

            Pedido pedido = db.Pedidos
                .Include("Cliente")
                .Include("Muestras")
                .Include("Ordenes")
                .Include("Ordenes.Area")
                .SingleOrDefault(x => x.Codigo.ToString() == i);

            if (pedido == null)
                throw new HttpException(404, "No existe Pedido");

            List<TipoArchivo> tipos = new List<TipoArchivo>();
            if (pedido.Estado == EstadoPedido.Aprobado)
                tipos.Add(TipoArchivo.Proforma);
            else
                tipos.Add(TipoArchivo.AceptacionCliente);
            tipos.Add(TipoArchivo.RecepcionMuestra);
            tipos.Add(TipoArchivo.InformeResultados);
            tipos.Add(TipoArchivo.InformeSuplementario);

            pedido.Archivos = pedido.Archivos.ToList().Where(x => tipos.Contains(x.Tipo)).ToList();

            ResultadosViewModel model = new ResultadosViewModel
            {
                Pedido = pedido
            };

            return View("Index", model);
        }

        public ActionResult Descargar(String i)
        {
            if (i == null)
                throw new HttpException(404, "No existe Archivo");

            var s = i.Split('_');

            if (s.Count() != 2)
                throw new HttpException(404, "No existe Archivo");

            var CodigoPedido = s[0];
            var CodigoArchivo = s[1];

            Pedido pedido = db.Pedidos.Include("Archivos").SingleOrDefault(x => x.Codigo.ToString() == CodigoPedido);
            if (pedido == null)
                throw new HttpException(404, "No existe Archivo");

            Archivo archivo = pedido.Archivos.SingleOrDefault(x => x.Codigo.ToString() == CodigoArchivo);
            if (archivo == null)
                throw new HttpException(404, "No existe Archivo");

            if (!archivo.Subido)
                throw new HttpException(404, "No existe Archivo");

            FileInfo fileInfo = new FileInfo(Server.MapPath($"{Constantes.CAR_ARCHI}/{archivo.NombreArchivo}"));

            var referencia = "";
            switch(archivo.Tipo)
            {
                case TipoArchivo.Proforma:
                case TipoArchivo.AceptacionCliente:
                    referencia = pedido.NumeroProforma;
                    break;
                case TipoArchivo.OrdenTrabajo:
                case TipoArchivo.RecepcionMuestra:
                case TipoArchivo.InformeResultados:
                    referencia = pedido.NumeroPedido;
                    break;
            }

            byte[] fileBytes = System.IO.File.ReadAllBytes(fileInfo.FullName);
            string fileName = $"{archivo.Tipo}_{referencia}_{DateTime.UtcNow.ToEcuadorDate():yyyyMMdd_hhmmss}{archivo.Extension}";
            return File(fileBytes, System.Net.Mime.MediaTypeNames.Application.Octet, fileName);
        }

        protected override void OnException(ExceptionContext filterContext)
        {
            if (filterContext == null)
                base.OnException(filterContext);

            SYSOSPLog.Grabar(this, filterContext.Exception);

            if (filterContext.HttpContext.IsCustomErrorEnabled)
            {
                filterContext.ExceptionHandled = true;
                HandleErrorInfo model = new HandleErrorInfo(filterContext.Exception, "Error", "Error");

                if (filterContext.Result is System.Web.Mvc.EmptyResult)
                {
                    this.View("Error", model).ExecuteResult(this.ControllerContext);
                }
            }
        }

    }
}
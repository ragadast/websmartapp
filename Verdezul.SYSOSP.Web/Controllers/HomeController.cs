﻿using System.Web.Mvc;

namespace Verdezul.SYSOSP.Web.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }

        public ActionResult FechaHora()
        {
            return View();
        }
    }
}
﻿namespace Verdezul.SYSOSP.Web.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddingIDAnteriorPedido : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Pedidos", "IDAnterior", c => c.Int());
        }
        
        public override void Down()
        {
            DropColumn("dbo.Pedidos", "IDAnterior");
        }
    }
}

namespace Verdezul.SYSOSP.Web.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class MuestraCantidadRequerida : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Muestras", "CantidadRequerida", c => c.String(maxLength: 256));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Muestras", "CantidadRequerida");
        }
    }
}

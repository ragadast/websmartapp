namespace Verdezul.SYSOSP.Web.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class PedidoValidezOferta : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Pedidos", "ValidezOferta", c => c.Int(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Pedidos", "ValidezOferta");
        }
    }
}

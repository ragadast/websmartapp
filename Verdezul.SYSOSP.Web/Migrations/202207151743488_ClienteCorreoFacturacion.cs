namespace Verdezul.SYSOSP.Web.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ClienteCorreoFacturacion : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Clientes", "CorreoElectronicoFacturacion", c => c.String(maxLength: 64));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Clientes", "CorreoElectronicoFacturacion");
        }
    }
}

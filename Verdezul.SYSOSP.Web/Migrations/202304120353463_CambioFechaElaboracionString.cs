namespace Verdezul.SYSOSP.Web.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class CambioFechaElaboracionString : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.Muestras", "FechaElaboracion", c => c.String());
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Muestras", "FechaElaboracion", c => c.DateTime());
        }
    }
}

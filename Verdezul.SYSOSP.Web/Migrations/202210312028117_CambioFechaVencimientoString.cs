namespace Verdezul.SYSOSP.Web.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class CambioFechaVencimientoString : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.Muestras", "FechaVencimiento", c => c.String());
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Muestras", "FechaVencimiento", c => c.DateTime());
        }
    }
}

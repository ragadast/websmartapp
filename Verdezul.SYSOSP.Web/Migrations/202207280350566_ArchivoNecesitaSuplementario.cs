namespace Verdezul.SYSOSP.Web.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ArchivoNecesitaSuplementario : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Archivos", "NecesitaSuplementario", c => c.Boolean(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Archivos", "NecesitaSuplementario");
        }
    }
}

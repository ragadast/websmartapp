namespace Verdezul.SYSOSP.Web.Migrations
{
	using System;
	using System.Data.Entity.Migrations;

	public partial class CreacionVistaInforme : DbMigration
	{
		public override void Up()
		{
			Sql(@"
CREATE VIEW VistaSeguimientosInformes as 
select
distinct
	m.ID
	,p.ID IdProforma
	,c.ID IdCliente
	,p.Proforma
	,p.Numero
	,fpago.EntityDate FechaPago
	,c.NumeroIdentificacion ClienteIdentificacion
	,c.Nombre ClienteNombre
	,p.Estado EstadoPedido
	,m.EntityDate FechaRecepcionMuestra
	,a.Nombre NombreArea
	,ot.TiempoEntrega
	,m.Detalle NombreMuestra
	,m.Estado EstadoMuestra
	,ar.Subido ArchivoSubido
	,case when ar.Subido = 1 then ar.EntityDate end FechaCarga
	,ec.FechaNotificacion
from AnalisisMuestra am
inner join OrdenesTrabajo ot on am.OrdenTrabajoID = ot.ID
inner join Areas a on ot.AreaID = a.ID
inner join Muestras m on am.MuestraID = m.ID
inner join Pedidos p on m.PedidoID = p.ID
inner join Clientes c on p.ClienteID = c.ID
left join Archivos ar on ar.IdOrdenTrabajo = ot.ID and ar.IdMuestra = m.ID
left join (
	select ArchivoID, min(EntityDate) FechaNotificacion
	from EnviosCorreo 
	group by ArchivoID
) ec on ec.ArchivoID = ar.ID
left join FechasPedido fpago on fpago.Estado = 7 and fpago.PedidoID = p.ID
where fpago.EntityDate is not null
            ");
		}

		public override void Down()
		{
			Sql(@"
IF EXISTS (SELECT 1 FROM sysobjects WHERE name='VistaSeguimientosInformes') BEGIN
    DROP VIEW VistaSeguimientosInformes
END
            ");
		}
	}
}

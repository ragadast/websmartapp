namespace Verdezul.SYSOSP.Web.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class SYSOSP_Initial_Agosto : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Analisis",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        Nombre = c.String(nullable: false, maxLength: 128),
                        Tecnica = c.String(maxLength: 128),
                        Valor = c.Decimal(nullable: false, precision: 18, scale: 2),
                        TieneIVA = c.Boolean(nullable: false),
                        AcreditacionSAE = c.Boolean(nullable: false),
                        Monitoreo = c.Boolean(nullable: false),
                        TieneSubAnalisis = c.Boolean(nullable: false),
                        GrupoAnalisisID = c.Int(nullable: false),
                        EntityDate = c.DateTime(nullable: false),
                        Status = c.Int(nullable: false),
                        User = c.String(maxLength: 32),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.GruposAnalisis", t => t.GrupoAnalisisID, cascadeDelete: false)
                .Index(t => t.GrupoAnalisisID);
            
            CreateTable(
                "dbo.GruposAnalisis",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        Nombre = c.String(nullable: false, maxLength: 64),
                        AreaID = c.Int(nullable: false),
                        EntityDate = c.DateTime(nullable: false),
                        Status = c.Int(nullable: false),
                        User = c.String(maxLength: 32),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.Areas", t => t.AreaID, cascadeDelete: false)
                .Index(t => t.AreaID);
            
            CreateTable(
                "dbo.Areas",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        Nombre = c.String(nullable: false, maxLength: 32),
                        BioquimicoID = c.Int(nullable: false),
                        EntityDate = c.DateTime(nullable: false),
                        Status = c.Int(nullable: false),
                        User = c.String(maxLength: 32),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.Bioquimicos", t => t.BioquimicoID, cascadeDelete: false)
                .Index(t => t.BioquimicoID);
            
            CreateTable(
                "dbo.Bioquimicos",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        UserName = c.String(nullable: false, maxLength: 32),
                        EntityDate = c.DateTime(nullable: false),
                        Status = c.Int(nullable: false),
                        User = c.String(maxLength: 32),
                    })
                .PrimaryKey(t => t.ID);
            
            CreateTable(
                "dbo.OrdenesTrabajo",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        Numero = c.Int(nullable: false),
                        AreaID = c.Int(nullable: false),
                        BioquimicoID = c.Int(nullable: false),
                        PedidoID = c.Int(nullable: false),
                        TiempoEntrega = c.Int(nullable: false),
                        EntityDate = c.DateTime(nullable: false),
                        Status = c.Int(nullable: false),
                        User = c.String(maxLength: 32),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.Pedidos", t => t.PedidoID, cascadeDelete: false)
                .ForeignKey("dbo.Areas", t => t.AreaID, cascadeDelete: false)
                .ForeignKey("dbo.Bioquimicos", t => t.BioquimicoID, cascadeDelete: false)
                .Index(t => t.AreaID)
                .Index(t => t.BioquimicoID)
                .Index(t => t.PedidoID);
            
            CreateTable(
                "dbo.AnalisisMuestra",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        AnalisisID = c.Int(nullable: false),
                        MuestraID = c.Int(nullable: false),
                        OrdenTrabajoID = c.Int(nullable: false),
                        Cantidad = c.Int(nullable: false),
                        Nombre = c.String(nullable: false, maxLength: 128),
                        Tecnica = c.String(maxLength: 128),
                        Valor = c.Decimal(nullable: false, precision: 18, scale: 2),
                        TieneIVA = c.Boolean(nullable: false),
                        AcreditacionSAE = c.Boolean(nullable: false),
                        Monitoreo = c.Boolean(nullable: false),
                        EntityDate = c.DateTime(nullable: false),
                        Status = c.Int(nullable: false),
                        User = c.String(maxLength: 32),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.Analisis", t => t.AnalisisID, cascadeDelete: false)
                .ForeignKey("dbo.Muestras", t => t.MuestraID, cascadeDelete: false)
                .ForeignKey("dbo.OrdenesTrabajo", t => t.OrdenTrabajoID, cascadeDelete: false)
                .Index(t => t.AnalisisID)
                .Index(t => t.MuestraID)
                .Index(t => t.OrdenTrabajoID);
            
            CreateTable(
                "dbo.Muestras",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        Numero = c.Int(nullable: false),
                        Detalle = c.String(nullable: false, maxLength: 32),
                        Cantidad = c.Int(nullable: false),
                        PedidoID = c.Int(nullable: false),
                        CantidadInformes = c.Int(),
                        ObservacionInicial = c.String(maxLength: 64),
                        Tipo = c.String(maxLength: 64),
                        Descripcion = c.String(maxLength: 64),
                        Codigo = c.String(maxLength: 64),
                        Lote = c.String(maxLength: 64),
                        Contenido = c.String(maxLength: 64),
                        Naturaleza = c.String(maxLength: 64),
                        Observaciones = c.String(maxLength: 256),
                        Estado = c.Int(nullable: false),
                        FechaElaboracion = c.DateTime(),
                        FechaVencimiento = c.DateTime(),
                        EntityDate = c.DateTime(nullable: false),
                        Status = c.Int(nullable: false),
                        User = c.String(maxLength: 32),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.Pedidos", t => t.PedidoID, cascadeDelete: false)
                .Index(t => t.PedidoID);
            
            CreateTable(
                "dbo.Pedidos",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        ClienteID = c.Int(nullable: false),
                        Estado = c.Int(nullable: false),
                        Proforma = c.Int(nullable: false),
                        Numero = c.Int(),
                        PersonaContacto = c.String(maxLength: 64),
                        InformacionAdicional = c.String(maxLength: 32),
                        Codigo = c.Guid(nullable: false),
                        FormaPago = c.Int(nullable: false),
                        DocumentoRecaudacion = c.Int(nullable: false),
                        InformacionPago = c.String(),
                        PagoContratoID = c.Int(),
                        FechaRecaudacion = c.DateTime(),
                        Factura = c.String(maxLength: 32),
                        Comentario = c.String(),
                        IVA = c.Decimal(nullable: false, precision: 18, scale: 2),
                        EntityDate = c.DateTime(nullable: false),
                        Status = c.Int(nullable: false),
                        User = c.String(maxLength: 32),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.PagosContrato", t => t.PagoContratoID)
                .ForeignKey("dbo.Clientes", t => t.ClienteID, cascadeDelete: false)
                .Index(t => t.ClienteID)
                .Index(t => t.Proforma, unique: true)
                .Index(t => t.PagoContratoID);
            
            CreateTable(
                "dbo.Archivos",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        PedidoID = c.Int(nullable: false),
                        Codigo = c.Guid(nullable: false),
                        Tipo = c.Int(nullable: false),
                        IdOrdenTrabajo = c.Int(),
                        IdMuestra = c.Int(),
                        Subido = c.Boolean(nullable: false),
                        Extension = c.String(maxLength: 8),
                        Fecha = c.DateTime(),
                        EntityDate = c.DateTime(nullable: false),
                        Status = c.Int(nullable: false),
                        User = c.String(maxLength: 32),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.Pedidos", t => t.PedidoID, cascadeDelete: false)
                .Index(t => t.PedidoID);
            
            CreateTable(
                "dbo.Clientes",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        TipoIdentificacion = c.Int(nullable: false),
                        NumeroIdentificacion = c.String(nullable: false, maxLength: 13),
                        Nombre = c.String(nullable: false, maxLength: 64),
                        Direccion = c.String(nullable: false, maxLength: 128),
                        Telefono = c.String(nullable: false, maxLength: 64),
                        CorreoElectronico = c.String(nullable: false, maxLength: 64),
                        Estado = c.Int(nullable: false),
                        EntityDate = c.DateTime(nullable: false),
                        Status = c.Int(nullable: false),
                        User = c.String(maxLength: 32),
                    })
                .PrimaryKey(t => t.ID)
                .Index(t => t.NumeroIdentificacion, unique: true)
                .Index(t => t.Nombre, unique: true);
            
            CreateTable(
                "dbo.PagosContrato",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        Fecha = c.DateTime(nullable: false),
                        Valor = c.Decimal(nullable: false, precision: 18, scale: 2),
                        ClienteID = c.Int(nullable: false),
                        FormaPago = c.Int(nullable: false),
                        InformacionPago = c.String(nullable: false, maxLength: 16),
                        EntityDate = c.DateTime(nullable: false),
                        Status = c.Int(nullable: false),
                        User = c.String(maxLength: 32),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.Clientes", t => t.ClienteID, cascadeDelete: false)
                .Index(t => t.ClienteID);
            
            CreateTable(
                "dbo.FechasPedido",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        Estado = c.Int(nullable: false),
                        PedidoID = c.Int(nullable: false),
                        EntityDate = c.DateTime(nullable: false),
                        Status = c.Int(nullable: false),
                        User = c.String(maxLength: 32),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.Pedidos", t => t.PedidoID, cascadeDelete: false)
                .Index(t => new { t.Estado, t.PedidoID }, unique: true, name: "IX_FechasPedido");
            
            CreateTable(
                "dbo.SubAnalisisMuestra",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        Nombre = c.String(nullable: false, maxLength: 128),
                        AnalisisMuestraID = c.Int(nullable: false),
                        EntityDate = c.DateTime(nullable: false),
                        Status = c.Int(nullable: false),
                        User = c.String(maxLength: 32),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.AnalisisMuestra", t => t.AnalisisMuestraID, cascadeDelete: false)
                .Index(t => t.AnalisisMuestraID);
            
            CreateTable(
                "dbo.SubAnalisis",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        Nombre = c.String(nullable: false, maxLength: 128),
                        AnalisisID = c.Int(nullable: false),
                        EntityDate = c.DateTime(nullable: false),
                        Status = c.Int(nullable: false),
                        User = c.String(maxLength: 32),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.Analisis", t => t.AnalisisID, cascadeDelete: false)
                .Index(t => t.AnalisisID);
            
            CreateTable(
                "dbo.Parametros",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        Tipo = c.Int(nullable: false),
                        Valor = c.String(nullable: false, maxLength: 64),
                        TipoDato = c.Int(nullable: false),
                        EntityDate = c.DateTime(nullable: false),
                        Status = c.Int(nullable: false),
                        User = c.String(maxLength: 32),
                    })
                .PrimaryKey(t => t.ID);
            
            CreateTable(
                "dbo.AspNetRoles",
                c => new
                    {
                        Id = c.String(nullable: false, maxLength: 128),
                        Name = c.String(nullable: false, maxLength: 256),
                    })
                .PrimaryKey(t => t.Id)
                .Index(t => t.Name, unique: true, name: "RoleNameIndex");
            
            CreateTable(
                "dbo.AspNetUserRoles",
                c => new
                    {
                        UserId = c.String(nullable: false, maxLength: 128),
                        RoleId = c.String(nullable: false, maxLength: 128),
                    })
                .PrimaryKey(t => new { t.UserId, t.RoleId })
                .ForeignKey("dbo.AspNetRoles", t => t.RoleId, cascadeDelete: false)
                .ForeignKey("dbo.AspNetUsers", t => t.UserId, cascadeDelete: false)
                .Index(t => t.UserId)
                .Index(t => t.RoleId);
            
            CreateTable(
                "dbo.AspNetUsers",
                c => new
                    {
                        Id = c.String(nullable: false, maxLength: 128),
                        Cedula = c.String(nullable: false, maxLength: 10),
                        Nombres = c.String(nullable: false, maxLength: 32),
                        Apellidos = c.String(nullable: false, maxLength: 32),
                        Cargo = c.String(nullable: false, maxLength: 32),
                        Titulo = c.String(maxLength: 8),
                        Activo = c.Boolean(nullable: false),
                        Email = c.String(maxLength: 256),
                        EmailConfirmed = c.Boolean(nullable: false),
                        PasswordHash = c.String(),
                        SecurityStamp = c.String(),
                        PhoneNumber = c.String(),
                        PhoneNumberConfirmed = c.Boolean(nullable: false),
                        TwoFactorEnabled = c.Boolean(nullable: false),
                        LockoutEndDateUtc = c.DateTime(),
                        LockoutEnabled = c.Boolean(nullable: false),
                        AccessFailedCount = c.Int(nullable: false),
                        UserName = c.String(nullable: false, maxLength: 256),
                    })
                .PrimaryKey(t => t.Id)
                .Index(t => t.UserName, unique: true, name: "UserNameIndex");
            
            CreateTable(
                "dbo.AspNetUserClaims",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        UserId = c.String(nullable: false, maxLength: 128),
                        ClaimType = c.String(),
                        ClaimValue = c.String(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.AspNetUsers", t => t.UserId, cascadeDelete: false)
                .Index(t => t.UserId);
            
            CreateTable(
                "dbo.AspNetUserLogins",
                c => new
                    {
                        LoginProvider = c.String(nullable: false, maxLength: 128),
                        ProviderKey = c.String(nullable: false, maxLength: 128),
                        UserId = c.String(nullable: false, maxLength: 128),
                    })
                .PrimaryKey(t => new { t.LoginProvider, t.ProviderKey, t.UserId })
                .ForeignKey("dbo.AspNetUsers", t => t.UserId, cascadeDelete: false)
                .Index(t => t.UserId);

            Sql(@"
CREATE VIEW LogDB as 
select 0 as ID,				'' as Tabla,			0 as EntityID,	0 as PedidoID,	0 as OrdenTrabajoID,	0 as AnalisisMuestraID,	0 as ClienteID,			getdate() EntityDate,	1 Status,	'' as [User] union
select 10000000 + sm.ID,	'SubAnalisisMuestra' ,	sm.ID,			PedidoId,		OrdenTrabajoID,			AnalisisMuestraID,		null,					sm.EntityDate,			sm.Status,	sm.[User]	from SubAnalisisMuestra sm inner join AnalisisMuestra am on am.ID = sm.AnalisisMuestraID inner join OrdenesTrabajo ot on am.OrdenTrabajoID = ot.ID union
select 20000000 + am.ID,	'AnalisisMuestra',		am.ID,			PedidoId,		OrdenTrabajoID,			null,					null,					am.EntityDate,			am.Status,	am.[User]	from AnalisisMuestra am inner join OrdenesTrabajo ot on am.OrdenTrabajoID = ot.ID union
select 30000000 + ID,		'OrdenesTrabajo',		ot.ID,			PedidoId,		null,					null,					null,					ot.EntityDate,			ot.Status,	ot.[User]	from OrdenesTrabajo ot union
select 40000000 + ID,		'Archivos',				ar.ID,			PedidoId,		null,					null,					null,					ar.EntityDate,			ar.Status,	ar.[User]	from Archivos ar union
select 50000000 + ID,		'FechasPedido',			fp.ID,			PedidoId,		null,					null,					null,					fp.EntityDate,			fp.Status,	fp.[User]	from FechasPedido fp union
select 60000000 + ID,		'Muestras',				mu.ID,			PedidoId,		null,					null,					null,					mu.EntityDate,			mu.Status,	mu.[User]	from Muestras mu union
select 70000000 + ID,		'Pedidos',				pe.ID,			pe.ID,			null,					null,					ClienteID,				pe.EntityDate,			pe.Status,	pe.[User]	from Pedidos pe union
select 80000000 + ID,		'EnviosCorreo',			ID,				PedidoId,		null,					null,					null,					EntityDate,				Status,		[User]		from EnviosCorreo union

select 90000000 + ID,		'Clientes',				cl.ID,			null,			null,					null,					cl.ID,					cl.EntityDate,			cl.Status,	cl.[User]	from Clientes cl union
select 100000000 + ID,		'PagosContrato',		pg.ID,			null,			null,					null,					ClienteID,				pg.EntityDate,			pg.Status,	pg.[User]	from PagosContrato pg union

select 110000000 + ID,		'Parametros',			ID,				null,			null,					null,					null,					EntityDate,				Status,		[User]		from Parametros union
select 120000000 + ID,		'Bioquimicos',			ID,				null,			null,					null,					null,					EntityDate,				Status,		[User]		from Bioquimicos union
select 130000000 + ID,		'Areas',				ID,				null,			null,					null,					null,					EntityDate,				Status,		[User]		from Areas union
select 140000000 + ID,		'GruposAnalisis',		ID,				null,			null,					null,					null,					EntityDate,				Status,		[User]		from GruposAnalisis union
select 150000000 + ID,		'Analisis',				ID,				null,			null,					null,					null,					EntityDate,				Status,		[User]		from Analisis union
select 160000000 + ID,		'SubAnalisis',			ID,				null,			null,					null,					null,					EntityDate,				Status,		[User]		from SubAnalisis union
select 170000000 + ID,		'Plantillas',			ID,				null,			null,					null,					null,					EntityDate,				Status,		[User]		from Plantillas union
select 180000000 + ID,		'DetallesPlantilla',	ID,				null,			null,					null,					null,					EntityDate,				Status,		[User]		from DetallesPlantilla 
            ");
        }

        public override void Down()
        {
            Sql(@"
IF EXISTS (SELECT 1 FROM sysobjects WHERE name='LogDB') BEGIN
    DROP VIEW LogDB
END
            ");
            DropForeignKey("dbo.AspNetUserRoles", "UserId", "dbo.AspNetUsers");
            DropForeignKey("dbo.AspNetUserLogins", "UserId", "dbo.AspNetUsers");
            DropForeignKey("dbo.AspNetUserClaims", "UserId", "dbo.AspNetUsers");
            DropForeignKey("dbo.AspNetUserRoles", "RoleId", "dbo.AspNetRoles");
            DropForeignKey("dbo.SubAnalisis", "AnalisisID", "dbo.Analisis");
            DropForeignKey("dbo.GruposAnalisis", "AreaID", "dbo.Areas");
            DropForeignKey("dbo.OrdenesTrabajo", "BioquimicoID", "dbo.Bioquimicos");
            DropForeignKey("dbo.OrdenesTrabajo", "AreaID", "dbo.Areas");
            DropForeignKey("dbo.SubAnalisisMuestra", "AnalisisMuestraID", "dbo.AnalisisMuestra");
            DropForeignKey("dbo.AnalisisMuestra", "OrdenTrabajoID", "dbo.OrdenesTrabajo");
            DropForeignKey("dbo.OrdenesTrabajo", "PedidoID", "dbo.Pedidos");
            DropForeignKey("dbo.Muestras", "PedidoID", "dbo.Pedidos");
            DropForeignKey("dbo.FechasPedido", "PedidoID", "dbo.Pedidos");
            DropForeignKey("dbo.Pedidos", "ClienteID", "dbo.Clientes");
            DropForeignKey("dbo.Pedidos", "PagoContratoID", "dbo.PagosContrato");
            DropForeignKey("dbo.PagosContrato", "ClienteID", "dbo.Clientes");
            DropForeignKey("dbo.Archivos", "PedidoID", "dbo.Pedidos");
            DropForeignKey("dbo.AnalisisMuestra", "MuestraID", "dbo.Muestras");
            DropForeignKey("dbo.AnalisisMuestra", "AnalisisID", "dbo.Analisis");
            DropForeignKey("dbo.Areas", "BioquimicoID", "dbo.Bioquimicos");
            DropForeignKey("dbo.Analisis", "GrupoAnalisisID", "dbo.GruposAnalisis");
            DropIndex("dbo.AspNetUserLogins", new[] { "UserId" });
            DropIndex("dbo.AspNetUserClaims", new[] { "UserId" });
            DropIndex("dbo.AspNetUsers", "UserNameIndex");
            DropIndex("dbo.AspNetUserRoles", new[] { "RoleId" });
            DropIndex("dbo.AspNetUserRoles", new[] { "UserId" });
            DropIndex("dbo.AspNetRoles", "RoleNameIndex");
            DropIndex("dbo.SubAnalisis", new[] { "AnalisisID" });
            DropIndex("dbo.SubAnalisisMuestra", new[] { "AnalisisMuestraID" });
            DropIndex("dbo.FechasPedido", "IX_FechasPedido");
            DropIndex("dbo.PagosContrato", new[] { "ClienteID" });
            DropIndex("dbo.Clientes", new[] { "Nombre" });
            DropIndex("dbo.Clientes", new[] { "NumeroIdentificacion" });
            DropIndex("dbo.Archivos", new[] { "PedidoID" });
            DropIndex("dbo.Pedidos", new[] { "PagoContratoID" });
            DropIndex("dbo.Pedidos", new[] { "Proforma" });
            DropIndex("dbo.Pedidos", new[] { "ClienteID" });
            DropIndex("dbo.Muestras", new[] { "PedidoID" });
            DropIndex("dbo.AnalisisMuestra", new[] { "OrdenTrabajoID" });
            DropIndex("dbo.AnalisisMuestra", new[] { "MuestraID" });
            DropIndex("dbo.AnalisisMuestra", new[] { "AnalisisID" });
            DropIndex("dbo.OrdenesTrabajo", new[] { "PedidoID" });
            DropIndex("dbo.OrdenesTrabajo", new[] { "BioquimicoID" });
            DropIndex("dbo.OrdenesTrabajo", new[] { "AreaID" });
            DropIndex("dbo.Areas", new[] { "BioquimicoID" });
            DropIndex("dbo.GruposAnalisis", new[] { "AreaID" });
            DropIndex("dbo.Analisis", new[] { "GrupoAnalisisID" });
            DropTable("dbo.AspNetUserLogins");
            DropTable("dbo.AspNetUserClaims");
            DropTable("dbo.AspNetUsers");
            DropTable("dbo.AspNetUserRoles");
            DropTable("dbo.AspNetRoles");
            DropTable("dbo.Parametros");
            DropTable("dbo.LogDB");
            DropTable("dbo.SubAnalisis");
            DropTable("dbo.SubAnalisisMuestra");
            DropTable("dbo.FechasPedido");
            DropTable("dbo.PagosContrato");
            DropTable("dbo.Clientes");
            DropTable("dbo.Archivos");
            DropTable("dbo.Pedidos");
            DropTable("dbo.Muestras");
            DropTable("dbo.AnalisisMuestra");
            DropTable("dbo.OrdenesTrabajo");
            DropTable("dbo.Bioquimicos");
            DropTable("dbo.Areas");
            DropTable("dbo.GruposAnalisis");
            DropTable("dbo.Analisis");
        }
    }
}

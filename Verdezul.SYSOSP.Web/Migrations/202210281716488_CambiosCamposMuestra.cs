namespace Verdezul.SYSOSP.Web.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class CambiosCamposMuestra : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.Muestras", "Detalle", c => c.String(nullable: false, maxLength: 150));
            AlterColumn("dbo.Muestras", "CantidadRequerida", c => c.String(maxLength: 150));
            AlterColumn("dbo.Muestras", "ObservacionInicial", c => c.String(maxLength: 150));
            DropColumn("dbo.Muestras", "Tipo");
            DropColumn("dbo.Muestras", "Naturaleza");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Muestras", "Naturaleza", c => c.String(maxLength: 64));
            AddColumn("dbo.Muestras", "Tipo", c => c.String(maxLength: 64));
            AlterColumn("dbo.Muestras", "ObservacionInicial", c => c.String(maxLength: 64));
            AlterColumn("dbo.Muestras", "CantidadRequerida", c => c.String(maxLength: 256));
            AlterColumn("dbo.Muestras", "Detalle", c => c.String(nullable: false, maxLength: 32));
        }
    }
}

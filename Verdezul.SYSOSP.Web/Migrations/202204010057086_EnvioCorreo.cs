namespace Verdezul.SYSOSP.Web.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class EnvioCorreo : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.EnviosCorreo",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        TipoNotificacion = c.Int(nullable: false),
                        Direccion = c.String(),
                        Destinatario = c.String(),
                        PedidoID = c.Int(),
                        EntityDate = c.DateTime(nullable: false),
                        Status = c.Int(nullable: false),
                        User = c.String(maxLength: 32),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.Pedidos", t => t.PedidoID)
                .Index(t => t.PedidoID);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.EnviosCorreo", "PedidoID", "dbo.Pedidos");
            DropIndex("dbo.EnviosCorreo", new[] { "PedidoID" });
            DropTable("dbo.EnviosCorreo");
        }
    }
}

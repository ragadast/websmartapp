namespace Verdezul.SYSOSP.Web.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class EnviosCorreoConArchivo : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.EnviosCorreo", "ArchivoID", c => c.Int());
            CreateIndex("dbo.EnviosCorreo", "ArchivoID");
            AddForeignKey("dbo.EnviosCorreo", "ArchivoID", "dbo.Archivos", "ID");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.EnviosCorreo", "ArchivoID", "dbo.Archivos");
            DropIndex("dbo.EnviosCorreo", new[] { "ArchivoID" });
            DropColumn("dbo.EnviosCorreo", "ArchivoID");
        }
    }
}

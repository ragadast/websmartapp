namespace Verdezul.SYSOSP.Web.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class FechasMuestrasStringMaximo50 : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.Muestras", "FechaElaboracion", c => c.String(maxLength: 64));
            AlterColumn("dbo.Muestras", "FechaVencimiento", c => c.String(maxLength: 64));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Muestras", "FechaVencimiento", c => c.String());
            AlterColumn("dbo.Muestras", "FechaElaboracion", c => c.String());
        }
    }
}

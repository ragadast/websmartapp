namespace Verdezul.SYSOSP.Web.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class PlantillaAnalisis : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.DetallesPlantilla",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        PlantillaID = c.Int(nullable: false),
                        AnalisisID = c.Int(nullable: false),
                        EntityDate = c.DateTime(nullable: false),
                        Status = c.Int(nullable: false),
                        User = c.String(maxLength: 32),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.Analisis", t => t.AnalisisID, cascadeDelete: true)
                .ForeignKey("dbo.Plantillas", t => t.PlantillaID, cascadeDelete: true)
                .Index(t => t.PlantillaID)
                .Index(t => t.AnalisisID);
            
            CreateTable(
                "dbo.Plantillas",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        Nombre = c.String(nullable: false),
                        Activo = c.Boolean(nullable: false),
                        EntityDate = c.DateTime(nullable: false),
                        Status = c.Int(nullable: false),
                        User = c.String(maxLength: 32),
                    })
                .PrimaryKey(t => t.ID);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.DetallesPlantilla", "PlantillaID", "dbo.Plantillas");
            DropForeignKey("dbo.DetallesPlantilla", "AnalisisID", "dbo.Analisis");
            DropIndex("dbo.DetallesPlantilla", new[] { "AnalisisID" });
            DropIndex("dbo.DetallesPlantilla", new[] { "PlantillaID" });
            DropTable("dbo.Plantillas");
            DropTable("dbo.DetallesPlantilla");
        }
    }
}

namespace Verdezul.SYSOSP.Web.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class CantidadInformes : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.OrdenesTrabajo", "CantidadInformes", c => c.Int(nullable: false));
            AddColumn("dbo.Archivos", "NumeroInforme", c => c.Int());
            DropColumn("dbo.Muestras", "CantidadInformes");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Muestras", "CantidadInformes", c => c.Int());
            DropColumn("dbo.Archivos", "NumeroInforme");
            DropColumn("dbo.OrdenesTrabajo", "CantidadInformes");
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Verdezul.SYSOSP.Web.Models
{
    public class ForgotPasswordEmailModel
    {
        public string Usuario { get; set; }
        public string Link { get; set; }
        public DateTime Fecha { get; set; }
    }
}
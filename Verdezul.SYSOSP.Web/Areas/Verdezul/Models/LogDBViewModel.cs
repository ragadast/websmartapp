﻿using System;
using System.Collections.Generic;
using System.IO;
using Verdezul.SYSOSP.Web.Areas.Verdezul.Controllers;
using Verdezul.SYSOSP.Web.Entities;

namespace Verdezul.SYSOSP.Web.Areas.Verdezul.Models
{
    public class LogDBViewModel
    {
        public List<LogDB> Item { get; set; }
        public TipoMantenimiento TipoMantenimiento { get; set; }
        public TipoLogDB Tipo { get; set; }
        public String Tabla { get; set; }
        public int PedidoID { get; set; }
        public int ClienteID { get; set; }
    }

    public class LogErrorViewModel
    {
        public LogErrorViewModel()
        {
            Archivos = new List<string>();
            Renombramientos = new List<Renombramiento>();
        }
        
        public TipoLogError Tipo { get; set; }
        public List<string> Archivos { get; set; }

        public List<Renombramiento> Renombramientos { get; set; }
        public List<string> Lineas { get; set; }
        public string NombreArchivo { get; set; }
        public FileInfo FileInfo { get; set; }
    }

    public enum TipoLogDB
    {
        Tabla = 1,
        Pedido = 2,
        Cliente = 3
    }

    public enum TipoLogError
    {
        Lista = 1,
        Detalle = 2
    }
}

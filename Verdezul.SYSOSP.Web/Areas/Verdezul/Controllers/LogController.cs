﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Web.Mvc;
using Verdezul.SYSOSP.Web.Areas.Admin.Models;
using Verdezul.SYSOSP.Web.Areas.Verdezul.Models;
using Verdezul.SYSOSP.Web.DAL;
using Verdezul.SYSOSP.Web.Entities;
using Verdezul.SYSOSP.Web.Helpers;

namespace Verdezul.SYSOSP.Web.Areas.Verdezul.Controllers
{
    [Authorize(Users = Constantes.USR_SUPAD)]
    public class LogController : Controller
    {
        protected ApplicationDbContext db = new ApplicationDbContext();

        public ActionResult Index()
        {
            return RedirectToAction("DB");
        }

        public ActionResult DB()
        {
            var model = new LogDBViewModel
            {
                Tipo = TipoLogDB.Tabla,
                Item = new List<LogDB>()
            };
            return View("LogDB", model);
        }

        [HttpPost]
        public ActionResult DB(LogDBViewModel model)
        {
            List<LogDB> logs = new List<LogDB>();
            switch (model.Tipo)
            {
                case TipoLogDB.Tabla:
                    model.Item = db.LogDBs.Where(x => x.Tabla == model.Tabla).OrderBy(y => y.EntityDate).ToList();
                    break;
                case TipoLogDB.Pedido:
                    model.Item = db.LogDBs.Where(x => x.PedidoID == model.PedidoID).OrderBy(y => y.EntityDate).ToList();
                    break;
                case TipoLogDB.Cliente:
                    model.Item = db.LogDBs.Where(x => x.ClienteID == model.ClienteID).OrderBy(y => y.EntityDate).ToList();
                    break;
            }
            return View("LogDB", model);
        }

        public ActionResult Error()
        {
            string carpetaLog = Server.MapPath(Constantes.CAR_LOGS);

            LogErrorViewModel model = new LogErrorViewModel { 
                Archivos = Directory.GetFiles(carpetaLog).Select(x => x.Replace($"{carpetaLog}\\", "")).Where(y => !y.Contains("empty")).ToList(),
                Tipo = TipoLogError.Lista
            };
            return View("LogError", model);
        }

        public ActionResult ErrorDetail(string file)
        {
            string carpetaLog = Server.MapPath(Constantes.CAR_LOGS);

            LogErrorViewModel model = new LogErrorViewModel { 
                Lineas = System.IO.File.ReadAllLines($"{carpetaLog}/{file}").ToList(),
                NombreArchivo = file,
                Tipo = TipoLogError.Detalle
            };

            return View("LogError", model);
        }

        [HttpPost]
        public ActionResult LimpiarLogs()
        {
            string carpetaLog = Server.MapPath(Constantes.CAR_LOGS);

            foreach(var archivo in Directory.GetFiles(carpetaLog))
            {
                if (!archivo.Contains("empty"))
                    System.IO.File.Delete(archivo);
            }
            
            var result = 0;
            return Json(new { status = result });
        }

        public ActionResult Files()
        {
            string carpetaArchivos = Server.MapPath(Constantes.CAR_ARCHI);

            LogErrorViewModel model = new LogErrorViewModel
            {
                Archivos = Directory.GetFiles(carpetaArchivos).Where(p => !p.Contains("empty")).Select(x => x.Replace($"{carpetaArchivos}\\", "")).ToList(),
                Tipo = TipoLogError.Lista
            };
            return View("LogFiles", model);
        }

        public ActionResult FileDetail(string fileName)
        {
            string carpetaArchivos = Server.MapPath(Constantes.CAR_ARCHI);

            FileInfo fi = new FileInfo($"{carpetaArchivos}/{fileName}");

            LogErrorViewModel model = new LogErrorViewModel
            {
                FileInfo = fi,
                Tipo = TipoLogError.Detalle
            };
            return View("LogFiles", model);
        }

        public ActionResult FileDownload(string fileName)
        {

            FileInfo fileInfo = new FileInfo(Server.MapPath($"{Constantes.CAR_ARCHI}/{fileName}"));
            byte[] fileBytes = System.IO.File.ReadAllBytes(fileInfo.FullName);
            return File(fileBytes, System.Net.Mime.MediaTypeNames.Application.Octet, fileName);
        }

        public ActionResult View(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Area Area = db.Areas.Find(id);
            if (Area == null)
            {
                return HttpNotFound();
            }
            return View(Area);
        }

        public ActionResult TestCorreo()
        {
            var result = EmailHelper.SendMail(EmailHelper.PlantillaTest(), new MimeKit.MailboxAddress("Correo SYSOSP Test", ConfigurationManager.AppSettings["CorreoVerificacion"]), "Test");

            if (result != SendMailResult.Enviado)
                throw new SmtpException($"Testing {result}");
            else
            {
                EnvioCorreoDAL.GrabarEnvioCorreo(TipoNotificacion.Otros, null, null, "Correo SYSOSP Test", ConfigurationManager.AppSettings["CorreoVerificacion"]);
            }

            return View(new CorreoViewModel { MensajeProcedimiento = "Probando" });
        }
    }
}

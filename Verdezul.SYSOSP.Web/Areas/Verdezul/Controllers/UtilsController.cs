﻿using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web.Mvc;
using Verdezul.SYSOSP.Web.Areas.Admin.Models;
using Verdezul.SYSOSP.Web.Areas.Verdezul.Models;
using Verdezul.SYSOSP.Web.DAL;
using Verdezul.SYSOSP.Web.Entities;
using Verdezul.SYSOSP.Web.Helpers;
using WebGrease.Css.Extensions;

namespace Verdezul.SYSOSP.Web.Areas.Verdezul.Controllers
{
    public class Renombramiento
    {
        public string Carpeta { get; set; }
        public string Actual { get; set; }

        private string[] PartesActual { get { return Actual.Split('.'); } }

        public string NombreActual { get { return PartesActual[0]; } }
        public string ExtensionActual { get { return PartesActual[1]; } }
        public string Nuevo { get { return $"{NombreActual}-1.{ExtensionActual}"; } }
        public string CarpetaActual { get { return $"{Carpeta}\\{Actual}"; } }
        public string CarpetaNuevo { get { return $"{Carpeta}\\{Nuevo}"; } }

        public bool Existe { get { return File.Exists(CarpetaActual); } }
    }

    [Authorize(Users = Constantes.USR_SUPAD)]
    public class UtilsController : Controller
    {
        protected ApplicationDbContext db = new ApplicationDbContext();

        public ActionResult RenombrarArchivosR()
        {
            string carpetaArchivos = Server.MapPath(Constantes.CAR_ARCHI);

            var renombramientos = Directory.GetFiles(carpetaArchivos)
                                    .Select(x => x.Replace($"{carpetaArchivos}\\", ""))
                                    .Where(r => Regex.IsMatch(r, "^R-\\d{1,6}-\\d{1,6}-\\d{1,6}\\.[a-zA-Z]+$"))
                                    .Select(p => new Renombramiento { Actual = p, Carpeta = carpetaArchivos })
                                    .ToList();

            LogErrorViewModel model = new LogErrorViewModel
            {
                Renombramientos = renombramientos
            };

            return View("UtilsFiles", model);
        }

        [HttpPost]
        public ActionResult RenombrarArchivosR(FormCollection form)
        {
            string carpetaArchivos = Server.MapPath(Constantes.CAR_ARCHI);

            var renombramientos = Directory.GetFiles(carpetaArchivos)
                                    .Select(x => x.Replace($"{carpetaArchivos}\\", ""))
                                    .Where(p => p.StartsWith("R"))
                                    .Select(p => new Renombramiento { Actual = p, Carpeta = carpetaArchivos })
                                    .ToList();

            renombramientos.ForEach(x => System.IO.File.Move(x.CarpetaActual, x.CarpetaNuevo));

            LogErrorViewModel model = new LogErrorViewModel
            {
                Renombramientos = renombramientos
            };

            return View("UtilsFiles", model);
        }

        public ActionResult ArchivosFaltantes()
        {
            string carpetaArchivos = Server.MapPath(Constantes.CAR_ARCHI);

            var renombramientos = db.Archivos
                .Where(z => z.Subido).ToList()
                .Select(x => new Renombramiento { Actual = x.NombreArchivo, Carpeta = carpetaArchivos }).ToList();

            LogErrorViewModel model = new LogErrorViewModel
            {
                Renombramientos = renombramientos
            };

            return View("UtilsFaltantes", model);
        }

        [HttpPost]
        public ActionResult ArchivosFaltantes(FormCollection form)
        {
            string carpetaArchivos = Server.MapPath(Constantes.CAR_ARCHI);

            var archivosSupuestamenteSubidos = db.Archivos
                .Where(z => z.Subido).ToList();

            archivosSupuestamenteSubidos
                .Select(x => new Renombramiento { Actual = x.NombreArchivo, Carpeta = carpetaArchivos })
                .Where(x => !x.Existe)
                .ForEach(x => System.IO.File.Create(x.CarpetaActual).Dispose());

            var archivosExistentes = archivosSupuestamenteSubidos
                .Select(x => new Renombramiento { Actual = x.NombreArchivo, Carpeta = carpetaArchivos }).ToList();

            LogErrorViewModel model = new LogErrorViewModel
            {
                Renombramientos = archivosExistentes
            };

            return View("UtilsFaltantes", model);
        }

        public ActionResult Firmar()
        {
            int id = 69;

            var pedido = db.Pedidos
                           .Include("Cliente")
                           .Include("Archivos")
                           .Include("Fechas")
                           .Include("Ordenes.Area")
                           .Include("Ordenes.Bioquimico")
                           .Include("Ordenes.AnalisisMuestras.Muestra")
                           .Include("Ordenes.AnalisisMuestras.Analisis")
                           .Include("Ordenes.AnalisisMuestras.Analisis.GrupoAnalisis")
                           .Include("Ordenes.AnalisisMuestras.SubAnalisisMuestra")
                           .SingleOrDefault(x => x.ID == id);


            var model = new PedidoViewModel
            {
                TipoMantenimiento = TipoMantenimiento.Modificación,
                Item = pedido,
                //SubmitAction = "RecibirMuestras",
                UrlPedido = ""
            };

            return View("UtilsFirmar", model);
        }

        [HttpPost]
        public ActionResult Firmar(FormCollection form)
        {
            string source = "C:\\Users\\Danny Gutierrez\\source\\repos\\FirmaDigital\\FirmaDigital\\Ejemplos\\Afirmaciones.pdf";
            string target = "C:\\Users\\Danny Gutierrez\\source\\repos\\FirmaDigital\\FirmaDigital\\Ejemplos\\AfirmacionesFirmado.pdf";
            string pfxFileName = "C:\\Users\\Danny Gutierrez\\source\\repos\\FirmaDigital\\FirmaDigital\\Ejemplos\\995841901755621529830377841.pfx";
            string password = "piN19800319!";
            string reason = "Probando Firma";
            string location = "Quito, Ecuador";

            var apariencia = SigningPDF.CrearFirmaPDF(source, target, reason, location);
            SigningPDF.FirmarPDF(apariencia, pfxFileName, password);

            return View("UtilsFirmar");
        }
    }
}

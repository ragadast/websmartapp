﻿using System.Web.Mvc;

namespace Verdezul.SYSOSP.Web.Areas.Verdezul
{
    public class VerdezulAreaRegistration : AreaRegistration 
    {
        public override string AreaName 
        {
            get 
            {
                return "Verdezul";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context) 
        {
            //context.MapRoute(
            //    "Account",
            //    "Admin/Account/{action}/{id}",
            //    new { controller = "Account", action = "Index", area = "Admin", id = UrlParameter.Optional },
            //    new[] { "Verdezul.SYSOSP.Web.Areas.Admin.Controllers" }
            //    );

            context.MapRoute(
                "Verdezul_default",
                "Verdezul/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional },
                new[] { "Verdezul.SYSOSP.Web.Areas.Verdezul.Controllers" }
            );
        }
    }
}
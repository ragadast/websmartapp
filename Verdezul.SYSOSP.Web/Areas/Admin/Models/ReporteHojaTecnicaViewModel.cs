﻿using Verdezul.SYSOSP.Web.Entities;

namespace Verdezul.SYSOSP.Web.Areas.Admin.Models
{
    public class ReporteHojaTecnicaViewModel : ReporteViewModel
    {
        public Pedido Pedido { get; set; }
    }
}
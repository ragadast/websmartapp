﻿using System.Collections.Generic;
using Verdezul.SYSOSP.Web.Entities;

namespace Verdezul.SYSOSP.Web.Areas.Admin.Models
{
    public class UserViewModel<T>: GenericViewModel<T>
    {
        public Dictionary<string, string> Roles { get; set; }
    }

    public class UserListViewModel : UserViewModel<List<ApplicationUser>>
    {

    }

    public class UserViewModel : UserViewModel<ApplicationUser>
    {

    }
}
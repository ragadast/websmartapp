﻿using System;
using Verdezul.SYSOSP.Web.Entities;
using Verdezul.SYSOSP.Web.Helpers;

namespace Verdezul.SYSOSP.Web.Areas.Admin.Models
{
    public class ParametrosReporte
    {
        public string FechaInicialTexto { get; set; }
        public string FechaFinalTexto { get; set; }
        public DateTime FechaInicial { get { return FechaInicialTexto.Replace("/", "").TransformarFecha(true); } }
        public DateTime FechaFinal { get { return FechaFinalTexto.Replace("/", "").TransformarFecha(false); } }
        public int ClienteID { get; set; }
        public string NombreReferencialCliente { get; set; }
        public ReporteProforma ReporteProforma { get; set; }
        public string UserName { get; set; }
        public int CodigoArea { get; set; }
        public EstadoPedido EstadoPedido { get; set; }
    }
}
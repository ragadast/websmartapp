﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Verdezul.SYSOSP.Web.Areas.Admin.Models
{
    public class ExternalLoginConfirmationViewModel
    {
        [Required(ErrorMessage = "{0} es requerido")]
        [Display(Name = "Email")]
        public string Email { get; set; }
    }

    public class ExternalLoginListViewModel
    {
        public string ReturnUrl { get; set; }
    }

    public class SendCodeViewModel
    {
        public string SelectedProvider { get; set; }
        public ICollection<System.Web.Mvc.SelectListItem> Providers { get; set; }
        public string ReturnUrl { get; set; }
        public bool RememberMe { get; set; }
    }

    public class VerifyCodeViewModel
    {
        [Required]
        public string Provider { get; set; }

        [Required]
        [Display(Name = "Código")]
        public string Code { get; set; }
        public string ReturnUrl { get; set; }

        [Display(Name = "¿Recordar este navegador?")]
        public bool RememberBrowser { get; set; }

        public bool RememberMe { get; set; }
    }

    public class ForgotViewModel
    {
        [Required(ErrorMessage = "{0} es requerido")]
        [Display(Name = "Email")]
        public string Email { get; set; }
    }

    public class LoginViewModel
    {
        [Required(ErrorMessage = "{0} es requerido")]
        [Display(Name = "Usuario")]
        public string UserName { get; set; }

        [Required(ErrorMessage = "{0} es requerida")]
        [DataType(DataType.Password)]
        [Display(Name = "Contraseña")]
        public string Password { get; set; }

        [Display(Name = "¿Recordar?")]
        public bool RememberMe { get; set; }
    }

    public class RegisterViewModel
    {
        [Required(ErrorMessage = "{0} es requerido")]
        [Display(Name = "Usuario")]
        [StringLength(16, ErrorMessage = "{0} debe ser de al menos {2} caracteres", MinimumLength = 6)]
        public string UserName { get; set; }

        [Required(ErrorMessage = "{0} es requerido")]
        [EmailAddress(ErrorMessage = "El Email no tiene el formato correcto")]
        [Display(Name = "Email")]
        public string Email { get; set; }

        [Required(ErrorMessage = "{0} es requerida")]
        [StringLength(100, ErrorMessage = "{0} debe ser de al menos {2} caracteres", MinimumLength = 6)]
        [DataType(DataType.Password)]
        [Display(Name = "Contraseña")]
        public string Password { get; set; }

        [DataType(DataType.Password)]
        [Display(Name = "Confirmar Contraseña")]
        [Compare("Password", ErrorMessage = "La contraseña y su confirmacion no coinciden")]
        public string ConfirmPassword { get; set; }

        [Display(Name = "Cédula")]
        [Required(ErrorMessage = "{0} es requerido")]
        public string Cedula { get; set; }
        [Required(ErrorMessage = "{0} es requerido")]
        public string Nombres { get; set; }
        [Required(ErrorMessage = "{0} es requerido")]
        public string Apellidos { get; set; }
        [Required(ErrorMessage = "{0} es requerido")]
        public string Cargo { get; set; }
    }

    public class ResetPasswordViewModel
    {
        [Required(ErrorMessage = "{0} es requerido")]
        [Display(Name = "Usuario")]
        public string UserName { get; set; }

        [Required(ErrorMessage = "{0} es requerida")]
        [StringLength(100, ErrorMessage = "{0} debe ser de al menos {2} caracteres", MinimumLength = 6)]
        [DataType(DataType.Password)]
        [Display(Name = "Contraseña")]
        public string Password { get; set; }

        [DataType(DataType.Password)]
        [Display(Name = "Confirmar Contraseña")]
        [Compare("Password", ErrorMessage = "Contraseña y su confirmacion no coinciden")]
        public string ConfirmPassword { get; set; }

        public string Code { get; set; }
    }

    public class ForgotPasswordViewModel
    {
        [Required(ErrorMessage = "{0} es requerido")]
        [Display(Name = "Usuario")]
        public string UserName { get; set; }
    }
}

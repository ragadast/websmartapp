﻿using System.Collections.Generic;
using Verdezul.SYSOSP.Web.Entities;

namespace Verdezul.SYSOSP.Web.Areas.Admin.Models
{
    public class BioquimicoListViewModel : GenericViewModel<List<Bioquimico>>
    {

    }

    public class BioquimicoViewModel : GenericViewModel<Bioquimico>
    {
        public List<string> UsuariosArea { get; set; }
    }
}
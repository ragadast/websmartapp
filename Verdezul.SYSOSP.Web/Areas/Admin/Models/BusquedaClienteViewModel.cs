﻿using System.Collections.Generic;
using Verdezul.SYSOSP.Web.Entities;

namespace Verdezul.SYSOSP.Web.Areas.Admin.Models
{
    public class BusquedaClienteViewModel
    {
        public TipoResultadoBusqueda Tipo { get; set; }
        public List<Cliente> Lista { get; set; }
        public Cliente Item { get; set; }
        public bool ExistenMasResultados { get; set; }
    }

    public enum TipoResultadoBusqueda {
        Varios = 1,
        Unico = 2,
        Ninguno = 3,
    }
}
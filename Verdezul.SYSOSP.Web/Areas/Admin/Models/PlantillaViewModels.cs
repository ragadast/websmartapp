﻿using System.Collections.Generic;
using Verdezul.SYSOSP.Web.Entities;

namespace Verdezul.SYSOSP.Web.Areas.Admin.Models
{
    public class PlantillaListViewModel : GenericViewModel<List<Plantilla>>
    {

    }

    public class PlantillaViewModel : GenericViewModel<Plantilla>
    {
        public PlantillaViewModel()
        {
            Areas = new List<Area>();
            CatalogoAnalisis = new List<Catalogo>();
        }
        public List<Area> Areas { get; set; }
        public List<Catalogo> CatalogoAnalisis { get; set; }
    }
}
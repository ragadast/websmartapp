﻿using System.Collections.Generic;
using Verdezul.SYSOSP.Web.Entities;

namespace Verdezul.SYSOSP.Web.Areas.Admin.Models
{
    public class ParametroListViewModel : GenericViewModel<List<Parametro>>
    {

    }

    public class ParametroViewModel : GenericViewModel<Parametro>
    {

    }
}
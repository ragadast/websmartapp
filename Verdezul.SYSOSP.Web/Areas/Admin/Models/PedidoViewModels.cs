﻿using System;
using System.Collections.Generic;
using Verdezul.SYSOSP.Web.Entities;
using Verdezul.SYSOSP.Web.Helpers;

namespace Verdezul.SYSOSP.Web.Areas.Admin.Models
{
    public class PedidoListViewModel : GenericViewModel<List<Pedido>>
    {
        public PedidoListViewModel()
        {
            AreasxUsuario = new List<Area>();
        }

        public bool PuedeCrearPedido { get; set; }
        public TipoFiltro TipoFiltro { get; set; }
        public string InformacionFiltro { get; set; }
        public List<Area> AreasxUsuario { get; set; }
    }

    public class PedidoViewModel : GenericViewModel<Pedido>
    {
        public PedidoViewModel()
        {
            CatalogoAnalisis = new List<Catalogo>();
            Areas = new List<Area>();
            PagosContratos = new List<PagoContrato>();
            ListaPlantillas = new List<Plantilla>();
            ListaGrupos = new List<GrupoAnalisis>();
        }

        public List<Catalogo> CatalogoAnalisis { get; set; }
        public List<Area> Areas { get; set; }
        public List<PagoContrato> PagosContratos { get; set; }
        public List<Plantilla> ListaPlantillas { get; set; }
        public List<GrupoAnalisis> ListaGrupos { get; set; }
        public int PedidoIdDuplicado { get; set; }
        public bool MantenerSolicitud { get; set; }
        public string UrlPedido { get; set; }
    }

    public class PedidoAprobadoListViewModel : GenericViewModel<List<Pedido>>
    {
        public PedidoAprobadoListViewModel()
        {
            this.Item = new List<Pedido>();
        }
        public string FechaInicialTexto { get; set; }
        public string FechaFinalTexto { get; set; }
        public DateTime FechaInicial { get { return FechaInicialTexto.Replace("/", "").TransformarFecha(true); } }
        public DateTime FechaFinal { get { return FechaFinalTexto.Replace("/", "").TransformarFecha(false); } }
    }

    public enum TipoFiltro
    {
        Asignados = 1,
        Texto = 2,
        Fechas = 3,
        Todos = 4,
    }

    public class Catalogo
    {
        public int ID { get; set; }
        public string ObjetoJS { get; set; }
        public string Nombre { get; set; }
        public int? IDArea { get; set; }
        public DivisionCatalogo Division { get; set; }
    }

    public enum DivisionCatalogo
    {
        InicioArea = 1,
        InicioGrupo = 2,
        Analisis = 3,
        FinGrupo = 4,
        FinArea = 5,
    }
}
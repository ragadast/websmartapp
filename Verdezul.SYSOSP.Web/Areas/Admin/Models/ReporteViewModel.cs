﻿using Microsoft.Reporting.WebForms;
using System.Web.UI.WebControls;
using Verdezul.SYSOSP.Web.Entities;

namespace Verdezul.SYSOSP.Web.Areas.Admin.Models
{
    public class ReporteViewModel
    {
        public ReporteViewModel()
        {
            Visor = new ReportViewer();
            Visor.ProcessingMode = ProcessingMode.Local;
            Visor.LocalReport.EnableExternalImages = true;
            Visor.SizeToReportContent = true;
            Visor.Width = Unit.Percentage(900);
            Visor.Height = Unit.Percentage(900);
        }

        public ReportViewer Visor { get; set; }

        public string Titulo { get; set; }
    }
}
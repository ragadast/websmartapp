﻿using System.Collections.Generic;
using Verdezul.SYSOSP.Web.Entities;

namespace Verdezul.SYSOSP.Web.Areas.Admin.Models
{
    public class AnalisisListViewModel : GenericViewModel<List<Analisis>>
    {

    }

    public class AnalisisViewModel : GenericViewModel<Analisis>
    {
        public AnalisisViewModel()
        {
            Grupos = new List<GrupoAnalisis>();
        }

        public List<GrupoAnalisis> Grupos { get; set; }
    }
}
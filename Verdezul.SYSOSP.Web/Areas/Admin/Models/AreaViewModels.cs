﻿using System.Collections.Generic;
using Verdezul.SYSOSP.Web.Entities;

namespace Verdezul.SYSOSP.Web.Areas.Admin.Models
{
    public class AreaListViewModel : GenericViewModel<List<Area>>
    {

    }

    public class AreaViewModel : GenericViewModel<Area>
    {
        public AreaViewModel()
        {
            Bioquimicos = new Dictionary<int, string>();
        }

        public Dictionary<int, string> Bioquimicos { get; set; }
    }
}
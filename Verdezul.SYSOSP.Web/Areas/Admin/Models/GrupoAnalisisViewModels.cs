﻿using System.Collections.Generic;
using Verdezul.SYSOSP.Web.Entities;

namespace Verdezul.SYSOSP.Web.Areas.Admin.Models
{
    public class GrupoAnalisisListViewModel : GenericViewModel<List<GrupoAnalisis>>
    {

    }

    public class GrupoAnalisisViewModel : GenericViewModel<GrupoAnalisis>
    {
        public GrupoAnalisisViewModel()
        {
            Areas = new List<Area>();
        }

        public List<Area> Areas { get; set; }
    }
}
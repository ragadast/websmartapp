﻿using System.Collections.Generic;
using Verdezul.SYSOSP.Web.Entities;

namespace Verdezul.SYSOSP.Web.Areas.Admin.Models
{
    public class ResultadoViewModel
    {
        public string NombreArchivo { get; set; }
    }
}
﻿using System.Collections.Generic;
using Verdezul.SYSOSP.Web.Entities;

namespace Verdezul.SYSOSP.Web.Areas.Admin.Models
{
    public class ReporteInternoViewModel : ReporteViewModel
    {
        public ReporteInternoViewModel() : base()
        {
            Areas = new List<Area>();
            Usuarios = new List<ApplicationUser>();
            ListaReportes = new List<ReporteProforma>();
        }

        public bool Renderizar { get; set; }
        public ParametrosReporte Parametros { get; set; }
        public List<Area> Areas { get; set; }
        public List<ApplicationUser> Usuarios { get; set; }
        public List<EstadoPedido> EstadoPedidos { get; set; }
        public List<ReporteProforma> ListaReportes { get; set; }
    }
}
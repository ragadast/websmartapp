﻿using System.Collections.Generic;
using Verdezul.SYSOSP.Web.Entities;

namespace Verdezul.SYSOSP.Web.Areas.Admin.Models
{
    public class ClienteListViewModel : GenericViewModel<List<Cliente>>
    {

    }

    public class ClienteViewModel : GenericViewModel<Cliente>
    {

    }
}
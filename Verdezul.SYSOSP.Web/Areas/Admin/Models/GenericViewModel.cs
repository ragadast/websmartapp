﻿using Verdezul.SYSOSP.Web.Entities;

namespace Verdezul.SYSOSP.Web.Areas.Admin.Models
{
    public class GenericViewModel<T>
    {
        public T Item { get; set; }
        public TipoMantenimiento TipoMantenimiento { get; set; }
        public string SubmitAction { get; set; }
    }
}
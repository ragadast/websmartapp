﻿using System.Collections.Generic;
using Verdezul.SYSOSP.Web.Entities;

namespace Verdezul.SYSOSP.Web.Areas.Admin.Models
{
    public class CorreoViewModel
    {
        public CorreoViewModel()
        {
            Parametros = new List<Parametro>();
        }
        public string NombreCliente { get; set; }
        public string NombreArchivo { get; set; }
        public string UrlPedido { get; set; }
        public string MensajeProcedimiento { get; set; }
        public List<Parametro> Parametros { get; set; }
        public TipoNotificacion Tipo { get; set; }
    }
}
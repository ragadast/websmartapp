﻿using System.Collections.Generic;
using Verdezul.SYSOSP.Web.Entities;

namespace Verdezul.SYSOSP.Web.Areas.Admin.Models
{
    public class PagoContratoListViewModel : GenericViewModel<List<PagoContrato>>
    {

    }

    public class PagoContratoViewModel : GenericViewModel<PagoContrato>
    {

    }
}
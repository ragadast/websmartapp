﻿using Microsoft.Reporting.WebForms;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using Verdezul.SYSOSP.Web.Areas.Admin.Models;
using Verdezul.SYSOSP.Web.Entities;
using Verdezul.SYSOSP.Web.Helpers;

namespace Verdezul.SYSOSP.Web.Areas.Admin.Controllers
{
    [Authorize] // TODO: ver que roles se ponen
    public class ReportesController : AdminReportController<ReporteInternoViewModel>
    {
        public ReportesController()
        {
            ReportModel = new ReporteInternoViewModel();
            CarpetaReportes = Constantes.CAR_REP_INT;
        }

        #region ListadoProformas
        public ActionResult ListadoProformas()
        {
            InitReportModel(ReporteGeneral.ListadoProformas);

            ReportModel.EstadoPedidos = GetEstadosPedidoListado();

            return View(ReportModel);
        }

        [HttpPost]
        public ActionResult ListadoProformas(ParametrosReporte Parametros)
        {
            ReporteGeneral reporte = ReporteGeneral.ListadoProformas;

            List<ReportParameter> parametrosReporteRS = GetParametrosReporteBasicos();

            ReportModel.EstadoPedidos = GetEstadosPedidoListado();

            // Petición de Proformas
            var proformas = db.Pedidos
                .Where(x => x.Estado == Parametros.EstadoPedido)
                .Select(p => new
                {
                    p.ID,
                    p.Proforma,
                    p.Cliente,
                    NumeroMuestras = p.Muestras.Count,
                    FechaAsociada = p.Fechas.FirstOrDefault(y => y.Estado == Parametros.EstadoPedido),
                    p.Ordenes
                })
                .ToList()
                .Where(x => x.FechaAsociada.Fecha >= Parametros.FechaInicial && x.FechaAsociada.Fecha <= Parametros.FechaFinal)
                .Where(x => x.FechaAsociada.User == Parametros.UserName || Parametros.UserName == " ")
                .Where(x => x.Ordenes.Any(o => o.AreaID == Parametros.CodigoArea || Parametros.CodigoArea == 0))
                .Select(x => new
                {
                    x.ID,
                    Proforma = x.Proforma.ToString().Insert(4, "-"),
                    ClienteNombre = x.Cliente.Nombre,
                    ClienteIdentificacion = x.Cliente.NumeroIdentificacion,
                    NumeroOrdenes = x.Ordenes.Count,
                    x.NumeroMuestras,
                    //Usuario = x.FechaIngreso.User
                    x.FechaAsociada.Fecha
                })
                .OrderBy(x => x.Proforma)
                .ToList();

            // Peticion de Áreas y Usuarios
            var areas = db.Areas.Where(x => x.Status == EstadoEntidad.Activo).ToList();
            var nombreArea = (Parametros.CodigoArea == 0) ? "(Todas)" : areas.Single(x => x.ID == Parametros.CodigoArea).Nombre;
            var usuarios = db.Users.Where(x => x.Activo && x.UserName != Constantes.USR_SUPAD).ToList();
            var nombreUsuario = (Parametros.UserName == " ") ? "(Todos)" : usuarios.Single(x => x.UserName == Parametros.UserName).NombresCompletos;

            // Configuración de Parámetros
            var parametrosSistema = db.Parametros.ToList();
            parametrosSistema.Add(new Parametro(TipoParametroReporte.FechaInicial, Parametros.FechaInicialTexto));
            parametrosSistema.Add(new Parametro(TipoParametroReporte.FechaFinal, Parametros.FechaFinalTexto));
            parametrosSistema.Add(new Parametro { ID = 103, Valor = Parametros.EstadoPedido.ToDisplayName() });
            parametrosSistema.Add(new Parametro { ID = 104, Valor = nombreArea });
            parametrosSistema.Add(new Parametro { ID = 105, Valor = nombreUsuario });

            // Configuración de Reporte
            ReportModel.Visor.LocalReport.DataSources.Add(new ReportDataSource("PedidosDS", proformas));
            ReportModel.Visor.LocalReport.DataSources.Add(new ReportDataSource("ParametrosDS", parametrosSistema));

            SetReportModel(reporte, parametrosReporteRS, Parametros, areas, usuarios);

            return View(ReportModel);
        }
        #endregion

        #region ListadoRecepcionMuestras
        public ActionResult ListadoRecepcionMuestras()
        {
            InitReportModel(ReporteGeneral.ListadoRecepcionMuestras);

            return View(ReportModel);
        }

        [HttpPost]
        public ActionResult ListadoRecepcionMuestras(ParametrosReporte Parametros)
        {
            // Petición de Muestras
            var muestras = db.Muestras
                .Where(x => x.Estado == EstadoMuestra.Recibida)
                .Where(x => x.User == Parametros.UserName || Parametros.UserName == " ")
                .Select(x => new
                {
                    x.Detalle,
                    x.Numero,
                    x.Cantidad,
                    x.Pedido,
                    NumeroAnalisis = x.AnalisisMuestras.Count(),
                    Ordenes = x.AnalisisMuestras.Select(p => p.OrdenTrabajo).Distinct(),
                    FechaRecepcion = x.EntityDate,
                    UsuarioRecepcion = x.User
                })
                .ToList()
                .Where(y => y.FechaRecepcion >= Parametros.FechaInicial && y.FechaRecepcion <= Parametros.FechaFinal)
                .Where(x => x.Ordenes.Any(o => o.AreaID == Parametros.CodigoArea || Parametros.CodigoArea == 0))
                .Select(y => new
                {
                    Nombre = $"{y.Numero} - {y.Detalle}",
                    y.Cantidad,
                    y.Pedido.NumeroPedido,
                    y.NumeroAnalisis,
                    NumeroOrdenes = y.Ordenes.Count(),
                    UsuarioElaboracion = y.UsuarioRecepcion,
                    FechaElaboracion = y.FechaRecepcion
                })
                .OrderBy(c => c.NumeroPedido);

            // Peticion de Áreas y Usuarios
            var areas = db.Areas.Where(x => x.Status == EstadoEntidad.Activo).ToList();
            var nombreArea = (Parametros.CodigoArea == 0) ? "(Todas)" : areas.Single(x => x.ID == Parametros.CodigoArea).Nombre;
            var usuarios = db.Users.Where(x => x.Activo && x.UserName != Constantes.USR_SUPAD).ToList();
            var nombreUsuario = (Parametros.UserName == " ") ? "(Todos)" : usuarios.Single(x => x.UserName == Parametros.UserName).NombresCompletos;

            // Configuración de Parámetros
            var parametrosSistema = db.Parametros.ToList();
            parametrosSistema.Add(new Parametro(TipoParametroReporte.FechaInicial, Parametros.FechaInicialTexto));
            parametrosSistema.Add(new Parametro(TipoParametroReporte.FechaFinal, Parametros.FechaFinalTexto));
            parametrosSistema.Add(new Parametro { ID = 104, Valor = nombreArea });
            parametrosSistema.Add(new Parametro { ID = 105, Valor = nombreUsuario });

            ReportModel.Visor.LocalReport.DataSources.Add(new ReportDataSource("MuestrasDS", muestras));
            ReportModel.Visor.LocalReport.DataSources.Add(new ReportDataSource("ParametrosDS", parametrosSistema));

            List<ReportParameter> parametrosReporteRS = GetParametrosReporteBasicos();

            ReporteGeneral reporte = ReporteGeneral.ListadoRecepcionMuestras;

            SetReportModel(reporte, parametrosReporteRS, Parametros, areas, usuarios);

            return View(ReportModel);
        }
        #endregion

        #region ListadoPagos
        public ActionResult ListadoPagos()
        {
            InitReportModel(ReporteGeneral.ListadoPagos);

            return View(ReportModel);
        }

        [HttpPost]
        public ActionResult ListadoPagos(ParametrosReporte Parametros)
        {
            ReporteGeneral reporte = ReporteGeneral.ListadoPagos;

            List<ReportParameter> parametrosReporteRS = GetParametrosReporteBasicos();

            // Petición de Proformas
            var proformas = db.Pedidos
                .Where(x => x.Fechas.Any(y => y.Estado == EstadoPedido.Pagado))
                .Select(p => new
                {
                    p.ID,
                    p.Proforma, //TODO: Usar el nombre compuesto de la entidad
                    p.Cliente,
                    p.Ordenes,
                    p.DocumentoRecaudacion,
                    p.FormaPago,
                    p.InformacionPago,
                    FechaPago = p.Fechas.FirstOrDefault(y => y.Estado == EstadoPedido.Pagado),
                    ValorPago = p.Ordenes.Sum(d => d.AnalisisMuestras.Sum(r => r.Cantidad * r.Valor)),
                })
                .ToList()
                .Where(x => x.FechaPago.Fecha >= Parametros.FechaInicial && x.FechaPago.Fecha <= Parametros.FechaFinal)
                .Where(x => x.FechaPago.User == Parametros.UserName || Parametros.UserName == " ")
                .Where(x => x.Ordenes.Any(o => o.AreaID == Parametros.CodigoArea || Parametros.CodigoArea == 0))
                .Select(x => new
                {
                    x.ID,
                    Proforma = x.Proforma.ToString().Insert(4, "-"),
                    ClienteNombre = x.Cliente.Nombre,
                    Usuario = x.FechaPago.User,
                    FechaPago = x.FechaPago.Fecha,
                    x.DocumentoRecaudacion,
                    x.FormaPago,
                    x.InformacionPago,
                    x.ValorPago
                })
                .OrderBy(x => x.Proforma)
                .ToList();

            // Peticion de Áreas y Usuarios
            var areas = db.Areas.Where(x => x.Status == EstadoEntidad.Activo).ToList();
            var nombreArea = (Parametros.CodigoArea == 0) ? "(Todas)" : areas.Single(x => x.ID == Parametros.CodigoArea).Nombre;
            var usuarios = db.Users.Where(x => x.Activo && x.UserName != Constantes.USR_SUPAD).ToList();
            var nombreUsuario = (Parametros.UserName == " ") ? "(Todos)" : usuarios.Single(x => x.UserName == Parametros.UserName).NombresCompletos;

            // Configuración de Parámetros
            var parametrosSistema = db.Parametros.ToList();
            parametrosSistema.Add(new Parametro(TipoParametroReporte.FechaInicial, Parametros.FechaInicialTexto));
            parametrosSistema.Add(new Parametro(TipoParametroReporte.FechaFinal, Parametros.FechaFinalTexto));
            parametrosSistema.Add(new Parametro { ID = 104, Valor = nombreArea });
            parametrosSistema.Add(new Parametro { ID = 105, Valor = nombreUsuario });

            // Configuración de Reporte
            ReportModel.Visor.LocalReport.DataSources.Add(new ReportDataSource("PedidosDS", proformas));
            ReportModel.Visor.LocalReport.DataSources.Add(new ReportDataSource("ParametrosDS", parametrosSistema));
            ReportModel.Visor.LocalReport.DataSources.Add(new ReportDataSource("EnumeradosDS", Enumerado.GetEnumerados()));

            SetReportModel(reporte, parametrosReporteRS, Parametros, areas, usuarios);

            return View(ReportModel);
        }
        #endregion

        #region ListadoAnalisis
        public ActionResult ListadoAnalisis()
        {
            ReporteGeneral reporte = ReporteGeneral.CatalogoAnalisis;
            var archivoReporte = $@"{CarpetaReportes}{reporte}.rdlc";
            ReportModel.Titulo = reporte.ToDisplayName();
            ReportModel.Visor.LocalReport.ReportPath = Request.MapPath(Request.ApplicationPath) + archivoReporte;

            var parametrosSistema = db.Parametros.ToList();

            var analisis = db.Analisis
                .Where(x => x.Status == EstadoEntidad.Activo)
                .Select(x => new
                {
                    x.Tecnica,
                    x.Valor,
                    x.Nombre,
                    x.TieneIVA,
                    x.AcreditacionSAE,
                    x.Monitoreo,
                    x.SubAnalisis,
                    GrupoAnalisisID = x.GrupoAnalisis.ID,
                    NombreGrupo = x.GrupoAnalisis.Nombre,
                    AreaID = x.GrupoAnalisis.Area.ID,
                    NombreArea = x.GrupoAnalisis.Area.Nombre,
                })
                .OrderBy(y => y.AreaID).ThenBy(x => x.GrupoAnalisisID).ThenBy(x => x.Nombre)
                .ToList();

            var listaAnalisis = new List<AnalisisMuestraHelp>();

            int i = 0;
            var nombreGrupo = $"{analisis.First().NombreArea}-{analisis.First().NombreGrupo}";
            foreach (var x in analisis)
            {
                i = (nombreGrupo == $"{x.NombreArea}-{x.NombreGrupo}") ? i + 1 : 1;
                nombreGrupo = $"{x.NombreArea}-{x.NombreGrupo}";

                listaAnalisis.Add(new AnalisisMuestraHelp
                {
                    Orden = i,
                    Tecnica = x.Tecnica,
                    Valor = x.Valor,
                    Nombre = x.Nombre,
                    NombreGrupo = x.NombreGrupo,
                    GrupoAnalisisID = x.GrupoAnalisisID,
                    TieneIVA = x.TieneIVA,
                    AcreditacionSAE = x.AcreditacionSAE,
                    Monitoreo = x.Monitoreo,
                    NombreNivel = "Analisis",
                    NombreArea = x.NombreArea,
                    AreaID = x.AreaID
                });

                foreach (var subAnalisis in x.SubAnalisis)
                {
                    listaAnalisis.Add(new AnalisisMuestraHelp
                    {
                        Orden = i,
                        Tecnica = "",
                        Valor = 0m,
                        Nombre = subAnalisis.Nombre,
                        NombreGrupo = x.NombreGrupo,
                        GrupoAnalisisID = x.GrupoAnalisisID,
                        TieneIVA = false,
                        AcreditacionSAE = false,
                        Monitoreo = false,
                        NombreNivel = "SubAnalisis",
                        NombreArea = x.NombreArea,
                        AreaID = x.AreaID
                    });
                }
            }

            List<ReportParameter> parametrosReporteRS = GetParametrosReporteBasicos();

            ReportModel.Visor.LocalReport.DataSources.Add(new ReportDataSource("ParametrosDS", parametrosSistema));
            ReportModel.Visor.LocalReport.DataSources.Add(new ReportDataSource("AnalisisDS", listaAnalisis));
            ReportModel.Visor.LocalReport.SetParameters(parametrosReporteRS);

            return View(ReportModel);
        }
        #endregion

        #region ListadoClientes
        public ActionResult ListadoClientes()
        {
            ReporteGeneral reporte = ReporteGeneral.ListadoClientes;

            List<ReportParameter> parametrosReporteRS = GetParametrosReporteBasicos();

            // Petición de Proformas
            var clientes = db.Clientes.OrderBy(z => z.Nombre).ToList();
            // Configuración de Parámetros
            var parametrosSistema = db.Parametros.ToList();

            // Configuración de Reporte
            ReportModel.Visor.LocalReport.DataSources.Add(new ReportDataSource("ClientesDS", clientes));
            ReportModel.Visor.LocalReport.DataSources.Add(new ReportDataSource("ParametrosDS", parametrosSistema));
            ReportModel.Visor.LocalReport.DataSources.Add(new ReportDataSource("EnumeradosDS", Enumerado.GetEnumerados()));

            var archivoReporte = $@"{CarpetaReportes}{reporte}.rdlc";
            ReportModel.Titulo = reporte.ToDisplayName();
            ReportModel.Visor.LocalReport.ReportPath = Request.MapPath(Request.ApplicationPath) + archivoReporte;
            ReportModel.Visor.LocalReport.SetParameters(parametrosReporteRS);
            ReportModel.Renderizar = true;
            return View(ReportModel);
        }
        #endregion

        #region General
        private void InitReportModel(ReporteGeneral reporte)
        {
            DateTime fechaActualEC = TimeZoneExtensions.EcuadorNow;

            DateTime fechaInicial = new DateTime(fechaActualEC.Year, fechaActualEC.Month, 1);

            ReportModel.Titulo = reporte.ToDisplayName();
            ReportModel.Renderizar = false;
            ReportModel.Areas = db.Areas.Where(x => x.Status == EstadoEntidad.Activo).ToList();
            ReportModel.Usuarios = db.Users.Where(x => x.Activo && x.UserName != Constantes.USR_SUPAD).ToList();
            ReportModel.Parametros = new ParametrosReporte
            {
                FechaInicialTexto = fechaInicial.ToString(TipoSalida.Fecha),
                FechaFinalTexto = fechaActualEC.ToString(TipoSalida.Fecha),
            };
        }

        private void SetReportModel(ReporteGeneral reporte, List<ReportParameter> parametrosReporteRS, ParametrosReporte parametros,
            List<Area>areas, List<ApplicationUser> usuarios)
        {
            var archivoReporte = $@"{CarpetaReportes}{reporte}.rdlc";
            ReportModel.Titulo = reporte.ToDisplayName();
            ReportModel.Visor.LocalReport.ReportPath = Request.MapPath(Request.ApplicationPath) + archivoReporte;
            ReportModel.Visor.LocalReport.SetParameters(parametrosReporteRS);
            ReportModel.Renderizar = true;
            ReportModel.Areas = areas;
            ReportModel.Usuarios = usuarios;
            ReportModel.Parametros = parametros;
        }

        private List<EstadoPedido> GetEstadosPedidoListado()
        {
            List<EstadoPedido> estados = new List<EstadoPedido>
            {
                EstadoPedido.Aprobado,
                EstadoPedido.Aceptado,
                EstadoPedido.Pagado,
                EstadoPedido.Entregado
            };
            return estados;
        }
        #endregion
    }
}

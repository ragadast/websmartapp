﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data.Entity;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Verdezul.SYSOSP.Web.Areas.Admin.Models;
using Verdezul.SYSOSP.Web.DAL;
using Verdezul.SYSOSP.Web.Entities;
using Verdezul.SYSOSP.Web.Helpers;
using Verdezul.SYSOSP.Web.Log;

namespace Verdezul.SYSOSP.Web.Areas.Admin.Controllers
{
    [Authorize]
    public partial class PedidosController : AdminController<Pedido>
    {
        private readonly ICollection<ValidationResult> results = new List<ValidationResult>();

        #region General
        public ActionResult Index(string q)
        {
            PedidoListViewModel model = new PedidoListViewModel();
            model.PuedeCrearPedido = User.IsInRole(Constantes.ROL_GESTI);
            model.TipoFiltro = TipoFiltro.Asignados;

            if (User.IsInRole(Constantes.ROL_AREAS))
            {
                model.AreasxUsuario = db.Areas
                    .Include("Bioquimico")
                    .Where(x => x.Bioquimico.UserName == User.Identity.Name)
                    .ToList();
            }

            if (q == null)
                q = "1_nodata";

            var dataFilter = q.Split('_');

            if (dataFilter.Count() == 2 && Enum.IsDefined(typeof(TipoFiltro), Convert.ToInt32(dataFilter[0])))
            {
                model.TipoFiltro = (TipoFiltro)Convert.ToInt32(dataFilter[0]);
            }

            switch (model.TipoFiltro)
            {
                case TipoFiltro.Asignados:
                    model.Item = GetPedidos(TipoFiltro.Asignados, null, null, null, model.AreasxUsuario, TipoListadoPedido.NoCerrados);
                    model.TipoMantenimiento = TipoMantenimiento.ListadoAsignados;
                    break;
                case TipoFiltro.Texto:
                    model.Item = GetPedidos(TipoFiltro.Texto, dataFilter[1], null, null, null, TipoListadoPedido.NoCerrados);
                    model.InformacionFiltro = dataFilter[1];
                    model.TipoMantenimiento = TipoMantenimiento.Listado;
                    break;
                case TipoFiltro.Fechas:
                    try
                    {
                        var fechas = dataFilter[1].Split('-');
                        DateTime fecha1 = fechas[0].TransformarFecha(true);
                        DateTime fecha2 = fechas[1].TransformarFecha(false);
                        model.Item = GetPedidos(TipoFiltro.Fechas, null, fecha1, fecha2, null, TipoListadoPedido.NoCerrados);
                        model.InformacionFiltro = String.Format("{0:dd/MM/yyyy}_{1:dd/MM/yyyy}", fecha1.ToEcuadorDate(), fechas[1].TransformarFecha(true).ToEcuadorDate());
                        model.TipoMantenimiento = TipoMantenimiento.Listado;
                    }
                    catch (Exception ex)
                    {
                        SYSOSPLog.Grabar(this, ex);
                        model.Item = GetPedidos(TipoFiltro.Asignados, null, null, null, model.AreasxUsuario, TipoListadoPedido.NoCerrados);
                        model.TipoFiltro = TipoFiltro.Asignados;
                    }
                    break;
            }

            return View(model);
        }

        public ActionResult Cerrados(string q)
        {
            PedidoListViewModel model = new PedidoListViewModel();
            model.TipoFiltro = TipoFiltro.Todos;

            if (q == null)
                q = "4_nodata";

            var dataFilter = q.Split('_');

            if (dataFilter.Count() == 2 && Enum.IsDefined(typeof(TipoFiltro), Convert.ToInt32(dataFilter[0])))
            {
                model.TipoFiltro = (TipoFiltro)Convert.ToInt32(dataFilter[0]);
            }

            switch (model.TipoFiltro)
            {
                case TipoFiltro.Todos:
                    model.Item = GetPedidos(TipoFiltro.Todos, null, null, null, null, TipoListadoPedido.SoloCerrados);
                    model.TipoMantenimiento = TipoMantenimiento.Visualización;
                    break;
                case TipoFiltro.Texto:
                    model.Item = GetPedidos(TipoFiltro.Texto, dataFilter[1], null, null, null, TipoListadoPedido.SoloCerrados);
                    model.InformacionFiltro = dataFilter[1];
                    model.TipoMantenimiento = TipoMantenimiento.Listado;
                    break;
                case TipoFiltro.Fechas:
                    try
                    {
                        var fechas = dataFilter[1].Split('-');
                        DateTime fecha1 = fechas[0].TransformarFecha(true);
                        DateTime fecha2 = fechas[1].TransformarFecha(false);
                        model.Item = GetPedidos(TipoFiltro.Fechas, null, fecha1, fecha2, null, TipoListadoPedido.SoloCerrados);
                        model.InformacionFiltro = String.Format("{0:dd/MM/yyyy}_{1:dd/MM/yyyy}", fecha1.ToEcuadorDate(), fechas[1].TransformarFecha(true).ToEcuadorDate());
                        model.TipoMantenimiento = TipoMantenimiento.Listado;
                    }
                    catch (Exception ex)
                    {
                        SYSOSPLog.Grabar(this, ex);
                        model.Item = GetPedidos(TipoFiltro.Todos, null, null, null, null, TipoListadoPedido.SoloCerrados);
                        model.TipoFiltro = TipoFiltro.Asignados;
                    }
                    break;
            }

            return View(model);
        }

        public ActionResult NotificacionesPendientes()
        {
            List<EstadoPedido> listaEstadosRol = AgregarEstadosPorNotificacion();

            var pedidos = db.Pedidos
                    .Include("Cliente")
                    .Include("EnviosCorreo")
                    .Include("Archivos")
                    .Where(x => x.Status == EstadoEntidad.Activo)
                    .Where(x => listaEstadosRol.Contains(x.Estado))
                    .ToList()
                    .Where(x => x.NumeroCorreosPendientes > 0);

            PedidoListViewModel model = new PedidoListViewModel
            {
                TipoMantenimiento = TipoMantenimiento.ListadoAsignados,
                Item = pedidos.ToList()
            };

            return View(model);
        }

        [Authorize(Roles = Constantes.ROL_FINAN)]
        public ActionResult FacturasPendientes()
        {
            List<EstadoPedido> listaEstadosRol = AgregarEstadosPorNotificacion();

            var pedidos = db.Pedidos
                    .Include("Cliente")
                    .Where(x => x.Status == EstadoEntidad.Activo)
                    .Where(x => x.Estado == EstadoPedido.Pagado || x.Estado == EstadoPedido.Entregado)
                    .Where(x => x.Factura == null);

            PedidoListViewModel model = new PedidoListViewModel
            {
                TipoMantenimiento = TipoMantenimiento.ListadoAsignados,
                Item = pedidos.ToList()
            };

            return View(model);
        }

        private List<EstadoPedido> AgregarEstadosPorRol()
        {
            var listaEstados = new List<EstadoPedido>();
            if (User.IsInRole(Constantes.ROL_ADMIN))
            {

            }

            if (User.IsInRole(Constantes.ROL_GESTI))
            {
                if (!listaEstados.Any(x => x == EstadoPedido.Aprobado)) listaEstados.Add(EstadoPedido.Aprobado);
                if (!listaEstados.Any(x => x == EstadoPedido.Pagado)) listaEstados.Add(EstadoPedido.Pagado);
                if (!listaEstados.Any(x => x == EstadoPedido.Entregado)) listaEstados.Add(EstadoPedido.Entregado);
            }

            if (User.IsInRole(Constantes.ROL_SECRE))
            {
                if (!listaEstados.Any(x => x == EstadoPedido.Ingresado)) listaEstados.Add(EstadoPedido.Ingresado);
                if (!listaEstados.Any(x => x == EstadoPedido.Muestras)) listaEstados.Add(EstadoPedido.Muestras);
                if (!listaEstados.Any(x => x == EstadoPedido.Parametrizado)) listaEstados.Add(EstadoPedido.Parametrizado);
                if (!listaEstados.Any(x => x == EstadoPedido.Pagado)) listaEstados.Add(EstadoPedido.Pagado);
            }

            if (User.IsInRole(Constantes.ROL_CALID))
            {
                if (!listaEstados.Any(x => x == EstadoPedido.Tiempos)) listaEstados.Add(EstadoPedido.Tiempos);
            }

            if (User.IsInRole(Constantes.ROL_AREAS))
            {
                if (!listaEstados.Any(x => x == EstadoPedido.Pagado)) listaEstados.Add(EstadoPedido.Pagado);
            }

            if (User.IsInRole(Constantes.ROL_FINAN))
            {
                if (!listaEstados.Any(x => x == EstadoPedido.Aceptado)) listaEstados.Add(EstadoPedido.Aceptado);
            }

            return listaEstados;
        }

        private List<EstadoPedido> AgregarEstadosPorNotificacion()
        {
            var listaEstados = new List<EstadoPedido>();
            if (User.IsInRole(Constantes.ROL_GESTI))
            {
                if (!listaEstados.Any(x => x == EstadoPedido.Aprobado)) listaEstados.Add(EstadoPedido.Aprobado);
                if (!listaEstados.Any(x => x == EstadoPedido.Entregado)) listaEstados.Add(EstadoPedido.Entregado);
            }

            if (User.IsInRole(Constantes.ROL_SECRE))
            {
                if (!listaEstados.Any(x => x == EstadoPedido.Pagado)) listaEstados.Add(EstadoPedido.Pagado);
            }

            return listaEstados;
        }
        #endregion

        #region Creacion Pedidos
        [Authorize(Roles = Constantes.ROL_GESTI)]
        public ActionResult Create(string cid)
        {
            var parametroValidezOferta = db.Parametros.SingleOrDefault(x => x.Tipo == TipoParametro.ValidezOferta);

            var model = new PedidoViewModel
            {
                TipoMantenimiento = TipoMantenimiento.Creación,
                Item = new Pedido()
            };
            model.Item.Estado = EstadoPedido.Ingresado;
            model.Item.Cliente = new Cliente();
            model.Item.ClienteID = 0;
            model.Item.ValidezOferta = Convert.ToInt32(parametroValidezOferta.Valor);

            if (cid != null)
            {
                try
                {
                    int clienteID = Convert.ToInt32(cid);
                    var cliente = db.Clientes.SingleOrDefault(x => x.ID == clienteID);

                    if (cliente != null)
                    {
                        model.Item.Cliente = cliente;
                        model.Item.ClienteID = clienteID;
                    }
                }
                catch { }
            }

            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(PedidoViewModel model, HttpPostedFileBase ArchivoSolicitud)
        {
            var Cliente = db.Clientes.First(x => x.ID == model.Item.ClienteID);

            model.Item.Cliente = Cliente;
            model.Item.Codigo = Guid.NewGuid();
            model.Item.Estado = EstadoPedido.Ingresado;
            model.Item.Numero = null;

            var maxProforma = db.Pedidos.Select(x => x.Proforma).Count() > 0 ? db.Pedidos.Select(x => x.Proforma).Max() : (DateTime.Today.Year * 10000) + 0;
            model.Item.Proforma = ((maxProforma / 10000 == DateTime.Today.Year) ? maxProforma : DateTime.Today.Year * 10000) + 1;

            if (model.Item.Cliente.CorreoElectronico == null || model.Item.Cliente.CorreoElectronico == "")
            {
                ModelState.AddModelError("CorreoElectronicoCliente", Mensajes.MEN_ERROR_SINCORREO);
                ModelState.AddModelError("CorreoElectronicoClienteLink", "#linkCliente");
            }

            /* Creando plantilla de Archivos para Pedido */
            Archivo archivoSolicitud = null;
            if (ArchivoSolicitud != null)
            {
                archivoSolicitud = new Archivo() { Tipo = TipoArchivo.Solicitud, Subido = true };
                archivoSolicitud.Subido = true;
                archivoSolicitud.Extension = Path.GetExtension(ArchivoSolicitud.FileName);
                archivoSolicitud.Fecha = DateTime.UtcNow;
                archivoSolicitud.SetDataLog();
                model.Item.Archivos.Add(archivoSolicitud);
            }
            else
            {
                ModelState.AddModelError("ArchivoSolicitud", Mensajes.MEN_ERROR_SINSOLICITUD);
            }

            if (ModelState.IsValid)
            {
                db.Pedidos.Add(model.Item);
                db.FechasPedido.Add(new FechaPedido(model.Item));

                Grabar();

                ArchivoSolicitud.SaveAs(Server.MapPath($"{Constantes.CAR_ARCHI}/{archivoSolicitud.NombreArchivo}"));

                return RedirectToAction("Index");
            }

            model.TipoMantenimiento = TipoMantenimiento.Creación;
            return View(model);
        }
        #endregion

        #region Duplicar Pedidos
        [Authorize(Roles = Constantes.ROL_GESTI)]
        public ActionResult Duplicar(int? id)
        {
            if (id == null)
                throw new HttpException(400, "Petición inválida");

            var pedido = db.Pedidos
                .Include("Cliente")
                .SingleOrDefault(x => x.ID == id.Value);

            if (pedido == null)
                throw new HttpException(404, "No existe Pedido");

            var model = new PedidoViewModel
            {
                TipoMantenimiento = TipoMantenimiento.Duplicación,
                Item = new Pedido()
            };

            model.Item.Estado = EstadoPedido.Ingresado;
            model.Item.Cliente = pedido.Cliente;
            model.Item.ClienteID = pedido.ClienteID;
            model.Item.ValidezOferta = pedido.ValidezOferta;
            model.Item.PersonaContacto = pedido.PersonaContacto;
            model.Item.InformacionAdicional = pedido.InformacionAdicional;
            model.MantenerSolicitud = true;
            model.PedidoIdDuplicado = id.Value;

            return View("Create", model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Duplicar(PedidoViewModel model, HttpPostedFileBase ArchivoSolicitud)
        {
            var pedido = db.Pedidos
                .Include("Cliente")
                .Include("Archivos")
                .Include("Muestras")
                .Include("Fechas")
                .Include("Ordenes")
                .Include("Ordenes.AnalisisMuestras")
                .Include("Ordenes.AnalisisMuestras.Analisis")
                .Include("Ordenes.AnalisisMuestras.SubAnalisisMuestra")
                .SingleOrDefault(x => x.ID == model.PedidoIdDuplicado);

            var Cliente = db.Clientes.First(x => x.ID == model.Item.ClienteID);

            model.Item.Cliente = Cliente;
            model.Item.Codigo = Guid.NewGuid();
            model.Item.Numero = null;
            model.Item.IDAnterior = model.PedidoIdDuplicado;

            model.Item.Estado = EstadoPedido.Ingresado;
            db.FechasPedido.Add(new FechaPedido(model.Item));

            // Agregando Muestras
            var tieneEstadoMuestras = pedido.Fechas.SingleOrDefault(x => x.Estado == EstadoPedido.Muestras) != null;
            if (tieneEstadoMuestras && pedido.Muestras.Count() > 0)
            {
                model.Item.Estado = EstadoPedido.Muestras;
                db.FechasPedido.Add(new FechaPedido(model.Item));

                foreach (var muestra in pedido.Muestras)
                {
                    model.Item.Muestras.Add(muestra.Duplicar());
                }

                // Agregando Ordenes
                var tieneEstadoParametrizado = pedido.Fechas.SingleOrDefault(x => x.Estado == EstadoPedido.Parametrizado) != null;
                if (tieneEstadoParametrizado && pedido.Ordenes.Count() > 0)
                {
                    model.Item.IVA = GetIVA();
                    model.Item.Estado = EstadoPedido.Parametrizado;
                    db.FechasPedido.Add(new FechaPedido(model.Item));

                    foreach (var orden in pedido.Ordenes)
                    {
                        model.Item.Ordenes.Add(orden.Duplicar(model.Item.Muestras));
                    }

                    var tieneEstadoTiempos = pedido.Fechas.SingleOrDefault(x => x.Estado == EstadoPedido.Tiempos) != null;
                    if (tieneEstadoTiempos)
                    {
                        model.Item.Estado = EstadoPedido.Tiempos;
                        db.FechasPedido.Add(new FechaPedido(model.Item));
                    }
                }
            }

            var maxProforma = db.Pedidos.Select(x => x.Proforma).Count() > 0 ? db.Pedidos.Select(x => x.Proforma).Max() : (DateTime.Today.Year * 10000) + 0;
            model.Item.Proforma = ((maxProforma / 10000 == DateTime.Today.Year) ? maxProforma : DateTime.Today.Year * 10000) + 1;

            if (model.Item.Cliente.CorreoElectronico == null || model.Item.Cliente.CorreoElectronico == "")
            {
                ModelState.AddModelError("CorreoElectronicoCliente", Mensajes.MEN_ERROR_SINCORREO);
                ModelState.AddModelError("CorreoElectronicoClienteLink", "#linkCliente");
            }

            /* Creando plantilla de Archivos para Pedido */
            Archivo archivoSolicitud = null;
            var archivoADuplicar = pedido.Archivos.SingleOrDefault(x => x.Tipo == TipoArchivo.Solicitud && x.Subido);

            if (!model.MantenerSolicitud)
            {
                if (ArchivoSolicitud != null)
                {
                    archivoSolicitud = new Archivo() { Tipo = TipoArchivo.Solicitud, Subido = true };
                    archivoSolicitud.Subido = true;
                    archivoSolicitud.Extension = Path.GetExtension(ArchivoSolicitud.FileName);
                    archivoSolicitud.Fecha = DateTime.UtcNow;
                    archivoSolicitud.SetDataLog();
                    model.Item.Archivos.Add(archivoSolicitud);
                }
                else
                {
                    ModelState.AddModelError("ArchivoSolicitud", Mensajes.MEN_ERROR_SINSOLICITUD);
                }
            }
            else
            {
                // duplicar archivo de solicitud
                if (archivoADuplicar == null)
                {
                    ModelState.AddModelError("ArchivoSolicitud", Mensajes.MEN_ERROR_SINARCHIVOADUPLICAR);
                }
                else
                {
                    archivoSolicitud = new Archivo() { Tipo = TipoArchivo.Solicitud, Subido = true };
                    archivoSolicitud.Subido = true;
                    archivoSolicitud.Extension = archivoADuplicar.Extension;
                    archivoSolicitud.Fecha = DateTime.UtcNow;
                    archivoSolicitud.SetDataLog();
                    model.Item.Archivos.Add(archivoSolicitud);
                }
            }

            if (ModelState.IsValid)
            {
                db.Pedidos.Add(model.Item);

                Grabar();

                if (!model.MantenerSolicitud)
                {
                    ArchivoSolicitud.SaveAs(Server.MapPath($"{Constantes.CAR_ARCHI}/{archivoSolicitud.NombreArchivo}"));
                }
                else
                {
                    System.IO.File.Copy(
                        Server.MapPath($"{Constantes.CAR_ARCHI}/{archivoADuplicar.NombreArchivo}"),
                        Server.MapPath($"{Constantes.CAR_ARCHI}/{archivoSolicitud.NombreArchivo}"),
                        true);
                }

                TempData.Add("MensajePostBack", new MensajePostBack
                {
                    Texto = $"Se ha duplicado el pedido correctamente.\\nLos datos son:\\n - Número asignado: {model.Item.NombreOficial} \\n - Estado: {model.Item.Estado.ToDisplayName()}.",
                    Tipo = MensajePostBack.TipoMensaje.Informacion
                });
                return RedirectToAction("Index");
            }

            model.TipoMantenimiento = TipoMantenimiento.Duplicación;
            return View("Create", model);
        }

        #endregion

        #region Agregar Muestras Pedido
        [Authorize(Roles = Constantes.ROL_SECRE)]
        public ActionResult AddMuestras(int? id)
        {
            if (id == null)
                throw new HttpException(400, "Petición inválida");

            var Pedido = GetPedido(id.Value, EstadoPedido.Ingresado);

            if (Pedido == null)
                throw new HttpException(404, "No existe Pedido");

            var model = new PedidoViewModel
            {
                TipoMantenimiento = TipoMantenimiento.Modificación,
                Item = Pedido
            };
            return View("Edit", model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult AddMuestras(PedidoViewModel model, FormCollection form)
        {
            string action = form["Item.Action"];

            var pedido = db.Pedidos.SingleOrDefault(x => x.ID == model.Item.ID && x.Estado == EstadoPedido.Ingresado);

            /* Eliminando muestras anteriores si existieran */
            db.Muestras.RemoveRange(pedido.Muestras);
            db.SaveChanges();

            pedido.Muestras = model.Item.Muestras;

            if (model.Item.Muestras.Count == 0)
            {
                ModelState.AddModelError("NumeroMuestras", Mensajes.MEN_ERROR_SINMUESTRAS);
            }

            if (ModelState.IsValid)
            {
                foreach (var muestra in pedido.Muestras.Select((x, i) => new { index = i + 1, value = x }))
                {
                    muestra.value.Numero = muestra.index;
                    muestra.value.Estado = EstadoMuestra.Pendiente;
                    muestra.value.SetDataLog();
                }

                if (action.StartsWith("Enviar Muestras"))
                {
                    pedido.Estado = EstadoPedido.Muestras;
                    db.FechasPedido.Add(new FechaPedido(pedido));
                }

                Actualizar(pedido);

                if (action.Contains("Parametrizar"))
                    return RedirectToAction("AddParametros", new { id = pedido.ID });

                return RedirectToAction("Index");
            }

            // Envío de ticks para que coincidan las validaciones
            model.Item = pedido;
            ModelState.Keys
                .Where(x => x.Contains("Item.Muestras[") && x.Contains("].Detalle"))
                .Select(key => Convert.ToInt32(key.Replace("Item.Muestras[", "").Replace("].Detalle", "")))
                .ToList()
                .ForEach(tick => model.Item.Muestras.FirstOrDefault(x => !x.Ticks.HasValue).Ticks = tick);

            model.TipoMantenimiento = TipoMantenimiento.Modificación;
            return View("Edit", model);
        }
        #endregion

        #region Parametrización Análisis de Pedido
        [Authorize(Roles = Constantes.ROL_SECRE)]
        public ActionResult AddParametros(int? id)
        {
            if (id == null)
                throw new HttpException(400, "Petición inválida");

            var Pedido = GetPedido(id.Value, EstadoPedido.Muestras);

            if (Pedido == null)
                throw new HttpException(404, "No existe Pedido");

            var model = new PedidoViewModel
            {
                TipoMantenimiento = TipoMantenimiento.Modificación,
                CatalogoAnalisis = GetCatalogos(),
                ListaPlantillas = db.Plantillas.Where(x => x.Status == EstadoEntidad.Activo).ToList(),
                ListaGrupos = db.GruposAnalisis.Where(x => x.Status == EstadoEntidad.Activo).ToList(),
                Areas = db.Areas.Where(x => x.Status == EstadoEntidad.Activo).ToList(),
                Item = Pedido
            };
            return View("Edit", model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult AddParametros(FormCollection form)
        {
            string action = form["Item.Action"];

            int pedidoId = Convert.ToInt32(form["Item.ID"]);

            var pedido = GetPedido(pedidoId, EstadoPedido.Muestras);

            if (action == "Reingresar Muestras")
            {
                pedido.Estado = EstadoPedido.Ingresado;
                db.FechasPedido.Remove(pedido.Fechas.SingleOrDefault(x => x.Estado == EstadoPedido.Muestras && x.PedidoID == pedidoId));
                pedido.Muestras.ToList().ForEach(x => db.AnalisisMuestras.RemoveRange(x.AnalisisMuestras));
                db.OrdenesTrabajo.RemoveRange(pedido.Ordenes);
                Actualizar(pedido);
                return RedirectToAction("AddMuestras", new { id = pedidoId });
            }

            pedido.IVA = GetIVA();

            db.OrdenesTrabajo.RemoveRange(pedido.Ordenes);
            db.SaveChanges();

            /* Recuperando los datos del formulario */
            var indexString = form["Item.AnalisisMuestras.Index"];
            if (indexString == null)
                indexString = "";

            var listaAnalisisMuestra = indexString.Split(',').Where(y => y != "")
                .Select(index => new AnalisisMuestraHelp
                {
                    AnalisisID = Convert.ToInt32(form[$"Item.AnalisisMuestras[{index}].AnalisisID"]),
                    MuestraID = Convert.ToInt32(form[$"Item.AnalisisMuestras[{index}].MuestraID"]),
                    Cantidad = Convert.ToInt32(form[$"Item.AnalisisMuestras[{index}].Cantidad"]),
                    Ticks = Convert.ToInt32(index)
                }).ToList();

            /* Completando la información de la lista inicial*/
            var analisisIdList = listaAnalisisMuestra.Select(x => x.AnalisisID).ToList();

            var QueryAnalisis = db.Analisis
                    .Where(y => analisisIdList.Contains(y.ID))
                    .Include("SubAnalisis")
                    .Include("GrupoAnalisis.Area");

            listaAnalisisMuestra.ForEach(amh => amh.Completar(QueryAnalisis.Single(x => x.ID == amh.AnalisisID)));

            /* Generando OrdenTrabajo(s) según Area(s) */
            var listaOrdenes = listaAnalisisMuestra
                .GroupBy(x => new { x.AreaID, x.BioquimicoID, x.MuestraID })
                .Select((grupo, index) => new OrdenTrabajo
                {
                    PedidoID = pedidoId,
                    Numero = index + 1,
                    AreaID = grupo.Key.AreaID,
                    BioquimicoID = grupo.Key.BioquimicoID,
                    AnalisisMuestras = grupo.Select(x => x.ToAnalisisMuestra()).ToList(),
                    CantidadInformes = 1
                }).ToList();

            db.Entry(pedido).State = EntityState.Modified;

            db.OrdenesTrabajo.AddRange(listaOrdenes);

            /* Grabando Ordenes de trabajo y AnalisisMuestras */
            if (action.StartsWith("Enviar Parametrización"))
            {
                var t = pedido.Muestras.Select(z => new { MuestraID = z.ID, CuentaAnalisis = z.AnalisisMuestras.Count });

                if (t.Any(x => x.CuentaAnalisis == 0))
                {
                    ModelState.AddModelError("NumeroAnalisisMuestras", Mensajes.MEN_ERROR_SINANALISIS);
                    t.Where(x => x.CuentaAnalisis == 0).ToList()
                        .ForEach(r => ModelState.AddModelError($"Item.Muestra[{r.MuestraID}].NumeroAnalisisMuestras", Mensajes.MEN_ERROR_SINANALISISAMUESTRA));
                }

                pedido.Estado = EstadoPedido.Parametrizado;
                db.FechasPedido.Add(new FechaPedido(pedido));
            }

            var errors = db.GetValidationErrors().ToList();
            if (errors.Count() == 0 && ModelState.IsValid)
            {
                db.SaveChanges();

                if (action.Contains("Tiempos"))
                    return RedirectToAction("EditarTiempos", new { id = pedido.ID });
            }
            else
            {
                errors.Where(z => z.Entry.Entity.GetType() == typeof(AnalisisMuestra)).ToList()
                    .ForEach(e => e.ValidationErrors.ToList()
                    .ForEach(t => ModelState.AddModelError($"Item.AnalisisMuestras[{((AnalisisMuestra)e.Entry.Entity).Ticks}].{t.PropertyName}", t.ErrorMessage)));

                var model = new PedidoViewModel
                {
                    TipoMantenimiento = TipoMantenimiento.Modificación,
                    CatalogoAnalisis = GetCatalogos(),
                    Item = pedido
                };
                model.Item.Estado = EstadoPedido.Muestras;
                return View("Edit", model);
            }

            return RedirectToAction("Index");
        }
        #endregion

        #region Parametrización Tiempos
        [Authorize(Roles = Constantes.ROL_SECRE)]
        public ActionResult EditarTiempos(int? id)
        {
            if (id == null)
                throw new HttpException(400, "Petición inválida");

            var Pedido = GetPedido(id.Value, EstadoPedido.Parametrizado);

            if (Pedido == null)
                throw new HttpException(404, "No existe Pedido");

            var model = new PedidoViewModel
            {
                TipoMantenimiento = TipoMantenimiento.Modificación,
                Item = Pedido
            };
            return View("Edit", model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult EditarTiempos(FormCollection form)
        {
            string action = form["Item.Action"];

            int pedidoId = Convert.ToInt32(form["Item.ID"]);

            var pedido = GetPedido(pedidoId, EstadoPedido.Parametrizado);

            if (action == "Reingresar Parametrización")
            {
                pedido.Estado = EstadoPedido.Muestras;
                db.FechasPedido.Remove(pedido.Fechas.SingleOrDefault(x => x.Estado == EstadoPedido.Parametrizado && x.PedidoID == pedidoId));
                Actualizar(pedido);
                return RedirectToAction("AddParametros", new { id = pedidoId });
            }

            foreach (var ot in pedido.Ordenes)
            {
                ot.TiempoEntrega = Convert.ToInt32(form[$"Item.Ordenes[{ot.ID}].TiempoEntrega"]);
                ot.CantidadInformes = Convert.ToInt32(form[$"Item.Ordenes[{ot.ID}].CantidadInformes"]);
            }

            //// Asignar tiempo de entrega máximo
            //var TiempoEntregaMaximo = pedido.Ordenes.Max(x => x.TiempoEntrega);
            //foreach (var ot in pedido.Ordenes)
            //{
            //    ot.TiempoEntrega = TiempoEntregaMaximo;
            //}

            foreach (var ot in pedido.Ordenes)
            {
                if (!(ot.TiempoEntrega > 0))
                {
                    ModelState.AddModelError($"Item.Ordenes[{ot.ID}].TiempoEntrega", Mensajes.MEN_ERROR_TIEMPOENTREGA0);
                }

                if (!(ot.CantidadInformes > 0))
                {
                    ModelState.AddModelError($"Item.Ordenes[{ot.ID}].CantidadInformes", Mensajes.MEN_ERROR_CANTIINFORMES0);
                }
            }

            if (ModelState.IsValid)
            {
                if (action == "Enviar Tiempos")
                {
                    pedido.Estado = EstadoPedido.Tiempos;
                    db.FechasPedido.Add(new FechaPedido(pedido));
                }

                Actualizar(pedido);

                return RedirectToAction("Index");
            }

            var model = new PedidoViewModel
            {
                TipoMantenimiento = TipoMantenimiento.Modificación,
                Item = pedido
            };
            return View("Edit", model);
        }
        #endregion

        #region Aprobar Pedido/Proforma
        [Authorize(Roles = Constantes.ROL_CALID)]
        public ActionResult Aprobar(int? id)
        {
            if (id == null)
                throw new HttpException(400, "Petición inválida");

            var Pedido = GetPedido(id.Value, EstadoPedido.Tiempos);

            if (Pedido == null)
                throw new HttpException(404, "No existe Pedido");

            var model = new PedidoViewModel
            {
                TipoMantenimiento = TipoMantenimiento.Modificación,
                Item = Pedido
            };
            return View("Edit", model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Aprobar(FormCollection form)
        {
            var action = form["Item.Action"];

            int pedidoId = Convert.ToInt32(form["Item.ID"]);

            var pedido = db.Pedidos
                .Include("Ordenes")
                .Include("Ordenes.AnalisisMuestras")
                .Include("Muestras.AnalisisMuestras")
                .SingleOrDefault(x => x.ID == pedidoId && x.Estado == EstadoPedido.Tiempos);

            switch (action)
            {
                case "Aprobar":
                    pedido.Estado = EstadoPedido.Aprobado;

                    /* Creando plantilla de Archivos para Pedido */
                    pedido.Archivos.Add(new Archivo(pedidoId)
                    {
                        Tipo = TipoArchivo.Proforma
                    });

                    pedido.Archivos.Add(new Archivo(pedidoId)
                    {
                        Tipo = TipoArchivo.AceptacionCliente
                    });

                    /* Creando plantilla de Archivos para Ordenes de Trabajo */
                    foreach (var orden in pedido.Ordenes)
                    {
                        var archivoOrdenTrabajo = new Archivo(pedidoId)
                        {
                            Tipo = TipoArchivo.OrdenTrabajo,
                            IdOrdenTrabajo = orden.ID
                        };
                        pedido.Archivos.Add(archivoOrdenTrabajo);

                        /* Creando plantilla de Archivos para Muestras x Area (OrdenTrabajo) */
                        var listaTodosInformesResultados = orden.AnalisisMuestras
                            .Select(am => new
                            {
                                am.OrdenTrabajoID,
                                am.MuestraID,
                                am.OrdenTrabajo.CantidadInformes
                            })
                            .Distinct()
                            .ToList();

                        foreach (var archivoHelp in listaTodosInformesResultados)
                        {
                            for (int index = 1; index <= archivoHelp.CantidadInformes; index++)
                            {
                                pedido.Archivos.Add(new Archivo()
                                {
                                    PedidoID = pedidoId,
                                    IdOrdenTrabajo = archivoHelp.OrdenTrabajoID,
                                    IdMuestra = archivoHelp.MuestraID,
                                    NumeroInforme = index,
                                    Tipo = TipoArchivo.InformeResultados,
                                    Subido = false
                                });

                                pedido.Archivos.Add(new Archivo()
                                {
                                    PedidoID = pedidoId,
                                    IdOrdenTrabajo = archivoHelp.OrdenTrabajoID,
                                    IdMuestra = archivoHelp.MuestraID,
                                    NumeroInforme = index,
                                    Tipo = TipoArchivo.InformeSuplementario,
                                    Subido = false
                                });
                            }
                        }
                    }

                    /* Creando plantilla de Archivos para Muestras */
                    foreach (var muestra in pedido.Muestras)
                    {
                        pedido.Archivos.Add(new Archivo(pedidoId)
                        {
                            Tipo = TipoArchivo.RecepcionMuestra,
                            IdMuestra = muestra.ID
                        });
                    }

                    /* Grabando Ordenes de trabajo y AnalisisMuestras */
                    db.FechasPedido.Add(new FechaPedido(pedido));
                    Actualizar(pedido);
                    break;

                case "Desaprobar":
                    pedido.Estado = EstadoPedido.Cerrado;
                    pedido.Comentario = $"No aprobado: {form["Item.Comentario"]}";

                    /* Grabando Ordenes de trabajo y AnalisisMuestras */
                    db.FechasPedido.Add(new FechaPedido(pedido));
                    Actualizar(pedido);
                    break;

                case "Devolver":
                    pedido.Estado = EstadoPedido.Parametrizado;
                    db.FechasPedido.Remove(pedido.Fechas.SingleOrDefault(x => x.Estado == EstadoPedido.Tiempos && x.PedidoID == pedidoId));
                    Actualizar(pedido);
                    break;
            }

            return RedirectToAction("Index");
        }
        #endregion

        #region Aceptar Pedido de parte del Cliente
        [Authorize(Roles = Constantes.ROL_GESTI)]
        public ActionResult Aceptar(int? id)
        {
            if (id == null)
                throw new HttpException(400, "Petición inválida");

            var pedido = GetPedido(id.Value, EstadoPedido.Aprobado);

            if (pedido == null)
                throw new HttpException(404, "No existe Pedido");

            var model = new PedidoViewModel
            {
                TipoMantenimiento = TipoMantenimiento.Modificación,
                Item = pedido,
                UrlPedido = GetUrlPedido(pedido.Codigo)
            };
            return View("Edit", model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Aceptar(FormCollection form)
        {
            int pedidoId = Convert.ToInt32(form["Item.ID"]);

            var Pedido = db.Pedidos.SingleOrDefault(x => x.ID == pedidoId && x.Estado == EstadoPedido.Aprobado);

            string action = form["Item.Action"];

            switch (action)
            {
                case "Enviar a Pago":
                    Pedido.Estado = EstadoPedido.Aceptado;
                    break;
                case "Caducar":
                    Pedido.Estado = EstadoPedido.Cerrado;
                    Pedido.Comentario = "Cerrado por Caducado";
                    break;
            }

            /* Grabando Ordenes de trabajo y AnalisisMuestras */
            db.FechasPedido.Add(new FechaPedido(Pedido));
            Actualizar(Pedido);

            return RedirectToAction("Index");
        }
        #endregion

        #region Registro de Pago
        [Authorize(Roles = Constantes.ROL_FINAN)]
        public ActionResult GenerarPago(int? id)
        {
            if (id == null)
                throw new HttpException(400, "Petición inválida");

            var pedido = GetPedido(id.Value, EstadoPedido.Aceptado);

            if (pedido == null)
                throw new HttpException(404, "No existe Pedido");

            if (!pedido.FechaRecaudacion.HasValue)
                pedido.FechaRecaudacion = DateTime.UtcNow;

            var model = new PedidoViewModel
            {
                TipoMantenimiento = TipoMantenimiento.Modificación,
                PagosContratos = db.PagoContratos.Where(x => x.ClienteID == pedido.ClienteID).ToList(),
                Item = pedido,
                UrlPedido = GetUrlPedido(pedido.Codigo)
            };
            return View("Edit", model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult GenerarPago(FormCollection form)
        {
            int pedidoId = Convert.ToInt32(form["Item.ID"]);

            var pedido = db.Pedidos.Include("Cliente").SingleOrDefault(x => x.ID == pedidoId && x.Estado == EstadoPedido.Aceptado);

            string action = form["Item.Action"];

            switch (action)
            {
                case "Registrar y Notificar Pago":
                    pedido.Estado = EstadoPedido.Pagado;
                    pedido.FormaPago = (FormaPago)Enum.Parse(typeof(FormaPago), form["Item.FormaPago"]);

                    var maxNumeroPedido = db.Pedidos.Select(x => x.Numero).Max();
                    pedido.Numero = ((maxNumeroPedido / 10000 == DateTime.Today.Year) ? maxNumeroPedido : DateTime.Today.Year * 10000) + 1;

                    if (form["Item.FechaRecaudacion"] != null && form["Item.FechaRecaudacion"] != "")
                    {
                        pedido.FechaRecaudacion = DateTime.Parse(form["Item.FechaRecaudacion"]).ToUniversalTime();
                    }

                    if (pedido.FormaPago == FormaPago.Contrato)
                    {
                        pedido.DocumentoRecaudacion = 0;
                        if (form["Item.PagoContratoID"] != "")
                        {
                            var pagoContratoID = Convert.ToInt32(form["Item.PagoContratoID"]);
                            var PagoContrato = db.PagoContratos.SingleOrDefault(x => x.ID == pagoContratoID);
                            if (PagoContrato != null)
                            {
                                pedido.InformacionPago = PagoContrato.NombreContrato;
                                pedido.PagoContratoID = PagoContrato.ID;
                            }
                        }
                    }
                    else
                    {
                        pedido.InformacionPago = form["Item.InformacionPago"];
                        pedido.DocumentoRecaudacion = (DocumentoRecaudacion)Enum.Parse(typeof(DocumentoRecaudacion), form["Item.DocumentoRecaudacion"]);
                    }

                    if (Validator.TryValidateObject(pedido, new ValidationContext(pedido), results))
                    {
                        db.FechasPedido.Add(new FechaPedido(pedido));
                        Actualizar(pedido);

                        var parametros = db.Parametros.ToList();

                        string path = $"{Request.ApplicationPath}{(Request.ApplicationPath.EndsWith("/") ? "" : "/")}";
                        var UrlPedido = String.Format("http://{0}{1}Resultado/{2}", Request.Url.Authority, path, pedido.Codigo);

                        var result = EmailHelper.SendRecepcionPago(pedido, parametros, UrlPedido);

                        if (result != SendMailResult.Enviado)
                            TempData.Add("MensajePostBack", new MensajePostBack
                            {
                                Texto = "Hubo un inconveniente al enviar la información de pago.",
                                Tipo = MensajePostBack.TipoMensaje.Error,
                                CodigoMensaje = result.ToDisplayName()
                            });
                        else
                        {
                            EnvioCorreoDAL.GrabarEnvioCorreo(TipoNotificacion.PagoReceptado, pedido.ID, null, pedido.Cliente.Nombre, pedido.Cliente.CorreoElectronico);
                        }

                        return RedirectToAction("Index");
                    }
                    else
                    {
                        results.ToList().ForEach(x => x.MemberNames.ToList().ForEach(y => ModelState.AddModelError($"Item.{y}", x.ErrorMessage)));

                        var PedidoDB = GetPedido(pedidoId, null);
                        pedido.Ordenes = PedidoDB.Ordenes;
                        pedido.Muestras = PedidoDB.Muestras;
                        pedido.Estado = EstadoPedido.Aceptado;

                        var model = new PedidoViewModel
                        {
                            TipoMantenimiento = TipoMantenimiento.Modificación,
                            PagosContratos = db.PagoContratos.Where(x => x.ClienteID == pedido.ClienteID).ToList(),
                            Item = pedido
                        };
                        return View("Edit", model);
                    }

                case "Cerrar por No Pago":
                    pedido.Estado = EstadoPedido.Cerrado;
                    pedido.Comentario = "Cerrado por no Pago";

                    /* Grabando Ordenes de trabajo y AnalisisMuestras */
                    db.FechasPedido.Add(new FechaPedido(pedido));
                    Actualizar(pedido);

                    return RedirectToAction("Index");

                default:
                    return null;
            }
        }
        #endregion

        #region Generacion de ODT
        [Authorize(Roles = Constantes.ROL_GESTI)]
        public ActionResult GeneracionOrdenes(int? id)
        {
            if (id == null)
                throw new HttpException(400, "Petición inválida");

            var pedido = GetPedido(id.Value, EstadoPedido.Pagado);

            if (pedido == null)
                throw new HttpException(404, "No existe Pedido");

            var model = new PedidoViewModel
            {
                TipoMantenimiento = TipoMantenimiento.Modificación,
                Item = pedido,
                SubmitAction = "GeneracionOrdenes",
                UrlPedido = GetUrlPedido(pedido.Codigo)
            };
            return View("Edit", model);
        }
        #endregion

        #region Recepción de Muestras
        [Authorize(Roles = Constantes.ROL_SECRE)]
        public ActionResult RecibirMuestras(int? id)
        {
            if (id == null)
                throw new HttpException(400, "Petición inválida");

            var pedido = GetPedido(id.Value, EstadoPedido.Pagado);

            if (pedido == null)
                throw new HttpException(404, "No existe Pedido");

            var model = new PedidoViewModel
            {
                TipoMantenimiento = TipoMantenimiento.Modificación,
                Item = pedido,
                SubmitAction = "RecibirMuestras",
                UrlPedido = GetUrlPedido(pedido.Codigo)
            };
            return View("Edit", model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult RecibirMuestras(FormCollection form)
        {
            int pedidoId = Convert.ToInt32(form["Item.ID"]);

            var Pedido = db.Pedidos
                .Include("Cliente")
                .Include("Fechas")
                .Include("Archivos")
                .Include("Ordenes")
                .Include("Ordenes.Area")
                .SingleOrDefault(x => x.ID == pedidoId && x.Estado == EstadoPedido.Pagado);

            Pedido.Estado = EstadoPedido.Entregado;
            db.FechasPedido.Add(new FechaPedido(Pedido));

            /* Grabando Ordenes de trabajo y AnalisisMuestras */
            Actualizar(Pedido);

            return RedirectToAction("Index");
        }
        #endregion

        #region Entrega de Resultados
        [Authorize(Roles = Constantes.ROL_AREAS)]
        public ActionResult SubirResultados(int? id)
        {
            if (id == null)
                throw new HttpException(400, "Petición inválida");

            var pedido = GetPedido(id.Value, EstadoPedido.Pagado);

            if (pedido == null)
                throw new HttpException(404, "No existe Pedido");

            var model = new PedidoViewModel
            {
                TipoMantenimiento = TipoMantenimiento.Modificación,
                Item = pedido,
                SubmitAction = "SubirResultados",
                UrlPedido = GetUrlPedido(pedido.Codigo)
            };
            return View("Edit", model);
        }
        #endregion

        #region Cerrar Pedido
        [Authorize(Roles = Constantes.ROL_GESTI)]
        public ActionResult Cerrar(int? id)
        {
            if (id == null)
                throw new HttpException(400, "Petición inválida");

            var pedido = GetPedido(id.Value, EstadoPedido.Entregado);

            if (pedido == null)
                throw new HttpException(404, "No existe Pedido");

            var model = new PedidoViewModel
            {
                TipoMantenimiento = TipoMantenimiento.Modificación,
                Item = pedido,
                UrlPedido = GetUrlPedido(pedido.Codigo)
            };
            return View("Edit", model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Cerrar(FormCollection form)
        {
            int pedidoId = Convert.ToInt32(form["Item.ID"]);

            var Pedido = db.Pedidos.SingleOrDefault(x => x.ID == pedidoId && x.Estado == EstadoPedido.Entregado);

            if (Pedido.Estado == EstadoPedido.Entregado)
            {
                Pedido.Estado = EstadoPedido.Cerrado;
                db.FechasPedido.Add(new FechaPedido(Pedido));

                /* Grabando Ordenes de trabajo y AnalisisMuestras */
                Actualizar(Pedido);

                return RedirectToAction("Index");
            }

            return RedirectToAction("Cerrar", new { id = pedidoId });
        }
        #endregion

        #region Ver Pedido
        public ActionResult Ver(int? id)
        {
            if (id == null)
                throw new HttpException(400, "Petición inválida");

            var pedido = GetPedido(id.Value, null);

            if (pedido == null)
                throw new HttpException(404, "No existe Pedido");

            var model = new PedidoViewModel
            {
                TipoMantenimiento = TipoMantenimiento.Visualización,
                Item = pedido,
                UrlPedido = GetUrlPedido(pedido.Codigo)
            };
            return View("Edit", model);
        }
        #endregion

        #region Revisar Factura
        [Authorize(Roles = Constantes.ROL_FINAN)]
        public ActionResult RevisarFactura(int? id)
        {
            if (id == null)
                throw new HttpException(400, "Petición inválida");

            var pedido = GetPedido(id.Value, null);

            if (pedido == null)
                throw new HttpException(404, "No existe Pedido");

            var model = new PedidoViewModel
            {
                TipoMantenimiento = TipoMantenimiento.RevisionFactura,
                Item = pedido,
                UrlPedido = GetUrlPedido(pedido.Codigo)
            };
            return View("Edit", model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult RevisarFactura(FormCollection form)
        {
            int pedidoId = Convert.ToInt32(form["Item.ID"]);

            var pedido = db.Pedidos.SingleOrDefault(x => x.ID == pedidoId && x.Status == EstadoEntidad.Activo);

            if (pedido == null)
                throw new HttpException(404, "No existe Pedido");

            string factura = form["Item.Factura"];

            pedido.Factura = factura;

            Actualizar(pedido);
            return RedirectToAction("FacturasPendientes");
        }
        #endregion

        public ActionResult CaducarMasivo()
        {
            DateTime fechaActualEC = TimeZoneExtensions.EcuadorNow;

            DateTime fechaInicial = new DateTime(fechaActualEC.Year, fechaActualEC.Month, 1);

            var model = new PedidoAprobadoListViewModel
            {
                TipoMantenimiento = TipoMantenimiento.AnluacionMasiva,
                FechaInicialTexto = fechaInicial.ToString(TipoSalida.Fecha),
                FechaFinalTexto = fechaActualEC.ToString(TipoSalida.Fecha),
            };

            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult CaducarMasivo(PedidoAprobadoListViewModel model)
        {

            var pedidos = db.Pedidos
                    .Include("Cliente")
                    .Include("Fechas")
                    .Where(x => x.Estado == EstadoPedido.Aprobado)
                    .Where(x => x.Status == EstadoEntidad.Activo)
                    .ToList()
                    .Where(x =>
                        x.Fechas.First(y => y.Estado == EstadoPedido.Aprobado).Fecha >= model.FechaInicial &&
                        x.Fechas.First(y => y.Estado == EstadoPedido.Aprobado).Fecha <= model.FechaFinal
                    );

            switch (model.SubmitAction)
            {
                case "Buscar":

                    model.TipoMantenimiento = TipoMantenimiento.AnluacionMasiva;
                    model.Item = pedidos.ToList();
                    return View(model);

                case "Caducar":
                    var idsPedidosCaducar = pedidos.Select(x => x.ID);

                    var pedidosParaCaducar = db.Pedidos.Where(x => idsPedidosCaducar.Contains(x.ID)).ToList();

                    int numeroPedidosCaducados = 0;

                    foreach (var p in pedidosParaCaducar)
                    {
                        p.Estado = EstadoPedido.Cerrado;
                        p.Comentario = "Cerrado por Caducado";
                        db.FechasPedido.Add(new FechaPedido(p));
                        Actualizar(p);
                        numeroPedidosCaducados++;
                    }

                    model.SubmitAction = "Buscar";
                    model.Item = new List<Pedido>();

                    TempData.Add("MensajePostBack", new MensajePostBack
                    {
                        Texto = $"Se han caducado {numeroPedidosCaducados} pedidos correctamente.",
                        Tipo = MensajePostBack.TipoMensaje.Informacion
                    });

                    return CaducarMasivo(model);
                default:
                    return null;
            }
        }
    }
}

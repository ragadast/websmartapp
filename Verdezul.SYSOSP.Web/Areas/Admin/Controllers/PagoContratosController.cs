﻿using System;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web.Mvc;
using Verdezul.SYSOSP.Web.Areas.Admin.Models;
using Verdezul.SYSOSP.Web.Entities;

namespace Verdezul.SYSOSP.Web.Areas.Admin.Controllers
{
    [Authorize(Roles = Constantes.ROL_GESTI)]
    public class PagoContratosController : AdminController<PagoContrato>
    {
        public ActionResult Index()
        {
            PagoContratoListViewModel model = new PagoContratoListViewModel();
            model.Item = db.PagoContratos.Where(x => x.Status == EstadoEntidad.Activo).Include("Cliente").ToList();
            return View(model);
        }

        public ActionResult Create()
        {
            PagoContratoViewModel model = new PagoContratoViewModel();
            model.TipoMantenimiento = TipoMantenimiento.Creación;
            model.Item = new PagoContrato();
            model.Item.Fecha = DateTime.UtcNow;
            model.Item.Cliente = new Cliente();
            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(PagoContratoViewModel model)
        {
            var Cliente = db.Clientes.First(x => x.ID == model.Item.ClienteID);

            model.Item.Cliente = Cliente;

            if (ModelState.IsValid)
            {
                model.Item.Fecha = model.Item.Fecha.ToUniversalTime();
                db.PagoContratos.Add(model.Item);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            model.TipoMantenimiento = TipoMantenimiento.Creación;
            return View(model);
        }

        public ActionResult View(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            PagoContrato PagoContratos = db.PagoContratos.Include("Cliente").SingleOrDefault(x => x.ID == id);
            if (PagoContratos == null)
            {
                return HttpNotFound();
            }
            return View(PagoContratos);
        }
    }
}

﻿using Microsoft.AspNet.Identity.EntityFramework;
using System;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web.Mvc;
using Verdezul.SYSOSP.Web.Areas.Admin.Models;
using Verdezul.SYSOSP.Web.DAL;
using Verdezul.SYSOSP.Web.Entities;

namespace Verdezul.SYSOSP.Web.Areas.Admin.Controllers
{
    [Authorize(Roles = Constantes.ROL_ADMIN)]
    public class UsersController : Controller
    {
        protected ApplicationDbContext db = new ApplicationDbContext();

        public ActionResult Index()
        {
            UserListViewModel model = new UserListViewModel();
            var users = db.Users.Where(z => z.UserName != Constantes.USR_SUPAD).ToList();
            users.ForEach(x => x.EsBioquimico = db.Bioquimicos.Any(y => y.UserName == x.UserName && y.Status == EstadoEntidad.Activo));
            model.Item = users;
            model.Roles = db.Roles.ToDictionary(x => x.Id, y => y.Name);
            return View(model);
        }

        public ActionResult Edit(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            ApplicationUser user = db.Users.SingleOrDefault(x => x.UserName == id);
            if (user == null)
            {
                return HttpNotFound();
            }

            UserViewModel model = new UserViewModel();
            model.TipoMantenimiento = TipoMantenimiento.Modificación;
            model.Item = user;
            model.Roles = db.Roles.ToDictionary(x => x.Id, y => y.Name);
            model.Item.EsBioquimico = db.Bioquimicos.Any(bq => bq.UserName == user.UserName && bq.Status == EstadoEntidad.Activo);
            return View("Create", model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(FormCollection form)
        {
            string UserName = form["Item.UserName"];

            var user = db.Users.SingleOrDefault(x => x.UserName == UserName);
            if (user == null)
            {
                return HttpNotFound();
            }

            user.Email = form["Item.Email"];
            user.Cedula = form["Item.Cedula"];
            user.Nombres = form["Item.Nombres"];
            user.Apellidos = form["Item.Apellidos"];
            user.Cargo = form["Item.Cargo"];
            user.Titulo = form["Item.Titulo"];
            user.PhoneNumber = form["Item.PhoneNumber"];
            user.Activo = form["Item.Activo"] != null && form["Item.Activo"] == "on";

            var cuentaBloqueada = form["Item.Bloqueada"] != null && form["Item.Bloqueada"] == "on";
            if (cuentaBloqueada && (user.LockoutEndDateUtc == null || user.LockoutEndDateUtc < DateTime.UtcNow))
            {
                user.LockoutEndDateUtc = DateTime.UtcNow.AddHours(5);
            }
            else if (!cuentaBloqueada)
            {
                user.LockoutEndDateUtc = null;
            }

            user.EmailConfirmed = true;
            user.Roles.Clear();
            form.AllKeys.Where(x => x.Contains("Item.Roles")).ToList()
                .ForEach(x => user.Roles.Add(new IdentityUserRole { UserId = user.Id, RoleId = x.Replace("Item.Roles[", "").Replace("]", "") }));

            var esBioquimico = form["Item.EsBioquimico"] != null && form["Item.EsBioquimico"] == "on";
            var bioquimico = db.Bioquimicos.SingleOrDefault(x => x.UserName == UserName);

            if (esBioquimico)
            {
                if (bioquimico != null)
                {
                    bioquimico.Status = EstadoEntidad.Activo;
                }
                else
                {
                    db.Bioquimicos.Add(new Bioquimico(UserName));
                }
            }
            else
            {
                if (bioquimico != null)
                {
                    bioquimico.Status = EstadoEntidad.Eliminado;
                }
            }

            db.Entry(user).State = EntityState.Modified;
            db.SaveChanges();

            return RedirectToAction("Index");
        }
    }
}
﻿using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web.Mvc;
using Verdezul.SYSOSP.Web.Entities;
using Verdezul.SYSOSP.Web.Areas.Admin.Models;

namespace Verdezul.SYSOSP.Web.Areas.Admin.Controllers
{
    [Authorize(Roles = Constantes.ROL_GESTI)]
    public class CatalogosAnalisisController : AdminController<Analisis>
    {
        public ActionResult Index()
        {
            AnalisisListViewModel model = new AnalisisListViewModel();
            model.Item = db.Analisis.Where(x => x.Status == EstadoEntidad.Activo)
                .Include("GrupoAnalisis")
                .Include("GrupoAnalisis.Area")
                .OrderBy(x=> x.GrupoAnalisis.Area.ID)
                .ThenBy(x => x.GrupoAnalisis.ID)
                .ThenBy(x => x.Nombre)
                .ToList();
            return View(model);
        }

        public ActionResult Create()
        {
            AnalisisViewModel model = new AnalisisViewModel();
            model.TipoMantenimiento = TipoMantenimiento.Creación;
            model.Grupos = db.GruposAnalisis.Include("Area").Where(x => x.Status == EstadoEntidad.Activo).ToList();
            model.Item = new Analisis();
            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(AnalisisViewModel model)
        {
            if (ModelState.IsValid)
            {
                model.Item.Nombre = model.Item.Nombre.ToUpper();
                if (model.Item.Tecnica != null)
                    model.Item.Tecnica = model.Item.Tecnica.ToUpper();

                foreach (var subAnalisis in model.Item.SubAnalisis)
                {
                    subAnalisis.Nombre = subAnalisis.Nombre.ToUpper();
                }

                db.Analisis.Add(model.Item);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            model.TipoMantenimiento = TipoMantenimiento.Creación;
            model.Grupos = db.GruposAnalisis.Include("Area").Where(x => x.Status == EstadoEntidad.Activo).ToList();
            return View(model);
        }

        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            Analisis Analisis = db.Analisis.Find(id);
            if (Analisis == null)
            {
                return HttpNotFound();
            }

            AnalisisViewModel model = new AnalisisViewModel();
            model.TipoMantenimiento = TipoMantenimiento.Modificación;
            model.Grupos = db.GruposAnalisis.Include("Area").Where(x => x.Status == EstadoEntidad.Activo).ToList();
            model.Item = Analisis;
            return View("Create", model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(AnalisisViewModel model)
        {
            if (ModelState.IsValid)
            {
                model.Item.Nombre = model.Item.Nombre.ToUpper();
                if (model.Item.Tecnica != null)
                    model.Item.Tecnica = model.Item.Tecnica.ToUpper();

                foreach (var subAnalisis in model.Item.SubAnalisis)
                {
                    subAnalisis.Nombre = subAnalisis.Nombre.ToUpper();
                    db.Entry(subAnalisis).State = (subAnalisis.ID != 0) ? EntityState.Modified : EntityState.Added;
                }

                db.Entry(model.Item).State = EntityState.Modified;
                db.SaveChanges();

                db.SubAnalisis.RemoveRange(db.SubAnalisis.Where(x => x.AnalisisID == model.Item.ID && x.Status == EstadoEntidad.Eliminado));
                db.SaveChanges();

                var a = db.Analisis.Single(x => model.Item.ID == x.ID);
                a.TieneSubAnalisis = (a.SubAnalisis.Count() > 0);
                db.SaveChanges();

                return RedirectToAction("Index");
            }

            model.TipoMantenimiento = TipoMantenimiento.Modificación;
            model.Grupos = db.GruposAnalisis.Include("Area").Where(x => x.Status == EstadoEntidad.Activo).ToList();

            return View("Create", model);
        }

        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Analisis Analisis = db.Analisis.Include("GrupoAnalisis").SingleOrDefault(x => x.ID == id);
            if (Analisis == null)
            {
                return HttpNotFound();
            }
            return View(Analisis);
        }

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Analisis Analisis = db.Analisis.Find(id);
            Analisis.Status = EstadoEntidad.Eliminado;
            //db.Analisis.Remove(Analisis);
            db.SaveChanges();
            return RedirectToAction("Index");
        }
    }
}

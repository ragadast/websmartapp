﻿using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web.Mvc;
using Verdezul.SYSOSP.Web.Entities;
using Verdezul.SYSOSP.Web.Areas.Admin.Models;

namespace Verdezul.SYSOSP.Web.Areas.Admin.Controllers
{
    [Authorize(Roles = Constantes.ROL_GESTI)]
    public class GruposAnalisisController : AdminController<GrupoAnalisis>
    {
        public ActionResult Index(int? id)
        {
            GrupoAnalisisListViewModel model = new GrupoAnalisisListViewModel();
            model.Item = db.GruposAnalisis.Where(x => x.Status == EstadoEntidad.Activo)
                .Include("Area")
                .OrderBy(x => x.Area.ID).ThenBy(x => x.ID)
                .ToList();
            return View(model);
        }

        public ActionResult Create()
        {
            GrupoAnalisisViewModel model = new GrupoAnalisisViewModel();
            model.TipoMantenimiento = TipoMantenimiento.Creación;
            model.Areas = db.Areas.Where(x => x.Status == EstadoEntidad.Activo).ToList();
            model.Item = new GrupoAnalisis();
            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(GrupoAnalisisViewModel model)
        {
            if (ModelState.IsValid)
            {
                db.GruposAnalisis.Add(model.Item);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            model.TipoMantenimiento = TipoMantenimiento.Creación;
            return View(model);
        }

        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            GrupoAnalisis GrupoAnalisis = db.GruposAnalisis.Find(id);
            if (GrupoAnalisis == null)
            {
                return HttpNotFound();
            }

            GrupoAnalisisViewModel model = new GrupoAnalisisViewModel();
            model.TipoMantenimiento = TipoMantenimiento.Modificación;
            model.Areas = db.Areas.Where(x => x.Status == EstadoEntidad.Activo).ToList();
            model.Item = GrupoAnalisis;
            return View("Create", model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(GrupoAnalisisViewModel model)
        {
            if (ModelState.IsValid)
            {
                db.Entry(model.Item).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View("Create", model);
        }

        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            GrupoAnalisis GrupoAnalisis = db.GruposAnalisis.Include("Area").SingleOrDefault(x => x.ID == id);
            if (GrupoAnalisis == null)
            {
                return HttpNotFound();
            }
            return View(GrupoAnalisis);
        }

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            GrupoAnalisis GrupoAnalisis = db.GruposAnalisis.Find(id);
            GrupoAnalisis.Status = EstadoEntidad.Eliminado;
            //db.GrupoAnalisis.Remove(GrupoAnalisis);
            db.SaveChanges();
            return RedirectToAction("Index");
        }
    }
}

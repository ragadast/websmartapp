﻿using Microsoft.Reporting.WebForms;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Verdezul.SYSOSP.Web.Areas.Admin.Models;
using Verdezul.SYSOSP.Web.Entities;
using Verdezul.SYSOSP.Web.Helpers;

namespace Verdezul.SYSOSP.Web.Areas.Admin.Controllers
{
    [Authorize] // TODO: ver que roles se ponen
    public partial class ReportesProformasController : AdminReportController<ReporteInternoViewModel>
    {
        public ReportesProformasController()
        {
            ReportModel = new ReporteInternoViewModel();
            CarpetaReportes = Constantes.CAR_REP_PRO;
        }

        #region ProformasIngresadas (1)
        public ActionResult ProformasIngresadas()
        {
            InitReportModel(ReporteProforma.ProformasIngresadas);

            return View(ReportModel);
        }

        [HttpPost]
        public ActionResult ProformasIngresadas(ParametrosReporte Parametros)
        {
            ReporteProforma reporte = ReporteProforma.ProformasIngresadas;

            List<ReportParameter> parametrosReporteRS = GetParametrosReporteBasicos();
            parametrosReporteRS.Add(new ReportParameter("Titulo", reporte.ToDisplayName()));

            Cliente cliente = db.Clientes.SingleOrDefault(x => x.ID == Parametros.ClienteID);

            Parametros.NombreReferencialCliente = (cliente != null) ? cliente.NombreReferencial : Constantes.TextoTodos;

            // Petición de Proformas
            var proformas = db.Pedidos
                .Include("Ordenes")
                .Include("Ordenes.AnalisisMuestras")
                .Where(x => x.ClienteID == Parametros.ClienteID || Parametros.ClienteID == 0)
                .Where(x => x.Estado == Parametros.EstadoPedido || Parametros.EstadoPedido == 0)
                .Select(p => new
                {
                    Pedido = p,
                    p.Cliente,
                    FechaIngresado = p.Fechas.FirstOrDefault(y => y.Estado == EstadoPedido.Ingresado),
                    FechaAprobado = p.Fechas.FirstOrDefault(y => y.Estado == EstadoPedido.Aprobado),
                    FechaPagado = p.Fechas.FirstOrDefault(y => y.Estado == EstadoPedido.Pagado),
                    FechaCerrado = p.Fechas.FirstOrDefault(y => y.Estado == EstadoPedido.Cerrado),
                })
                .ToList()
                .Where(x => x.FechaIngresado.Fecha >= Parametros.FechaInicial && x.FechaIngresado.Fecha <= Parametros.FechaFinal)
                .Select(x => new
                {
                    x.Pedido.ID,
                    Proforma = x.Pedido.Proforma.ToString().Insert(4, "-"),
                    FechaIngresado = x.FechaIngresado.Fecha,
                    ClienteIdentificacion = x.Cliente.NumeroIdentificacion,
                    ClienteNombre = x.Cliente.Nombre,
                    x.Cliente.Direccion,
                    x.Cliente.Telefono,
                    x.Cliente.CorreoElectronico,
                    x.Cliente.CorreoElectronicoFacturacion,
                    x.Pedido.PersonaContacto,
                    x.Pedido.InformacionAdicional,
                    UsuarioIngresado = x.FechaIngresado.User,
                    TotalSinIVA = x.Pedido.CostoTotalSinIVA,
                    TotalValorIVA = x.Pedido.SumaTotalValorIVA,
                    TotalConIVA = x.Pedido.CostoTotalConIVA,
                    Estado = x.Pedido.Estado.ToDisplayName(),
                    FechaAprobado = x.FechaAprobado != null ? x.FechaAprobado.Fecha : default(DateTime),
                    FechaPago = x.FechaPagado != null ? x.FechaPagado.Fecha : default(DateTime),
                    FechaCerrado = x.FechaCerrado != null ? x.FechaCerrado.Fecha : default(DateTime),
                })
                .OrderBy(x => x.Proforma)
                .ToList();

            // Configuración de Parámetros
            var parametrosSistema = db.Parametros.ToList();
            parametrosSistema.Add(new Parametro(TipoParametroReporte.FechaInicial, Parametros.FechaInicialTexto));
            parametrosSistema.Add(new Parametro(TipoParametroReporte.FechaFinal, Parametros.FechaFinalTexto));
            parametrosSistema.Add(new Parametro(TipoParametroReporte.EstadoPedido, Parametros.EstadoPedido != 0 ? Parametros.EstadoPedido.ToDisplayName() : Constantes.TextoTodos));
            parametrosSistema.Add(new Parametro(TipoParametroReporte.NombreCliente, Parametros.NombreReferencialCliente));

            // Configuración de Reporte
            ReportModel.Visor.LocalReport.DataSources.Add(new ReportDataSource("PedidosDS", proformas));
            ReportModel.Visor.LocalReport.DataSources.Add(new ReportDataSource("ParametrosDS", parametrosSistema));

            SetReportModel(reporte, parametrosReporteRS, Parametros);

            return View(ReportModel);
        }
        #endregion

        #region ProformasAprobadas (2)
        public ActionResult ProformasAprobadas()
        {
            InitReportModel(ReporteProforma.ProformasAprobadas);

            return View(ReportModel);
        }

        [HttpPost]
        public ActionResult ProformasAprobadas(ParametrosReporte Parametros)
        {
            ReporteProforma reporte = ReporteProforma.ProformasAprobadas;

            List<ReportParameter> parametrosReporteRS = GetParametrosReporteBasicos();
            parametrosReporteRS.Add(new ReportParameter("Titulo", reporte.ToDisplayName()));

            Cliente cliente = db.Clientes.SingleOrDefault(x => x.ID == Parametros.ClienteID);

            Parametros.NombreReferencialCliente = (cliente != null) ? cliente.NombreReferencial : Constantes.TextoTodos;

            // Petición de Proformas
            var proformas = db.Pedidos
                .Include("Ordenes")
                .Include("Ordenes.AnalisisMuestras")
                .Include("Fechas")
                .Where(x => x.ClienteID == Parametros.ClienteID || Parametros.ClienteID == 0)
                .Where(x => x.Fechas.Any(y => y.Estado == EstadoPedido.Aprobado))
                .Select(p => new
                {
                    Pedido = p,
                    p.Cliente,
                    //FechaIngresado = p.Fechas.FirstOrDefault(y => y.Estado == EstadoPedido.Ingresado),
                    FechaAprobado = p.Fechas.FirstOrDefault(y => y.Estado == EstadoPedido.Aprobado),
                    FechaPagado = p.Fechas.FirstOrDefault(y => y.Estado == EstadoPedido.Pagado),
                    FechaCerrado = p.Fechas.FirstOrDefault(y => y.Estado == EstadoPedido.Cerrado),
                })
                .ToList()
                .Where(x => x.FechaAprobado.Fecha >= Parametros.FechaInicial && x.FechaAprobado.Fecha <= Parametros.FechaFinal)
                .Select(x => new
                {
                    x.Pedido.ID,
                    Proforma = x.Pedido.Proforma.ToString().Insert(4, "-"),
                    //FechaIngresado = x.FechaIngresado.Fecha,
                    ClienteIdentificacion = x.Cliente.NumeroIdentificacion,
                    ClienteNombre = x.Cliente.Nombre,
                    //x.Cliente.Direccion,
                    //x.Cliente.Telefono,
                    //x.Cliente.CorreoElectronico,
                    //x.Cliente.CorreoElectronicoFacturacion,
                    //x.Pedido.PersonaContacto,
                    //x.Pedido.InformacionAdicional,
                    TotalSinIVA = x.Pedido.CostoTotalSinIVA,
                    TotalValorIVA = x.Pedido.SumaTotalValorIVA,
                    TotalConIVA = x.Pedido.CostoTotalConIVA,
                    Estado = x.Pedido.Estado.ToDisplayName(),
                    FechaAprobado = x.FechaAprobado != null ? x.FechaAprobado.Fecha : default(DateTime),
                    UsuarioAprobado = x.FechaAprobado.User,
                    FechaPago = x.FechaPagado != null ? x.FechaPagado.Fecha : default(DateTime),
                    FechaCerrado = x.FechaCerrado != null ? x.FechaCerrado.Fecha : default(DateTime),
                })
                .OrderBy(x => x.Proforma)
                .ToList();

            // Configuración de Parámetros
            var parametrosSistema = db.Parametros.ToList();
            parametrosSistema.Add(new Parametro(TipoParametroReporte.FechaInicial, Parametros.FechaInicialTexto));
            parametrosSistema.Add(new Parametro(TipoParametroReporte.FechaFinal, Parametros.FechaFinalTexto));
            parametrosSistema.Add(new Parametro(TipoParametroReporte.NombreCliente, Parametros.NombreReferencialCliente));

            // Configuración de Reporte
            ReportModel.Visor.LocalReport.DataSources.Add(new ReportDataSource("PedidosDS", proformas));
            ReportModel.Visor.LocalReport.DataSources.Add(new ReportDataSource("ParametrosDS", parametrosSistema));

            SetReportModel(reporte, parametrosReporteRS, Parametros);

            return View(ReportModel);

        }
        #endregion

        #region ProformasPagadas (3, 4, 5)
        public ActionResult ProformasPagadas()
        {
            InitReportModel(ReporteProforma.ProformasPagadasListado);

            CargarTiposReportesDeProformasPagadas();

            return View(ReportModel);
        }

        [HttpPost]
        public ActionResult ProformasPagadas(ParametrosReporte Parametros)
        {
            ReporteProforma reporte = Parametros.ReporteProforma;

            List<ReportParameter> parametrosReporteRS = GetParametrosReporteBasicos();
            parametrosReporteRS.Add(new ReportParameter("Titulo", reporte.ToDisplayName()));

            Cliente cliente = db.Clientes.SingleOrDefault(x => x.ID == Parametros.ClienteID);

            Parametros.NombreReferencialCliente = (cliente != null) ? cliente.NombreReferencial : Constantes.TextoTodos;

            CargarTiposReportesDeProformasPagadas();

            switch (Parametros.ReporteProforma)
            {
                case ReporteProforma.ProformasPagadasListado:
                    var proformas = GetProformasPagadasListado(Parametros);
                    ReportModel.Visor.LocalReport.DataSources.Add(new ReportDataSource("PedidosDS", proformas));
                    break;
                case ReporteProforma.ProformasPagadasDetalle:
                    var amuestrasD = GetProformasPagadasDetalle(Parametros);
                    ReportModel.Visor.LocalReport.DataSources.Add(new ReportDataSource("AnalisisMuestraDS", amuestrasD));
                    break;
                case ReporteProforma.ProformasPagadasResumen:
                    var amuestrasR = GetProformasPagadasResumen(Parametros);
                    ReportModel.Visor.LocalReport.DataSources.Add(new ReportDataSource("AnalisisMuestraDS", amuestrasR));
                    break;
                default:
                    throw new HttpException(400, "Petición inválida");
            }

            // Configuración de Parámetros
            var parametrosSistema = db.Parametros.ToList();
            parametrosSistema.Add(new Parametro(TipoParametroReporte.FechaInicial, Parametros.FechaInicialTexto));
            parametrosSistema.Add(new Parametro(TipoParametroReporte.FechaFinal, Parametros.FechaFinalTexto));
            parametrosSistema.Add(new Parametro(TipoParametroReporte.NombreCliente, Parametros.NombreReferencialCliente));

            // Configuración de Reporte
            ReportModel.Visor.LocalReport.DataSources.Add(new ReportDataSource("ParametrosDS", parametrosSistema));

            SetReportModel(reporte, parametrosReporteRS, Parametros);

            return View(ReportModel);
        }

        private IEnumerable GetProformasPagadasListado(ParametrosReporte Parametros)
        {
            // Petición de Proformas
            var proformas = db.Pedidos
                .Include("Ordenes")
                .Include("Ordenes.AnalisisMuestras")
            .Include("Fechas")
            .Where(x => x.ClienteID == Parametros.ClienteID || Parametros.ClienteID == 0)
                .Where(x => x.Fechas.Any(y => y.Estado == EstadoPedido.Pagado))
                .Select(p => new
                {
                    Pedido = p,
                    p.Cliente,
                    //FechaIngresado = p.Fechas.FirstOrDefault(y => y.Estado == EstadoPedido.Ingresado),
                    FechaAprobado = p.Fechas.FirstOrDefault(y => y.Estado == EstadoPedido.Aprobado),
                    FechaPagado = p.Fechas.FirstOrDefault(y => y.Estado == EstadoPedido.Pagado),
                    FechaCerrado = p.Fechas.FirstOrDefault(y => y.Estado == EstadoPedido.Cerrado),
                })
            .ToList()
            .Where(x => x.FechaPagado.Fecha >= Parametros.FechaInicial && x.FechaPagado.Fecha <= Parametros.FechaFinal)
            .Select(x => new
            {
                    x.Pedido.ID,
                    Proforma = x.Pedido.NumeroProforma,
                    Numero = x.Pedido.NumeroPedido,
                    //FechaIngresado = x.FechaIngresado.Fecha,
                    ClienteIdentificacion = x.Cliente.NumeroIdentificacion,
                    ClienteNombre = x.Cliente.Nombre,
                    x.Cliente.Direccion,
                    x.Cliente.Telefono,
                    x.Cliente.CorreoElectronico,
                    x.Cliente.CorreoElectronicoFacturacion,
                    //x.Pedido.PersonaContacto,
                    //x.Pedido.InformacionAdicional,
                    TotalSinIVA = x.Pedido.CostoTotalSinIVA,
                    TotalValorIVA = x.Pedido.SumaTotalValorIVA,
                    TotalConIVA = x.Pedido.CostoTotalConIVA,
                    Estado = x.Pedido.Estado.ToDisplayName(),
                    FechaAprobado = x.FechaAprobado != null ? x.FechaAprobado.Fecha : default(DateTime),
                    UsuarioAprobado = x.FechaAprobado.User,
                    FechaPago = x.FechaPagado != null ? x.FechaPagado.Fecha : default(DateTime),
                    UsuarioPago = x.FechaPagado.User,
                    FechaCerrado = x.FechaCerrado != null ? x.FechaCerrado.Fecha : default(DateTime),
                    FormaPago = x.Pedido.FormaPago.ToDisplayName(),
                    DocumentoRecaudacion = x.Pedido.DocumentoRecaudacion.ToDisplayName(),
                    x.Pedido.InformacionPago,
                    x.Pedido.FechaRecaudacion,
                    x.Pedido.Factura
                })
                .OrderBy(x => x.Proforma)
                .ToList();

            return proformas;
        }

        private IEnumerable GetProformasPagadasDetalle(ParametrosReporte Parametros)
        {
            // Petición de AnalisisMuestras
            var amuestras = db.AnalisisMuestras
                .Where(x => x.OrdenTrabajo.Pedido.ClienteID == Parametros.ClienteID || Parametros.ClienteID == 0)
                .Where(x => x.OrdenTrabajo.Pedido.Fechas.Any(y => y.Estado == EstadoPedido.Pagado))
                .Select(am => new
                {
                    AnalisisMuestra = am,
                    am.OrdenTrabajo,
                    am.OrdenTrabajo.Area,
                    am.OrdenTrabajo.Pedido,
                    am.OrdenTrabajo.Pedido.Cliente,
                    FechaPagado = am.OrdenTrabajo.Pedido.Fechas.FirstOrDefault(y => y.Estado == EstadoPedido.Pagado),
                })
                .ToList()
                .Where(x => x.FechaPagado.Fecha >= Parametros.FechaInicial && x.FechaPagado.Fecha <= Parametros.FechaFinal)
                .Select(x => new
                {
                    Proforma = x.Pedido.NumeroProforma,
                    FechaPago = x.FechaPagado != null ? x.FechaPagado.Fecha : default(DateTime),
                    Numero = x.Pedido.NumeroPedido,
                    ClienteIdentificacion = x.Cliente.NumeroIdentificacion,
                    ClienteNombre = x.Cliente.Nombre,
                    Estado = x.Pedido.Estado.ToDisplayName(),
                    NombreArea = x.Area.Nombre,
                    NombreAnalisis = x.AnalisisMuestra.Nombre,
                    x.AnalisisMuestra.Cantidad,
                    x.AnalisisMuestra.Valor,
                    x.AnalisisMuestra.TotalSinIVA,
                    x.AnalisisMuestra.TotalValorIVA,
                    x.AnalisisMuestra.TotalConIVA,
                })
                .OrderBy(x => x.Proforma)
                .ToList();

            return amuestras;
        }

        private IEnumerable GetProformasPagadasResumen(ParametrosReporte Parametros)
        {
            // Petición de AnalisisMuestras
            var amuestras = db.AnalisisMuestras
                .Where(x => x.OrdenTrabajo.Pedido.ClienteID == Parametros.ClienteID || Parametros.ClienteID == 0)
                .Where(x => x.OrdenTrabajo.Pedido.Fechas.Any(y => y.Estado == EstadoPedido.Pagado))
                .Select(am => new
                {
                    AnalisisMuestra = am,
                    am.OrdenTrabajo,
                    am.OrdenTrabajo.Area,
                    am.OrdenTrabajo.Pedido,
                    FechaPagado = am.OrdenTrabajo.Pedido.Fechas.FirstOrDefault(y => y.Estado == EstadoPedido.Pagado),
                })
                .ToList()
                .Where(x => x.FechaPagado.Fecha >= Parametros.FechaInicial && x.FechaPagado.Fecha <= Parametros.FechaFinal)
                .Select(x => new
                {
                    NombreArea = x.Area.Nombre,
                    NombreAnalisis = x.AnalisisMuestra.Nombre,
                    x.AnalisisMuestra.Cantidad,
                    x.AnalisisMuestra.TotalSinIVA,
                    x.AnalisisMuestra.TotalValorIVA,
                    x.AnalisisMuestra.TotalConIVA,
                })
                .GroupBy(am => new { am.NombreArea, am.NombreAnalisis })
                .Select(x => new
                {
                    x.Key.NombreArea,
                    x.Key.NombreAnalisis,
                    Cantidad = x.Sum(y => y.Cantidad),
                    TotalSinIVA = x.Sum(y => y.TotalSinIVA),
                    TotalValorIVA = x.Sum(y => y.TotalValorIVA),
                    TotalConIVA = x.Sum(y => y.TotalConIVA),
                })
                .OrderBy(x => x.NombreArea).ThenBy(y => y.NombreAnalisis)
                .ToList();

            return amuestras;
        }
        #endregion

        #region ProformasCerradas (6, 7, 8)
        public ActionResult ProformasCerradas()
        {
            InitReportModel(ReporteProforma.ProformasCerradasListado);

            CargarTiposReportesDeProformasCerradas();

            return View(ReportModel);
        }

        [HttpPost]
        public ActionResult ProformasCerradas(ParametrosReporte Parametros)
        {
            ReporteProforma reporte = Parametros.ReporteProforma;

            List<ReportParameter> parametrosReporteRS = GetParametrosReporteBasicos();
            parametrosReporteRS.Add(new ReportParameter("Titulo", reporte.ToDisplayName()));

            Cliente cliente = db.Clientes.SingleOrDefault(x => x.ID == Parametros.ClienteID);

            Parametros.NombreReferencialCliente = (cliente != null) ? cliente.NombreReferencial : Constantes.TextoTodos;

            CargarTiposReportesDeProformasCerradas();

            switch (Parametros.ReporteProforma)
            {
                case ReporteProforma.ProformasCerradasListado:
                    var proformas = GetProformasCerradasListado(Parametros);
                    ReportModel.Visor.LocalReport.DataSources.Add(new ReportDataSource("PedidosDS", proformas));
                    break;
                case ReporteProforma.ProformasCerradasDetalle:
                    var amuestrasD = GetProformasCerradasDetalle(Parametros);
                    ReportModel.Visor.LocalReport.DataSources.Add(new ReportDataSource("AnalisisMuestraDS", amuestrasD));
                    break;
                case ReporteProforma.ProformasCerradasResumen:
                    var amuestrasR = GetProformasCerradasResumen(Parametros);
                    ReportModel.Visor.LocalReport.DataSources.Add(new ReportDataSource("AnalisisMuestraDS", amuestrasR));
                    break;
                default:
                    throw new HttpException(400, "Petición inválida");
            }

            // Configuración de Parámetros
            var parametrosSistema = db.Parametros.ToList();
            parametrosSistema.Add(new Parametro(TipoParametroReporte.FechaInicial, Parametros.FechaInicialTexto));
            parametrosSistema.Add(new Parametro(TipoParametroReporte.FechaFinal, Parametros.FechaFinalTexto));
            parametrosSistema.Add(new Parametro(TipoParametroReporte.NombreCliente, Parametros.NombreReferencialCliente));

            // Configuración de Reporte
            ReportModel.Visor.LocalReport.DataSources.Add(new ReportDataSource("ParametrosDS", parametrosSistema));

            SetReportModel(reporte, parametrosReporteRS, Parametros);

            return View(ReportModel);
        }

        private IEnumerable GetProformasCerradasListado(ParametrosReporte Parametros)
        {
            // Petición de Proformas
            var proformas = db.Pedidos
                .Include("Ordenes")
                .Include("Ordenes.AnalisisMuestras")
                .Include("Fechas")
                .Where(x => x.ClienteID == Parametros.ClienteID || Parametros.ClienteID == 0)
                .Where(x => x.Fechas.Any(y => y.Estado == EstadoPedido.Cerrado))
                .Select(p => new
                {
                    Pedido = p,
                    p.Cliente,
                    FechaIngresado = p.Fechas.FirstOrDefault(y => y.Estado == EstadoPedido.Ingresado),
                    //FechaAprobado = p.Fechas.FirstOrDefault(y => y.Estado == EstadoPedido.Aprobado),
                    //FechaPagado = p.Fechas.FirstOrDefault(y => y.Estado == EstadoPedido.Pagado),
                    FechaCerrado = p.Fechas.FirstOrDefault(y => y.Estado == EstadoPedido.Cerrado),
                })
                .ToList()
                .Where(x => x.FechaCerrado.Fecha >= Parametros.FechaInicial && x.FechaCerrado.Fecha <= Parametros.FechaFinal)
                .Select(x => new
                {
                    x.Pedido.ID,
                    Proforma = x.Pedido.NumeroProforma,
                    Numero = x.Pedido.NumeroPedido,
                    FechaIngresado = x.FechaIngresado != null ? x.FechaIngresado.Fecha : default(DateTime),
                    FechaCerrado = x.FechaCerrado != null ? x.FechaCerrado.Fecha : default(DateTime),
                    ClienteIdentificacion = x.Cliente.NumeroIdentificacion,
                    ClienteNombre = x.Cliente.Nombre,
                    x.Cliente.Telefono,
                    x.Cliente.CorreoElectronico,
                    MotivoCierre = x.Pedido.MotivoCierre.Value.ToDisplayName()
                })
                .OrderBy(x => x.Proforma)
                .ToList();

            return proformas;
        }

        private IEnumerable GetProformasCerradasDetalle(ParametrosReporte Parametros)
        {
            // Petición de AnalisisMuestras
            var amuestras = db.AnalisisMuestras
                .Where(x => x.OrdenTrabajo.Pedido.ClienteID == Parametros.ClienteID || Parametros.ClienteID == 0)
                .Where(x => x.OrdenTrabajo.Pedido.Fechas.Any(y => y.Estado == EstadoPedido.Cerrado))
                .Where(x => x.OrdenTrabajo.Pedido.Fechas.Any(y => y.Estado == EstadoPedido.Entregado))
                .Select(am => new
                {
                    AnalisisMuestra = am,
                    am.OrdenTrabajo,
                    am.OrdenTrabajo.Area,
                    am.OrdenTrabajo.Pedido,
                    am.OrdenTrabajo.Pedido.Cliente,
                    FechaCerrado = am.OrdenTrabajo.Pedido.Fechas.FirstOrDefault(y => y.Estado == EstadoPedido.Cerrado),
                })
                .ToList()
                .Where(x => x.FechaCerrado.Fecha >= Parametros.FechaInicial && x.FechaCerrado.Fecha <= Parametros.FechaFinal)
                .Select(x => new
                {
                    Proforma = x.Pedido.NumeroProforma,
                    FechaCerrado = x.FechaCerrado != null ? x.FechaCerrado.Fecha : default(DateTime),
                    Numero = x.Pedido.NumeroPedido,
                    ClienteIdentificacion = x.Cliente.NumeroIdentificacion,
                    ClienteNombre = x.Cliente.Nombre,
                    NombreArea = x.Area.Nombre,
                    NombreAnalisis = x.AnalisisMuestra.Nombre,
                    x.AnalisisMuestra.Cantidad,
                    x.AnalisisMuestra.Valor,
                    x.AnalisisMuestra.TotalSinIVA,
                    x.AnalisisMuestra.TotalValorIVA,
                    x.AnalisisMuestra.TotalConIVA,
                })
                .OrderBy(x => x.Proforma)
                .ToList();

            return amuestras;
        }

        private IEnumerable GetProformasCerradasResumen(ParametrosReporte Parametros)
        {
            // Petición de AnalisisMuestras
            var amuestras = db.AnalisisMuestras
                .Where(x => x.OrdenTrabajo.Pedido.ClienteID == Parametros.ClienteID || Parametros.ClienteID == 0)
                .Where(x => x.OrdenTrabajo.Pedido.Fechas.Any(y => y.Estado == EstadoPedido.Cerrado))
                .Where(x => x.OrdenTrabajo.Pedido.Fechas.Any(y => y.Estado == EstadoPedido.Entregado))
                .Select(am => new
                {
                    AnalisisMuestra = am,
                    am.OrdenTrabajo,
                    am.OrdenTrabajo.Area,
                    am.OrdenTrabajo.Pedido,
                    FechaCerrado = am.OrdenTrabajo.Pedido.Fechas.FirstOrDefault(y => y.Estado == EstadoPedido.Cerrado),
                })
                .ToList()
                .Where(x => x.FechaCerrado.Fecha >= Parametros.FechaInicial && x.FechaCerrado.Fecha <= Parametros.FechaFinal)
                .Select(x => new
                {
                    NombreArea = x.Area.Nombre,
                    NombreAnalisis = x.AnalisisMuestra.Nombre,
                    x.AnalisisMuestra.Cantidad,
                    x.AnalisisMuestra.TotalSinIVA,
                    x.AnalisisMuestra.TotalValorIVA,
                    x.AnalisisMuestra.TotalConIVA,
                })
                .GroupBy(am => new { am.NombreArea, am.NombreAnalisis })
                .Select(x => new
                {
                    x.Key.NombreArea,
                    x.Key.NombreAnalisis,
                    Cantidad = x.Sum(y => y.Cantidad),
                    TotalSinIVA = x.Sum(y => y.TotalSinIVA),
                    TotalValorIVA = x.Sum(y => y.TotalValorIVA),
                    TotalConIVA = x.Sum(y => y.TotalConIVA),
                })
                .OrderBy(x => x.NombreArea).ThenBy(y => y.NombreAnalisis)
                .ToList();

            return amuestras;
        }
        #endregion

        #region SeguimientoInformes (9)
        public ActionResult SeguimientoInformes()
        {
            InitReportModel(ReporteProforma.ProformasAprobadas);

            return View(ReportModel);
        }

        [HttpPost]
        public ActionResult SeguimientoInformes(ParametrosReporte Parametros)
        {
            ReporteProforma reporte = ReporteProforma.SeguimientoInformes;

            List<ReportParameter> parametrosReporteRS = GetParametrosReporteBasicos();
            parametrosReporteRS.Add(new ReportParameter("Titulo", reporte.ToDisplayName()));

            Cliente cliente = db.Clientes.SingleOrDefault(x => x.ID == Parametros.ClienteID);

            Parametros.NombreReferencialCliente = (cliente != null) ? cliente.NombreReferencial : Constantes.TextoTodos;

            // Petición de Proformas
            var seguimientos = db.SeguimientosInformes
                .Where(x => x.IdCliente == Parametros.ClienteID || Parametros.ClienteID == 0)
                .Where(x => x.FechaPago >= Parametros.FechaInicial && x.FechaPago <= Parametros.FechaFinal)
                .ToList()
                .Select(x => new
                {
                    x.IdProforma,
                    Proforma = x.Proforma.ToString().Insert(4, "-"),
                    x.FechaPago,
                    x.ClienteNombre,
                    x.ClienteIdentificacion,
                    EstadoPedido = x.EstadoPedido.ToDisplayName(),
                    FechaRecepcion = x.FechaRecepcionMuestra,
                    x.TiempoEntrega,
                    x.NombreMuestra,
                    EstadoArchivo = x.ArchivoSubido ? "Subido" : "Pendiente",
                    x.FechaCarga,
                    x.FechaNotificacion
                })
                .Distinct()
                .OrderBy(x => x.IdProforma)
                .ToList();

            // Configuración de Parámetros
            var parametrosSistema = db.Parametros.ToList();
            parametrosSistema.Add(new Parametro(TipoParametroReporte.FechaInicial, Parametros.FechaInicialTexto));
            parametrosSistema.Add(new Parametro(TipoParametroReporte.FechaFinal, Parametros.FechaFinalTexto));
            parametrosSistema.Add(new Parametro(TipoParametroReporte.NombreCliente, Parametros.NombreReferencialCliente));

            // Configuración de Reporte
            ReportModel.Visor.LocalReport.DataSources.Add(new ReportDataSource("MuestrasDS", seguimientos));
            ReportModel.Visor.LocalReport.DataSources.Add(new ReportDataSource("ParametrosDS", parametrosSistema));

            SetReportModel(reporte, parametrosReporteRS, Parametros);

            return View(ReportModel);
        }
        #endregion

        #region General
        private void InitReportModel(ReporteProforma reporte)
        {
            DateTime fechaActualEC = TimeZoneExtensions.EcuadorNow;

            DateTime fechaInicial = new DateTime(fechaActualEC.Year, fechaActualEC.Month, 1);

            ReportModel.Titulo = reporte.ToDisplayName();
            ReportModel.Renderizar = false;
            ReportModel.Parametros = new ParametrosReporte
            {
                FechaInicialTexto = fechaInicial.ToString(TipoSalida.Fecha),
                FechaFinalTexto = fechaActualEC.ToString(TipoSalida.Fecha),
                ClienteID = 0,
                NombreReferencialCliente = Constantes.TextoTodos
            };
        }

        private void SetReportModel(ReporteProforma reporte, List<ReportParameter> parametrosReporteRS, ParametrosReporte parametros)
        {
            var archivoReporte = $@"{CarpetaReportes}{reporte}.rdlc";
            ReportModel.Titulo = reporte.ToDisplayName();
            ReportModel.Visor.LocalReport.ReportPath = Request.MapPath(Request.ApplicationPath) + archivoReporte;
            ReportModel.Visor.LocalReport.SetParameters(parametrosReporteRS);
            ReportModel.Renderizar = true;
            ReportModel.Parametros = parametros;
        }

        private void CargarTiposReportesDeProformasPagadas()
        {
            ReportModel.ListaReportes.Add(ReporteProforma.ProformasPagadasListado);
            ReportModel.ListaReportes.Add(ReporteProforma.ProformasPagadasDetalle);
            ReportModel.ListaReportes.Add(ReporteProforma.ProformasPagadasResumen);
        }

        private void CargarTiposReportesDeProformasCerradas()
        {
            ReportModel.ListaReportes.Add(ReporteProforma.ProformasCerradasListado);
            ReportModel.ListaReportes.Add(ReporteProforma.ProformasCerradasDetalle);
            ReportModel.ListaReportes.Add(ReporteProforma.ProformasCerradasResumen);
        }
        #endregion
    }
}

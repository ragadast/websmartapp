﻿using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web.Mvc;
using Verdezul.SYSOSP.Web.Entities;
using Verdezul.SYSOSP.Web.Areas.Admin.Models;

namespace Verdezul.SYSOSP.Web.Areas.Admin.Controllers
{
    [Authorize(Roles = Constantes.ROL_GESTI)]
    public class ParametrosController : AdminController<Parametro>
    {
        public ActionResult Index(int? id)
        {
            ParametroListViewModel model = new ParametroListViewModel();
            model.Item = db.Parametros.Where(x => x.Status == EstadoEntidad.Activo).ToList();
            return View(model);
        }

        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            Parametro Parametro = db.Parametros.Find(id);
            if (Parametro == null)
            {
                return HttpNotFound();
            }

            ParametroViewModel model = new ParametroViewModel();
            model.TipoMantenimiento = TipoMantenimiento.Modificación;
            model.Item = Parametro;
            return View("Create", model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(ParametroViewModel model)
        {
            if (ModelState.IsValid)
            {
                db.Entry(model.Item).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View("Create", model);
        }
    }
}

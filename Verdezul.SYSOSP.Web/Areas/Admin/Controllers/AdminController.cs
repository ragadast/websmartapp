﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Globalization;
using System.Linq;
using System.Web.Mvc;
using Verdezul.SYSOSP.Web.Areas.Admin.Models;
using Verdezul.SYSOSP.Web.DAL;
using Verdezul.SYSOSP.Web.Entities;
using Verdezul.SYSOSP.Web.Log;

namespace Verdezul.SYSOSP.Web.Areas.Admin.Controllers
{
    public class AdminController<T> : Controller where T : Entidad
    {
        protected ApplicationDbContext db = new ApplicationDbContext();

        protected override void OnException(ExceptionContext filterContext)
        {
            if (filterContext == null)
                base.OnException(filterContext);

            SYSOSPLog.Grabar(this, filterContext.Exception);

            if (filterContext.HttpContext.IsCustomErrorEnabled)
            {
                filterContext.ExceptionHandled = true;
                HandleErrorInfo model = new HandleErrorInfo(filterContext.Exception, "Error", "Error");

                if (filterContext.Result is System.Web.Mvc.EmptyResult)
                {
                    this.View("Error", model).ExecuteResult(this.ControllerContext);
                }
            }
        }

        protected void Grabar()
        {
            db.SaveChanges();
        }

        protected void Actualizar(T entidad)
        {
            entidad.SetDataLog();
            db.Entry(entidad).State = EntityState.Modified;
            db.SaveChanges();
        }

        protected void Eliminar(T entidad)
        {
            entidad.SetDataLog();
            entidad.Status = EstadoEntidad.Eliminado;
            db.Entry(entidad).State = EntityState.Modified;
            db.SaveChanges();
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        protected List<Catalogo> GetCatalogos()
        {
            var areas = db.Areas
                .Where(x => x.Status == EstadoEntidad.Activo)
                .Include("GruposAnalisis")
                .Include("GruposAnalisis.Analisis")
                .OrderBy(x => x.Nombre)
                .ToList();

            var listaAnalisis = new List<Catalogo>();
            foreach (var area in areas.OrderBy(x => x.ID))
            {
                listaAnalisis.Add(new Catalogo { ID = area.ID, ObjetoJS = area.ObjetoJS, Nombre = area.Nombre, Division = DivisionCatalogo.InicioArea });
                foreach (var grupo in area.GruposAnalisis.Where(x => x.Status == EstadoEntidad.Activo).OrderBy(x => x.ID))
                {
                    listaAnalisis.Add(new Catalogo { ID = grupo.ID, ObjetoJS = grupo.ObjetoJS, IDArea = area.ID, Nombre = grupo.Nombre, Division = DivisionCatalogo.InicioGrupo });
                    foreach (var analisis in grupo.Analisis.Where(x => x.Status == EstadoEntidad.Activo).OrderBy(x => x.Nombre))
                    {
                        listaAnalisis.Add(new Catalogo { ID = analisis.ID, ObjetoJS = analisis.ObjetoJS, IDArea = area.ID, Nombre = analisis.Nombre, Division = DivisionCatalogo.Analisis });
                    }
                    listaAnalisis.Add(new Catalogo { ID = grupo.ID, ObjetoJS = grupo.ObjetoJS, Nombre = grupo.Nombre, Division = DivisionCatalogo.FinGrupo });
                }
                listaAnalisis.Add(new Catalogo { ID = area.ID, ObjetoJS = area.ObjetoJS, Nombre = area.Nombre, Division = DivisionCatalogo.FinArea });
            }
            return listaAnalisis;
        }

        protected string GetUrlPedido(Guid codigoPedido)
        {
            string path = $"{Request.ApplicationPath}{(Request.ApplicationPath.EndsWith("/") ? "" : "/")}";
            return String.Format("http://{0}{1}Resultado/{2}", Request.Url.Authority, path, codigoPedido);
        }
    }
}
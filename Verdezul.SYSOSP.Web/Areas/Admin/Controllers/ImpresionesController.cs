﻿using Microsoft.Reporting.WebForms;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Verdezul.SYSOSP.Web.Areas.Admin.Models;
using Verdezul.SYSOSP.Web.Entities;
using Verdezul.SYSOSP.Web.Helpers;

namespace Verdezul.SYSOSP.Web.Areas.Admin.Controllers
{
    [Authorize]
    public partial class ImpresionesController : AdminReportController<ReporteViewModel>
    {
        public ImpresionesController()
        {
            ReportModel = new ReporteInternoViewModel();
            CarpetaReportes = Constantes.CAR_REP_IMP;
        }

        private Pedido GetPedido(int id, bool incluirInformacionUsuarios)
        {
            var pedido = db.Pedidos
                .Include("Cliente")
                .Include("Archivos")
                .Include("Fechas")
                .Include("Ordenes.Area")
                .Include("Ordenes.Bioquimico")
                .Include("Ordenes.AnalisisMuestras.Muestra")
                .Include("Ordenes.AnalisisMuestras.Analisis")
                .Include("Ordenes.AnalisisMuestras.Analisis.GrupoAnalisis")
                .Include("Ordenes.AnalisisMuestras.SubAnalisisMuestra")
                .SingleOrDefault(x => x.ID == id);

            if (incluirInformacionUsuarios)
            {
                pedido.Fechas.ToList().ForEach(x => x.NombreUsuario = db.Users.Single(y => y.UserName == x.User).NombresCompletos);
                pedido.Ordenes.ToList().ForEach(x => x.Area.Bioquimico.ApplicationUser = db.Users.Single(u => u.UserName == x.Area.Bioquimico.UserName));
            }

            return pedido;
        }

        public ActionResult Imprimir(int? id, ReportePedido? reportePedido, int? muestraID, int? ordenTrabajoID)
        {
            if (id == null)
                throw new HttpException(400, "Petición inválida");

            if (reportePedido == null)
                throw new HttpException(400, "Petición inválida");

            Pedido pedido = GetPedido(id.Value, true);

            if (pedido == null)
                throw new HttpException(404, "No existe Pedido");

            List<Pedido> pedidos = new List<Pedido>() { pedido };
            List<Cliente> clientes = new List<Cliente>() { pedido.Cliente };

            var archivoReporte = $@"{CarpetaReportes}{reportePedido}.rdlc";
            ReportModel.Titulo = reportePedido.Value.ToDisplayName();
            ReportModel.Visor.LocalReport.ReportPath = Request.MapPath(Request.ApplicationPath) + archivoReporte;

            var parametrosSistema = db.Parametros.ToList();

            var fechas = pedido.Fechas.OrderBy(x => x.Estado).ToList();

            var ordenes = pedido.Ordenes;

            List<AnalisisMuestra> analisisMuestras = new List<AnalisisMuestra>();
            ordenes.ToList().ForEach(x => analisisMuestras.AddRange(x.AnalisisMuestras));

            var ordenesExtendido = analisisMuestras.Select(am => new
            {
                am.OrdenTrabajo.TiempoEntrega,
                NombreArea = am.OrdenTrabajo.Area.Nombre,
                am.Muestra.NombreMuestra
            }).Distinct();

            var analisisMuestrasPlano = new List<AnalisisMuestraHelp>();

            int i = 0;
            foreach (var analisisMuestra in analisisMuestras.OrderBy(x => x.Muestra.Numero))
            {
                i++;
                analisisMuestrasPlano.Add(new AnalisisMuestraHelp
                {
                    Orden = i,
                    OrdenTrabajoID = analisisMuestra.OrdenTrabajoID,
                    MuestraID = analisisMuestra.MuestraID,
                    Cantidad = analisisMuestra.Cantidad,
                    Tecnica = analisisMuestra.Tecnica,
                    Valor = analisisMuestra.Valor,
                    TotalSinIVA = analisisMuestra.TotalSinIVA,
                    TotalConIVA = analisisMuestra.TotalConIVA,
                    AcreditacionSAE = analisisMuestra.AcreditacionSAE,
                    Nombre = analisisMuestra.Nombre,
                    NombreArea = analisisMuestra.OrdenTrabajo.Area.Nombre,
                    NumeroOrdenTrabajo = analisisMuestra.OrdenTrabajo.Numero,
                    NumeroMuestra = analisisMuestra.Muestra.Numero
                });

                foreach (var subAnalisisMuestra in analisisMuestra.SubAnalisisMuestra)
                {
                    analisisMuestrasPlano.Add(new AnalisisMuestraHelp
                    {
                        Orden = 0,
                        OrdenTrabajoID = analisisMuestra.OrdenTrabajoID,
                        MuestraID = analisisMuestra.MuestraID,
                        Cantidad = 0,
                        Tecnica = "",
                        Valor = 0,
                        TotalSinIVA = 0,
                        TotalConIVA = 0,
                        AcreditacionSAE = true,
                        Nombre = " - " + subAnalisisMuestra.Nombre,
                        NombreArea = analisisMuestra.OrdenTrabajo.Area.Nombre,
                        NumeroOrdenTrabajo = analisisMuestra.OrdenTrabajo.Numero,
                        NumeroMuestra = analisisMuestra.Muestra.Numero
                    });
                }
            }

            decimal TotalSinIVAPedido = analisisMuestras.Sum(x => x.TotalSinIVA);
            decimal TotalConIVAPedido = analisisMuestras.Sum(x => x.TotalConIVA);

            List<ReportParameter> parametrosReporte = GetParametrosReporteBasicos();

            switch (reportePedido)
            {
                case ReportePedido.HojaTecnica:
                    var ReportModelExtra = new ReporteHojaTecnicaViewModel
                    {
                        Titulo = ReportModel.Titulo,
                        Pedido = pedido
                    };
                    ReportModelExtra.Visor.LocalReport.ReportPath = ReportModel.Visor.LocalReport.ReportPath;
                    ReportModelExtra.Visor.LocalReport.DataSources.Add(new ReportDataSource("ClienteDS", clientes));
                    ReportModelExtra.Visor.LocalReport.DataSources.Add(new ReportDataSource("PedidosDS", pedidos));
                    ReportModelExtra.Visor.LocalReport.DataSources.Add(new ReportDataSource("MuestrasDS", pedido.Muestras));
                    ReportModelExtra.Visor.LocalReport.DataSources.Add(new ReportDataSource("OrdenesDS", ordenesExtendido));
                    ReportModelExtra.Visor.LocalReport.DataSources.Add(new ReportDataSource("ParametrosDS", parametrosSistema));
                    ReportModelExtra.Visor.LocalReport.DataSources.Add(new ReportDataSource("FechaPedidoDS", fechas));
                    ReportModelExtra.Visor.LocalReport.DataSources.Add(new ReportDataSource("AnalisisMuestrasDS", analisisMuestrasPlano));
                    ReportModelExtra.Visor.LocalReport.DataSources.Add(new ReportDataSource("EnumeradosDS", Enumerado.GetEnumerados()));
                    ReportModelExtra.Visor.LocalReport.SetParameters(parametrosReporte);
                    return View("ReporteHojaTecnica", ReportModelExtra);

                case ReportePedido.Proforma:
                    var ordenesExtendidoProforma = new[] { new {
                        TiempoEntrega = ordenesExtendido.Max(x => x.TiempoEntrega)
                    } }.ToList();

                    parametrosReporte.Add(new ReportParameter("URLConfidencialidad", new Uri(Server.MapPath("~/Reportes/Images/Confidencialidad.png")).AbsoluteUri));

                    ReportModel.Visor.LocalReport.DataSources.Add(new ReportDataSource("ClienteDS", clientes));
                    ReportModel.Visor.LocalReport.DataSources.Add(new ReportDataSource("PedidosDS", pedidos));
                    ReportModel.Visor.LocalReport.DataSources.Add(new ReportDataSource("MuestrasDS", pedido.Muestras));
                    ReportModel.Visor.LocalReport.DataSources.Add(new ReportDataSource("OrdenesDS", ordenesExtendidoProforma));
                    ReportModel.Visor.LocalReport.DataSources.Add(new ReportDataSource("ParametrosDS", parametrosSistema));
                    ReportModel.Visor.LocalReport.DataSources.Add(new ReportDataSource("FechaPedidoDS", fechas));
                    ReportModel.Visor.LocalReport.DataSources.Add(new ReportDataSource("AnalisisMuestrasDS", analisisMuestrasPlano));
                    ReportModel.Visor.LocalReport.DataSources.Add(new ReportDataSource("EnumeradosDS", Enumerado.GetEnumerados()));
                    ReportModel.Visor.LocalReport.SetParameters(parametrosReporte);
                    return View("ReportView", ReportModel);

                case ReportePedido.RecepcionMuestra:

                    List<Muestra> muestraUnica = new List<Muestra> { pedido.Muestras.Single(x => x.ID == muestraID) };

                    var ordenesMuestra = analisisMuestras
                        .Where(p => p.MuestraID == muestraID)
                        .Select(am => am.OrdenTrabajo)
                        .Distinct()
                        .Select(ot => new
                        {
                            NombreArea = ot.Area.Nombre,
                            NombreBioquimico = ot.Area.Bioquimico.ApplicationUser.NombresApellidos,
                            ot.NumeroOrden,
                            TotalSinIVA = ot.AnalisisMuestras.Where(am => am.MuestraID == muestraID).Sum(p => p.TotalSinIVA),
                            TotalConIVA = ot.AnalisisMuestras.Where(am => am.MuestraID == muestraID).Sum(p => p.TotalConIVA),
                        }).ToList();

                    parametrosSistema.Add(new Parametro { ID = 201, Valor = TotalSinIVAPedido.ToString("c") });
                    parametrosSistema.Add(new Parametro { ID = 202, Valor = TotalConIVAPedido.ToString("c") });

                    ReportModel.Visor.LocalReport.DataSources.Add(new ReportDataSource("ClienteDS", clientes));
                    ReportModel.Visor.LocalReport.DataSources.Add(new ReportDataSource("PedidosDS", pedidos));
                    ReportModel.Visor.LocalReport.DataSources.Add(new ReportDataSource("MuestrasDS", muestraUnica));
                    ReportModel.Visor.LocalReport.DataSources.Add(new ReportDataSource("OrdenesDS", ordenesMuestra));
                    ReportModel.Visor.LocalReport.DataSources.Add(new ReportDataSource("FechaPedidoDS", fechas));
                    ReportModel.Visor.LocalReport.DataSources.Add(new ReportDataSource("ParametrosDS", parametrosSistema));
                    ReportModel.Visor.LocalReport.DataSources.Add(new ReportDataSource("EnumeradosDS", Enumerado.GetEnumerados()));
                    ReportModel.Visor.LocalReport.SetParameters(parametrosReporte);
                    return View("ReportView", ReportModel);

                case ReportePedido.OrdenTrabajo:

                    var ordenUnica = pedido.Ordenes
                        .Where(x => x.ID == ordenTrabajoID)
                        .Select(ot => new
                        {
                            ot.ID,
                            ot.NumeroOrden,
                            NombreArea = ot.Area.Nombre,
                            NombreBioquimico = ot.Area.Bioquimico.NombresCompletos,
                            ot.TiempoEntrega
                        });

                    var analisisMuestrasOrdenTrabajo = analisisMuestrasPlano.Where(x => x.OrdenTrabajoID == ordenTrabajoID);
                    var muestrasAsociadasOrdenTrabajo = pedido.Muestras.Where(x => analisisMuestrasOrdenTrabajo.Select(y => y.MuestraID).Distinct().Contains(x.ID));

                    parametrosSistema.Add(new Parametro { ID = 201, Valor = TotalSinIVAPedido.ToString("c") });
                    parametrosSistema.Add(new Parametro { ID = 202, Valor = TotalConIVAPedido.ToString("c") });

                    ReportModel.Visor.LocalReport.DataSources.Add(new ReportDataSource("ClienteDS", clientes));
                    ReportModel.Visor.LocalReport.DataSources.Add(new ReportDataSource("FechaPedidoDS", fechas));
                    ReportModel.Visor.LocalReport.DataSources.Add(new ReportDataSource("AnalisisMuestrasDS", analisisMuestrasOrdenTrabajo));
                    ReportModel.Visor.LocalReport.DataSources.Add(new ReportDataSource("PedidosDS", pedidos));
                    ReportModel.Visor.LocalReport.DataSources.Add(new ReportDataSource("MuestrasDS", muestrasAsociadasOrdenTrabajo));
                    ReportModel.Visor.LocalReport.DataSources.Add(new ReportDataSource("OrdenesDS", ordenUnica));
                    ReportModel.Visor.LocalReport.DataSources.Add(new ReportDataSource("ParametrosDS", parametrosSistema));
                    ReportModel.Visor.LocalReport.DataSources.Add(new ReportDataSource("EnumeradosDS", Enumerado.GetEnumerados()));
                    ReportModel.Visor.LocalReport.SetParameters(parametrosReporte);
                    return View("ReportView", ReportModel);

                case ReportePedido.Plantilla:

                    var ordenUnica2 = pedido.Ordenes
                        .Where(x => x.ID == ordenTrabajoID)
                        .Select(ot => new
                        {
                            ot.ID,
                            ot.NumeroOrden,
                            NombreArea = ot.Area.Nombre,
                            NombreBioquimico = ot.Area.Bioquimico.NombresCompletos
                        });

                    var analisisMuestrasOrdenTrabajo2 = analisisMuestrasPlano.Where(x => x.OrdenTrabajoID == ordenTrabajoID).OrderBy(x => x.NumeroMuestra);
                    var muestrasAsociadasOrdenTrabajo2 = pedido.Muestras.Where(x => analisisMuestrasOrdenTrabajo2.Select(y => y.MuestraID).Distinct().Contains(x.ID));

                    parametrosSistema.Add(new Parametro { ID = 201, Valor = TotalSinIVAPedido.ToString("c") });
                    parametrosSistema.Add(new Parametro { ID = 202, Valor = TotalConIVAPedido.ToString("c") });

                    ReportModel.Visor.LocalReport.DataSources.Add(new ReportDataSource("ClienteDS", clientes));
                    ReportModel.Visor.LocalReport.DataSources.Add(new ReportDataSource("FechaPedidoDS", fechas));
                    ReportModel.Visor.LocalReport.DataSources.Add(new ReportDataSource("AnalisisMuestrasDS", analisisMuestrasOrdenTrabajo2));
                    ReportModel.Visor.LocalReport.DataSources.Add(new ReportDataSource("PedidosDS", pedidos));
                    ReportModel.Visor.LocalReport.DataSources.Add(new ReportDataSource("MuestrasDS", muestrasAsociadasOrdenTrabajo2));
                    ReportModel.Visor.LocalReport.DataSources.Add(new ReportDataSource("OrdenesDS", ordenUnica2));
                    ReportModel.Visor.LocalReport.SetParameters(parametrosReporte);
                    return View("ReportView", ReportModel);
            }

            return View("ReportView", ReportModel);
        }
    }
}
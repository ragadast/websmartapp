﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web.Mvc;
using Verdezul.SYSOSP.Web.Areas.Admin.Models;
using Verdezul.SYSOSP.Web.Entities;

namespace Verdezul.SYSOSP.Web.Areas.Admin.Controllers
{
    [Authorize]
    public partial class PedidosController : AdminController<Pedido>
    {
        private Pedido GetPedido(int id, EstadoPedido? estadoPedido)
        {
            return GetPedido(id, estadoPedido, false);
        }

        private Pedido GetPedido(int id, EstadoPedido? estadoPedido, bool incluirInformacionUsuarios)
        {
            var pedido = db.Pedidos
                .Include("Cliente")
                .Include("Archivos")
                .Include("Fechas")
                .Include("Ordenes.Area")
                .Include("Ordenes.Bioquimico")
                .Include("Ordenes.AnalisisMuestras.Muestra")
                .Include("Ordenes.AnalisisMuestras.Analisis")
                .Include("Ordenes.AnalisisMuestras.Analisis.GrupoAnalisis")
                .Include("Ordenes.AnalisisMuestras.SubAnalisisMuestra")
                .SingleOrDefault(x => x.ID == id &&
                                    (x.Estado == estadoPedido || estadoPedido == null));

            if (incluirInformacionUsuarios)
            {
                pedido.Fechas.ToList().ForEach(x => x.NombreUsuario = db.Users.Single(y => y.UserName == x.User).NombresCompletos);
                pedido.Ordenes.ToList().ForEach(x => x.Area.Bioquimico.ApplicationUser = db.Users.Single(u => u.UserName == x.Area.Bioquimico.UserName));
            }

            return pedido;
        }

        private List<Pedido> GetPedidos(TipoFiltro tipoFiltro, string texto, DateTime? fecha1, DateTime? fecha2, List<Area> areas, TipoListadoPedido tipoListadoPedido)
        {
            List<EstadoPedido> listaEstados = (tipoListadoPedido == TipoListadoPedido.NoCerrados) ? 
                Constantes.ListaEstadosPedidoNoCerrados : 
                Constantes.ListaEstadosPedidoCerrados;

            switch (tipoFiltro)
            {
                case TipoFiltro.Asignados:
                    List<EstadoPedido> listaEstadosRol = AgregarEstadosPorRol();
                    var pedidos = db.Pedidos.Where(x => x.Status == EstadoEntidad.Activo && listaEstadosRol.Contains(x.Estado))
                        .Include("Cliente")
                        .Include("Ordenes")
                        .Include("Ordenes.Area")
                        .ToList();

                    /* Quita todos los pedidos que no pertenezcan al area del usuario bioquimico sin deterioro del Área de Secretaría */
                    pedidos.RemoveAll(x => !x.TieneAlgunArea(areas.Select(y => y.ID)) &&
                                            x.Estado == EstadoPedido.Pagado &&
                                            User.IsInRole(Constantes.ROL_AREAS) &&
                                            !User.IsInRole(Constantes.ROL_SECRE));

                    return pedidos;

                case TipoFiltro.Texto:
                    List<int> numeros = new List<int>();
                    try
                    {
                        int numero = Convert.ToInt32(texto.Replace("-", "").Replace(" ", ""));
                        if (numero <= 9999)
                        {
                            numeros.Add(numero);
                            for (int year = 2021; year <= DateTime.Today.Year; year++)
                            {
                                numeros.Add(numero + (year * 10000));
                            }
                        }
                        else
                        {
                            numeros.Add(numero);
                        }
                    }
                    catch { }

                    return db.Pedidos.Where(x => x.Status == EstadoEntidad.Activo && listaEstados.Contains(x.Estado)
                        && (
                            x.Cliente.NumeroIdentificacion == texto.Trim() ||
                            x.Cliente.Nombre.Contains(texto) ||
                            x.PersonaContacto.Contains(texto) ||
                            numeros.Contains(x.Proforma) ||
                            (x.Numero.HasValue && numeros.Contains(x.Numero.Value))
                        )).Include("Cliente").ToList();

                case TipoFiltro.Fechas:
                    return db.Pedidos
                        .Where(x => x.Status == EstadoEntidad.Activo && listaEstados.Contains(x.Estado))
                        .Select(pedido => new
                        {
                            pedido.ID,
                            pedido.Cliente,
                            pedido.Estado,
                            pedido.Proforma,
                            pedido.Numero,
                            pedido.Ordenes,
                            pedido.Muestras,
                            pedido.Fechas,
                            FechaIngreso = pedido.Fechas.FirstOrDefault(y => y.Estado == EstadoPedido.Ingresado),
                            FechaCierre = pedido.Fechas.FirstOrDefault(y => y.Estado == EstadoPedido.Cerrado),
                        })
                        .ToList()
                        .Where(x => (x.FechaIngreso.Fecha >= fecha1.Value && x.FechaIngreso.Fecha <= fecha2.Value && tipoListadoPedido == TipoListadoPedido.NoCerrados) ||
                                    (x.FechaCierre != null && x.FechaCierre.Fecha >= fecha1.Value && x.FechaCierre.Fecha <= fecha2.Value && tipoListadoPedido == TipoListadoPedido.SoloCerrados))
                        .Select(x => new Pedido
                        {
                            ID = x.ID,
                            Cliente = x.Cliente,
                            Estado = x.Estado,
                            Proforma = x.Proforma,
                            Numero = x.Numero,
                            Ordenes = x.Ordenes,
                            Muestras = x.Muestras,
                            Fechas = x.Fechas
                        })
                        .ToList();

                case TipoFiltro.Todos:
                    return db.Pedidos
                        .Where(x => x.Status == EstadoEntidad.Activo && listaEstados.Contains(x.Estado))
                        .Include("Cliente")
                        .Include("Ordenes")
                        .Include("Ordenes.Area").ToList();

                default:
                    return null;
            }
        }

        private decimal GetIVA()
        {
            return Convert.ToDecimal(db.Parametros.Single(x => x.Tipo == TipoParametro.PorcentajeIVA).Valor) / 100;
        }
    }
}

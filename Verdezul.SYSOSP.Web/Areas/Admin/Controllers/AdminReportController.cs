﻿using Microsoft.Reporting.WebForms;
using System;
using System.Collections.Generic;
using System.Web.Mvc;
using Verdezul.SYSOSP.Web.Areas.Admin.Models;
using Verdezul.SYSOSP.Web.DAL;

namespace Verdezul.SYSOSP.Web.Areas.Admin.Controllers
{
    public class AdminReportController<T> : Controller where T : ReporteViewModel
    {
        protected ApplicationDbContext db = new ApplicationDbContext();

        protected T ReportModel;

        protected string CarpetaReportes;

        protected List<ReportParameter> GetParametrosReporteBasicos()
        {
            return new List<ReportParameter>
            {
                new ReportParameter("Usuario", User.Identity.Name),
                new ReportParameter("URLSelloUCEFCQ", new Uri(Server.MapPath("~/Reportes/Images/SelloUCEFCQ.jpg")).AbsoluteUri),
                new ReportParameter("ServerEnvironment", Environment.Database.GetConnectionTypeByServer())
            };
        }
    }
}
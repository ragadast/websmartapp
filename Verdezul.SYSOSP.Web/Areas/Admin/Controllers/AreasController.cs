﻿using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web.Mvc;
using Verdezul.SYSOSP.Web.Areas.Admin.Models;
using Verdezul.SYSOSP.Web.Entities;

namespace Verdezul.SYSOSP.Web.Areas.Admin.Controllers
{
    [Authorize(Roles = Constantes.ROL_GESTI)]
    public class AreasController : AdminController<Area>
    {

        private Dictionary<int, string> GetBioquimicos()
        {
            return (from bioqs in db.Bioquimicos
                    join users in db.Users
                    on bioqs.UserName equals users.UserName
                    where bioqs.Status == EstadoEntidad.Activo
                    select new { BioquimicoID = bioqs.ID, Titulo = users.Titulo, Nombres = users.Nombres, Apellidos = users.Apellidos })
                                .ToDictionary(x => x.BioquimicoID, x => $"{x.Titulo} {x.Nombres} {x.Apellidos}");
        }

        public ActionResult Index(int? id)
        {
            AreaListViewModel model = new AreaListViewModel();
            var areas = db.Areas.OrderBy(x => x.ID).Where(x => x.Status == EstadoEntidad.Activo);
            model.Item = areas.ToList();
            model.Item.ForEach(bq => bq.Bioquimico.ApplicationUser = db.Users.Where(user => areas.Select(p => p.Bioquimico).Select(sbq => sbq.UserName).Contains(user.UserName)).Single(suser => suser.UserName == bq.Bioquimico.UserName));
            return View(model);
        }

        public ActionResult Create()
        {
            AreaViewModel model = new AreaViewModel();
            model.TipoMantenimiento = TipoMantenimiento.Creación;
            model.Bioquimicos = GetBioquimicos();
            model.Item = new Area();
            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(AreaViewModel model)
        {
            if (ModelState.IsValid)
            {
                db.Areas.Add(model.Item);
                Grabar();
                return RedirectToAction("Index");
            }

            model.TipoMantenimiento = TipoMantenimiento.Creación;
            return View(model);
        }

        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            Area Area = db.Areas.Find(id);
            if (Area == null)
            {
                return HttpNotFound();
            }

            AreaViewModel model = new AreaViewModel();
            model.TipoMantenimiento = TipoMantenimiento.Modificación;
            model.Bioquimicos = GetBioquimicos();
            model.Item = Area;
            return View("Create", model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(AreaViewModel model)
        {
            if (ModelState.IsValid)
            {
                Actualizar(model.Item);
                return RedirectToAction("Index");
            }

            return View("Create", model);
        }

        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Area Area = db.Areas.Find(id);
            if (Area == null)
            {
                return HttpNotFound();
            }
            return View(Area);
        }

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            var area = db.Areas.Find(id);
            Eliminar(area);
            return RedirectToAction("Index");
        }
    }
}

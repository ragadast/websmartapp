﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data.Entity;
using System.Dynamic;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Verdezul.SYSOSP.Web.Areas.Admin.Models;
using Verdezul.SYSOSP.Web.Entities;
using Verdezul.SYSOSP.Web.Helpers;

namespace Verdezul.SYSOSP.Web.Areas.Admin.Controllers
{
    [Authorize(Roles = Constantes.ROL_GESTI + "," + Constantes.ROL_ADMIN)]
    public class ClientesController : AdminController<Cliente>
    {
        public ActionResult Index(string q)
        {
            if (q == null)
                q = "";
            ClienteListViewModel model = new ClienteListViewModel();
            model.Item = db.Clientes.Where(x => x.Status == EstadoEntidad.Activo && (x.Nombre.StartsWith(q) || q == "")).OrderBy(z => z.Nombre).ToList();
            return View(model);
        }

        public ActionResult Create()
        {
            ClienteViewModel model = new ClienteViewModel();
            model.TipoMantenimiento = TipoMantenimiento.Creación;
            model.Item = new Cliente();
            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(ClienteViewModel model)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    db.Clientes.Add(model.Item);
                    db.SaveChanges();

                    if (model.SubmitAction == "Grabar")
                        return RedirectToAction("Index");
                    else
                        return RedirectToAction("Create", "Pedidos", new { cid = model.Item.ID });
                }
                catch (Exception ex)
                {
                    ErrorCliente tipoError = VerificarExcepcion(ex);
                    if (tipoError == ErrorCliente.NoReconocido)
                        throw ex;
                    else
                        ModelState.AddModelError(tipoError.ToString(), tipoError.ToDisplayName());
                }
            }

            model.TipoMantenimiento = TipoMantenimiento.Creación;
            return View(model);
        }

        public ActionResult Edit(int? id)
        {
            if (id == null)
                throw new HttpException(400, "Petición inválida");

            Cliente Cliente = db.Clientes.Include("Pedidos").SingleOrDefault(x => x.ID == id.Value);
            if (Cliente == null)
                throw new HttpException(404, "No existe Cliente");

            ClienteViewModel model = new ClienteViewModel();
            model.TipoMantenimiento = TipoMantenimiento.Modificación;
            model.Item = Cliente;
            return View("Create", model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(ClienteViewModel model)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    db.Entry(model.Item).State = EntityState.Modified;
                    db.SaveChanges();
                    if (model.SubmitAction == "Grabar")
                        return RedirectToAction("Index");
                    else
                        return RedirectToAction("Create", "Pedidos", new { cid = model.Item.ID });
                }
                catch (Exception ex)
                {
                    ErrorCliente tipoError = VerificarExcepcion(ex);
                    if (tipoError == ErrorCliente.NoReconocido)
                        throw ex;
                    else
                        ModelState.AddModelError(tipoError.ToString(), tipoError.ToDisplayName());
                }
            }

            model.TipoMantenimiento = TipoMantenimiento.Modificación;

            return View("Create", model);
        }

        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Cliente Cliente = db.Clientes.Find(id);
            if (Cliente == null)
            {
                return HttpNotFound();
            }
            return View(Cliente);
        }

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Cliente Cliente = db.Clientes.Find(id);
            Cliente.Status = EstadoEntidad.Eliminado;
            //db.Clientes.Remove(Cliente);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        public ActionResult Search(string q)
        {
            BusquedaClienteViewModel model = new BusquedaClienteViewModel();

            var clientesEncontrados = db.Clientes
                .Where(x => x.NumeroIdentificacion.StartsWith(q.TrimEnd()) ||
                            x.Nombre.Contains(q)).OrderBy(x => x.Nombre).ToList();

            if (clientesEncontrados.Count() > 20)
            {
                model.ExistenMasResultados = true;
                clientesEncontrados = clientesEncontrados.Take(20).ToList();
            }

            switch (clientesEncontrados.Count())
            {
                case 0:
                    model.Tipo = TipoResultadoBusqueda.Ninguno;
                    break;
                case 1:
                    model.Tipo = TipoResultadoBusqueda.Unico;
                    model.Item = clientesEncontrados.First();
                    break;
                default:
                    model.Tipo = TipoResultadoBusqueda.Varios;
                    model.Lista = clientesEncontrados.ToList();
                    break;
            }
            return View(model);
        }

        public ActionResult TestIdentificacion()
        {
            var listaResultado = new List<string>().Select(x => new { Nombre = "", NumeroIdentificacion = "", Valido = true, Mensaje = "" }).ToList();
            var clientes = db.Clientes.ToList();
            clientes.ForEach(x => x.Telefono = "t");
            clientes.ForEach(x => x.Direccion = "d");

            foreach (var x in clientes)
            {
                var results = new List<ValidationResult>();
                var valido = Validator.TryValidateObject(x, new ValidationContext(x), results);

                if (!valido)
                {
                    var mensajeError = results.First().ErrorMessage.Replace("Número Identificación no es válido", "");
                    listaResultado.Add(new { Nombre = x.Nombre, NumeroIdentificacion = x.NumeroIdentificacion, Valido = valido, Mensaje = mensajeError });
                }
                //else
                //{
                //    listaResultado.Add(new { Nombre = x.Nombre, NumeroIdentificacion = x.NumeroIdentificacion, Valido = true, Mensaje = "" });
                //}
            }

            dynamic model = new ExpandoObject();
            model.Clientes = listaResultado.OrderBy(x => x.Mensaje).ThenBy(z => z.Nombre);
            return View(model);
        }

        private ErrorCliente VerificarExcepcion(Exception ex)
        {
            if (ex != null && ex.InnerException != null && ex.InnerException.InnerException != null)
            {
                if (ex.InnerException.InnerException.Message.Contains("IX_NumeroIdentificacion"))
                    return ErrorCliente.IdentificacionRepetida;
                if (ex.InnerException.InnerException.Message.Contains("IX_Nombre"))
                    return ErrorCliente.NombreRepetido;
            }
            return ErrorCliente.NoReconocido;
        }
    }
}

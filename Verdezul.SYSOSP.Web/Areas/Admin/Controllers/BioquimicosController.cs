﻿using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web.Mvc;
using Verdezul.SYSOSP.Web.Areas.Admin.Models;
using Verdezul.SYSOSP.Web.Entities;

namespace Verdezul.SYSOSP.Web.Areas.Admin.Controllers
{
    [Authorize(Roles = Constantes.ROL_GESTI)]
    public class BioquimicosController : AdminController<Bioquimico>
    {
        public ActionResult Index(int? id)
        {
            BioquimicoListViewModel model = new BioquimicoListViewModel();

            var bioquimicos = db.Bioquimicos; //.Where(x => x.Status == EstadoEntidad.Activo);
            model.Item = bioquimicos.ToList();
            model.Item.ForEach(bq => bq.ApplicationUser = db.Users.Where(user => bioquimicos.Select(sbq => sbq.UserName).Contains(user.UserName)).Single(suser => suser.UserName == bq.UserName));
            return View(model);
        }

        public ActionResult Create()
        {
            BioquimicoViewModel model = new BioquimicoViewModel();
            model.TipoMantenimiento = TipoMantenimiento.Creación;
            model.Item = new Bioquimico();
            model.UsuariosArea = GetUsuarios();
            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(BioquimicoViewModel model)
        {
            if (ModelState.IsValid)
            {
                db.Bioquimicos.Add(model.Item);
                Grabar();
                return RedirectToAction("Index");
            }

            model.TipoMantenimiento = TipoMantenimiento.Creación;
            return View(model);
        }

        [Authorize(Roles = Constantes.ROL_ADMIN)]
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            Bioquimico Bioquimico = db.Bioquimicos.Find(id);
            if (Bioquimico == null)
            {
                return HttpNotFound();
            }

            BioquimicoViewModel model = new BioquimicoViewModel();
            model.TipoMantenimiento = TipoMantenimiento.Modificación;
            model.Item = Bioquimico;
            model.UsuariosArea = GetUsuarios();
            return View("Create", model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(BioquimicoViewModel model)
        {
            if (ModelState.IsValid)
            {
                Actualizar(model.Item);
                return RedirectToAction("Index");
            }

            return View("Create", model);
        }

        [Authorize(Roles = Constantes.ROL_ADMIN)]
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Bioquimico Bioquimico = db.Bioquimicos.Find(id);
            if (Bioquimico == null)
            {
                return HttpNotFound();
            }
            return View(Bioquimico);
        }

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            var Bioquimico = db.Bioquimicos.Find(id);
            Eliminar(Bioquimico);
            return RedirectToAction("Index");
        }

        private List<string> GetUsuarios()
        {
            var usuariosArea = db.Users.ToList().Select(x => x.UserName).ToList(); // .Select(x => x.NombresCompletos).ToList();
            //var nombresUsuarios = db.Users.Where(x => usuariosArea.Contains(x.Id)).Select(x => x.UserName).Where(x => x != Constantes.USR_SUPAD).ToList();
            usuariosArea.Insert(0, "()");
            return usuariosArea;
        }
    }
}

﻿using System;
using System.Data.Entity;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Verdezul.SYSOSP.Web.Entities;

namespace Verdezul.SYSOSP.Web.Areas.Admin.Controllers
{
    [Authorize]
    public class ArchivosController : AdminController<Archivo>
    {
        [HttpPost]
        public JsonResult Subir(FormCollection formCollection, HttpPostedFileBase file)
        {
            TipoArchivo tipoArchivo = (TipoArchivo)Enum.Parse(typeof(TipoArchivo), formCollection["tipoArchivo"]);

            int IdPedido = Convert.ToInt32(formCollection["IdPedido"]);
            var pedido = db.Pedidos
                .Include("Archivos")
                .Include("Muestras")
                .Single(x => x.ID == IdPedido);

            int IdOrdenTrabajo = 0;
            int IdMuestra = 0;
            int NumeroInforme = 0;

            Archivo archivo = null;
            switch (tipoArchivo)
            {
                case TipoArchivo.Proforma:
                case TipoArchivo.AceptacionCliente:
                    archivo = pedido.Archivos.Single(x => x.Tipo == tipoArchivo);
                    break;
                case TipoArchivo.InformeResultados:
                case TipoArchivo.InformeSuplementario:
                    IdOrdenTrabajo = Convert.ToInt32(formCollection["IdOrdenTrabajo"]);
                    IdMuestra = Convert.ToInt32(formCollection["IdMuestra"]);
                    NumeroInforme = Convert.ToInt32(formCollection["NumeroInforme"]);
                    archivo = pedido.Archivos
                        .Single(x => 
                            x.Tipo == tipoArchivo && 
                            x.IdOrdenTrabajo == IdOrdenTrabajo && 
                            x.IdMuestra == IdMuestra && 
                            x.NumeroInforme == NumeroInforme);
                    break;
                case TipoArchivo.RecepcionMuestra:
                    IdMuestra = Convert.ToInt32(formCollection["IdMuestra"]);
                    archivo = pedido.Archivos.Single(x => x.Tipo == tipoArchivo && x.IdMuestra == IdMuestra);
                    var muestra = pedido.Muestras.Single(x => x.ID == IdMuestra);
                    muestra.Estado = EstadoMuestra.Recibida;
                    muestra.SetDataLog();
                    db.Entry(muestra).State = EntityState.Modified;
                    break;
                case TipoArchivo.OrdenTrabajo:
                    IdOrdenTrabajo = Convert.ToInt32(formCollection["IdOrdenTrabajo"]);
                    archivo = pedido.Archivos.Single(x => x.Tipo == tipoArchivo && x.IdOrdenTrabajo == IdOrdenTrabajo);
                    break;
            }

            var status = 0;

            if (file != null && archivo != null)
            {
                archivo.Subido = true;
                archivo.Extension = Path.GetExtension(file.FileName);
                archivo.Fecha = DateTime.UtcNow;
                archivo.SetDataLog();

                file.SaveAs(Server.MapPath($"{Constantes.CAR_ARCHI}/{archivo.NombreArchivo}"));

                Actualizar(archivo);
            } else
            {
                status = 1;
            }

            return Json(new { status });
        }

        public ActionResult Descargar(int? id)
        {
            Archivo archivo = db.Archivos.SingleOrDefault(x => x.ID == id.Value && x.Subido);
            if (archivo == null || !archivo.Subido)
                throw new HttpException(404, "No existe Archivo o la URL es incorrecta");

            FileInfo fileInfo = new FileInfo(Server.MapPath($"{Constantes.CAR_ARCHI}/{archivo.NombreArchivo}"));

            try
            {
                byte[] fileBytes = System.IO.File.ReadAllBytes(fileInfo.FullName);
                string fileName = $"{archivo.Tipo}_{DateTime.UtcNow:yyyyMMdd_hhmmss}{archivo.Extension}";
                return File(fileBytes, System.Net.Mime.MediaTypeNames.Application.Octet, fileName);
            }
            catch
            {
                throw new Exception("El archivo no existe en el repositorio del Sistema");
            }
        }

        [Authorize(Roles = Constantes.ROL_ELIM_ARCHIVOS)]
        public ActionResult Eliminar(int idArchivo, string accion)
        {
            var archivo = db.Archivos
                .Include("Pedido")
                .Include("Pedido.Muestras")
                .Single(x => x.ID == idArchivo);

            if (archivo == null)
                throw new HttpException(404, "No existe Archivo");

            string archivoBorrar = Server.MapPath($"{Constantes.CAR_ARCHI}/{archivo.NombreArchivo}");

            archivo.Subido = false;
            archivo.Extension = null;
            archivo.Fecha = null;

            switch(archivo.Tipo)
            {
                case TipoArchivo.RecepcionMuestra:
                    var muestra = archivo.Pedido.Muestras.Single(x => x.ID == archivo.IdMuestra);
                    muestra.Estado = EstadoMuestra.Pendiente;
                    muestra.SetDataLog();
                    db.Entry(muestra).State = EntityState.Modified;
                    break;
            }

            Actualizar(archivo);

            System.IO.File.Delete(archivoBorrar);

            return RedirectToAction(accion, "Pedidos", new { area = "Admin", id = archivo.PedidoID });
        }

        public JsonResult PedirSuplementario(int idArchivo)
        {
            var status = 0;
            Archivo archivo = db.Archivos.SingleOrDefault(x => x.ID == idArchivo);

            if (archivo == null)
                status = 100;
            else
            {
                archivo.NecesitaSuplementario = true;
                db.Entry(archivo).State = EntityState.Modified;
                db.SaveChanges();
            }

            return Json(new { status });
        }
    }
}
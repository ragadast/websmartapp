﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web.Mvc;
using Verdezul.SYSOSP.Web.Areas.Admin.Models;
using Verdezul.SYSOSP.Web.Entities;

namespace Verdezul.SYSOSP.Web.Areas.Admin.Controllers
{
    [Authorize(Roles = Constantes.ROL_GESTI + "," + Constantes.ROL_SECRE)]
    public class PlantillasController : AdminController<Plantilla>
    {
        public ActionResult Index()
        {
            PlantillaListViewModel model = new PlantillaListViewModel();
            model.Item = db.Plantillas.Where(x => x.Status == EstadoEntidad.Activo)
                .OrderBy(x => x.Nombre)
                .ToList();
            return View(model);
        }

        public ActionResult Create()
        {
            PlantillaViewModel model = new PlantillaViewModel();
            model.TipoMantenimiento = TipoMantenimiento.Creación;
            model.Item = new Plantilla();
            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(PlantillaViewModel model)
        {
            if (ModelState.IsValid)
            {
                db.Plantillas.Add(model.Item);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            model.TipoMantenimiento = TipoMantenimiento.Creación;
            return View(model);
        }

        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            Plantilla plantilla = db.Plantillas
                .Include("ListaAnalisis")
                .Include("ListaAnalisis.Analisis")
                .Include("ListaAnalisis.Analisis.GrupoAnalisis")
                .Include("ListaAnalisis.Analisis.GrupoAnalisis.Area")
                .SingleOrDefault(x => x.ID == id && x.Status == EstadoEntidad.Activo);

            if (plantilla == null)
            {
                return HttpNotFound();
            }

            PlantillaViewModel model = new PlantillaViewModel();
            model.TipoMantenimiento = TipoMantenimiento.Modificación;
            model.Areas = db.Areas.Where(x => x.Status == EstadoEntidad.Activo).ToList();
            model.CatalogoAnalisis = GetCatalogos();
            model.Item = plantilla;
            return View("Create", model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(FormCollection form)
        {
            int plantillaId = Convert.ToInt32(form["Item.ID"]);
            string nombre = form["Item.Nombre"];

            var plantilla = db.Plantillas
                .Include("ListaAnalisis")
                .FirstOrDefault(x => x.ID == plantillaId);

            var indexString = form["Item.ListaAnalisis.Index"];
            if (indexString == null)
                indexString = "";

            var listaAnalisis = indexString.Split(',').Where(y => y != "")
                .Select(index => new DetallePlantilla
                {
                    AnalisisID = Convert.ToInt32(form[$"Item.ListaAnalisis[{index}].AnalisisID"]),
                    PlantillaID = plantillaId,
                }).ToList();

            db.DetallesPlantilla.RemoveRange(plantilla.ListaAnalisis);
            db.SaveChanges();

            plantilla.Nombre = nombre;

            db.Entry(plantilla).State = EntityState.Modified;
            db.DetallesPlantilla.AddRange(listaAnalisis);
            db.SaveChanges();

            return RedirectToAction("Index");
        }

        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Plantilla plantilla = db.Plantillas.SingleOrDefault(x => x.ID == id);
            if (plantilla == null)
            {
                return HttpNotFound();
            }
            return View(plantilla);
        }

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Plantilla Plantilla = db.Plantillas.Find(id);
            Plantilla.Status = EstadoEntidad.Eliminado;
            //db.Plantilla.Remove(Plantilla);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        public ActionResult Listado(string jsonpyg)
        {
            ListaPlantillas listado = JsonConvert.DeserializeObject<ListaPlantillas>(jsonpyg);

            var p1 = db.DetallesPlantilla
                .Where(x => x.Status == EstadoEntidad.Activo)
                .Where(x => listado.Plantillas.Contains(x.PlantillaID))
                .Select(x => new { analisisid = x.AnalisisID, areaNombre = x.Analisis.GrupoAnalisis.Area.Nombre });

            var p2 = db.Analisis
                .Where(x => x.Status == EstadoEntidad.Activo)
                .Where(x => listado.Grupos.Contains(x.GrupoAnalisisID))
                .Select(x => new { analisisid = x.ID, areaNombre = x.GrupoAnalisis.Area.Nombre });

            var p = p1.Union(p2).ToList();

            return Json(p);
        }

        public class ListaPlantillas
        {
            public ListaPlantillas()
            {
                Plantillas = new List<int>();
                Grupos = new List<int>();
            }
            public List<int> Plantillas { get; set; }
            public List<int> Grupos { get; set; }
        }
    }
}

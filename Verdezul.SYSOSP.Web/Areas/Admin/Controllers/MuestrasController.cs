﻿using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Verdezul.SYSOSP.Web.Areas.Admin.Models;
using Verdezul.SYSOSP.Web.Entities;

namespace Verdezul.SYSOSP.Web.Areas.Admin.Controllers
{
    [Authorize(Roles = Constantes.ROL_SECRE)]
    public class MuestrasController : AdminController<Muestra>
    {
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            Muestra muestra = db.Muestras
                .Include("Pedido")
                .Include("Pedido.Cliente")
                .SingleOrDefault(x => x.ID == id);

            if (muestra == null)
            {
                return HttpNotFound();
            }

            var ordenesSubidasDeMuestra = db.Archivos
                .Where(x => x.PedidoID == muestra.PedidoID && x.Tipo == TipoArchivo.OrdenTrabajo && x.Subido)
                .Select(x => x.IdOrdenTrabajo.Value);

            var ordenesPendientes = muestra.OrdenesPendientes(ordenesSubidasDeMuestra.ToList());

            if (ordenesPendientes)
                throw new HttpException(404, "No se puede ingresar esta muestra. Es posible que falten Ordenes de Trabajo por subir.");

            MuestraViewModel model = new MuestraViewModel();
            model.TipoMantenimiento = TipoMantenimiento.Modificación;
            model.Item = new MuestraHelp(muestra);
            return View("Edit", model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(MuestraViewModel model)
        {
            int muestraID = model.Item.ID;

            var muestra = db.Muestras.SingleOrDefault(x => x.ID == muestraID);

            if (ModelState.IsValid)
            {
                muestra.Descripcion = model.Item.Descripcion;
                muestra.Lote = model.Item.Lote;
                muestra.Contenido = model.Item.Contenido;
                muestra.Codigo = model.Item.Codigo;
                muestra.Observaciones = model.Item.Observaciones;
                muestra.FechaElaboracion = model.Item.FechaElaboracion;
                muestra.FechaVencimiento = model.Item.FechaVencimiento;
                muestra.Estado = EstadoMuestra.Recibida;

                Actualizar(muestra);

                return RedirectToAction("RecibirMuestras", "Pedidos", new { id = model.Item.Pedido.ID });
            }

            var pedido = db.Pedidos.Include("Cliente").SingleOrDefault(x => x.ID == model.Item.Pedido.ID);
            model.Item.Pedido = pedido;
            model.Item.Detalle = muestra.Detalle;
            model.TipoMantenimiento = TipoMantenimiento.Modificación;

            return View("Edit", model);
        }
    }
}

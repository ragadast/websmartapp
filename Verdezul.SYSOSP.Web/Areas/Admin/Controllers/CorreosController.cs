﻿using System.Linq;
using System.Web.Mvc;
using Verdezul.SYSOSP.Web.DAL;
using Verdezul.SYSOSP.Web.Entities;
using Verdezul.SYSOSP.Web.Helpers;

namespace Verdezul.SYSOSP.Web.Areas.Admin.Controllers
{
    [Authorize]
    public class CorreosController : AdminController<Pedido>
    {
        [HttpPost]
        public ActionResult Proforma(int id)
        {
            return EnvioCorreo(id, TipoNotificacion.Proforma, null);
        }

        [HttpPost]
        [Authorize(Roles = Constantes.ROL_FINAN)]
        public ActionResult RecepcionPago(int id)
        {
            return EnvioCorreo(id, TipoNotificacion.PagoReceptado, null);
        }

        [HttpPost]
        [Authorize(Roles = Constantes.ROL_GESTI + "," + Constantes.ROL_SECRE)]
        public ActionResult ResultadosInforme(int id, int idArchivo)
        {
            Archivo archivo = db.Archivos.Single(x => x.ID == idArchivo);

            var tipoNotificacion = TipoNotificacion.InformeResultados;

            switch (archivo.Tipo)
            {
                case TipoArchivo.InformeResultados:
                    tipoNotificacion = TipoNotificacion.InformeResultados;
                    break;
                case TipoArchivo.InformeSuplementario:
                    tipoNotificacion = TipoNotificacion.InformeResultadosSuplementario;
                    break;
            }

            return EnvioCorreo(id, tipoNotificacion, idArchivo);
        }

        private ActionResult EnvioCorreo(int idPedido, TipoNotificacion tipoNotificacion, int? idArchivo)
        {
            var pedido = db.Pedidos
                .Include("Cliente")
                .Include("Fechas")
                .Include("Archivos")
                .Include("Ordenes")
                .Include("Ordenes.Area")
                .SingleOrDefault(x => x.ID == idPedido);

            var parametros = db.Parametros.ToList();

            var UrlPedido = GetUrlPedido(pedido.Codigo);

            var result = SendMailResult.Error;

            switch (tipoNotificacion)
            {
                case TipoNotificacion.Proforma:
                    result = EmailHelper.SendProforma(pedido, parametros, UrlPedido);
                    break;
                case TipoNotificacion.PagoReceptado:
                    result = EmailHelper.SendRecepcionPago(pedido, parametros, UrlPedido);
                    break;
                case TipoNotificacion.InformeResultados:
                case TipoNotificacion.InformeResultadosSuplementario:
                    result = EmailHelper.SendResultados(pedido, parametros, UrlPedido, idArchivo.Value);
                    break;
            }

            if (result == SendMailResult.Enviado)
            {
                EnvioCorreoDAL.GrabarEnvioCorreo(tipoNotificacion, pedido.ID, idArchivo, pedido.Cliente.Nombre, pedido.Cliente.CorreoElectronico);
            }

            return Json(new { status = result.ToDisplayName() });
        }
    }
}

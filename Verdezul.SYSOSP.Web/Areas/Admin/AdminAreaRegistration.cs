﻿using System.Web.Mvc;

namespace Verdezul.SYSOSP.Web.Areas.Admin
{
    public class AdminAreaRegistration : AreaRegistration 
    {
        public override string AreaName 
        {
            get 
            {
                return "Admin";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context) 
        {
            //context.MapRoute(
            //    "Account",
            //    "Admin/Account/{action}/{id}",
            //    new { controller = "Account", action = "Index", area = "Admin", id = UrlParameter.Optional },
            //    new[] { "Verdezul.SYSOSP.Web.Areas.Admin.Controllers" }
            //    );

            context.MapRoute(
                "Admin_default",
                "Admin/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional },
                new[] { "Verdezul.SYSOSP.Web.Areas.Admin.Controllers" }
            );
        }
    }
}
﻿namespace Verdezul.SYSOSP.Web
{
    public static class Mensajes
    {
        public const string MEN_GRABAR_PEDIDO =
            "ATENCIÓN: Algunos campos presentan errores previo a ser guardados. Verifíquelos.";

        public const string MEN_ENVIAR_NOTIF_PROFORMA = "Correo Electrónico de Proforma Enviado";
        public const string MEN_ENVIAR_NOTIF_PAGO = "Notificación de Pago enviada a Correo Electrónico";
        public const string MEN_ENVIAR_NOTIF_RESUL = "Notificación de Informe de Resultados enviada a Correo Electrónico";
        public const string MEN_ENVIAR_NOTIF_RESUL_SUPL = "Notificación de Informe Suplementario de Resultados enviada a Correo Electrónico";
        public const string MEN_PEDIR_SUPLEMENTARIO = "Se ha enviado al Área correspondiente la petición de subir un informe suplementario.";

        public const string MEN_ERROR_SINCORREO = "El cliente no tiene un correo electrónico válido ";
        public const string MEN_ERROR_SINSOLICITUD = "Se necesita un archivo para la solicitud";
        public const string MEN_ERROR_SINMUESTRAS = "Se necesita al menos una muestra";
        public const string MEN_ERROR_SINANALISISAMUESTRA = "La muestra necesita al menos un análisis";
        public const string MEN_ERROR_SINANALISIS = "Se necesita agregar análisis a alguna muestra";
        public const string MEN_ERROR_TIEMPOENTREGA0 = "Tiempo de entrega debe ser mayor que 0";
        public const string MEN_ERROR_CANTIINFORMES0 = "Al menos debe haber un informe";

        public const string MEN_ERROR_SINARCHIVOADUPLICAR = "No hay archivo del pedido anterior que se pueda duplicar";

    }
}
 
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Verdezul.SYSOSP.Web.Environment
{
    public class Server
    {
        public static ServerEnvironment GetServerEnvironment()
        {
            var siteName = System.Web.Hosting.HostingEnvironment.SiteName;
            switch (siteName)
            {
                case Constantes.SiteName_Pro:
                    return ServerEnvironment.Produccion;
                case Constantes.SiteName_Test:
                    return ServerEnvironment.Testing;
                case Constantes.SiteName_Des:
                    return ServerEnvironment.Desarrollo;
                default:
                    return ServerEnvironment.Interno;
            }
        }
    }

    public enum ServerEnvironment
    {
        Produccion = 1,
        Testing = 2,
        Desarrollo = 3,
        Interno = 4
    }
}
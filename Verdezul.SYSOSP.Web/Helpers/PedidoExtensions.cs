﻿using System.Linq;
using Verdezul.SYSOSP.Web.Entities;

namespace Verdezul.SYSOSP.Web.Helpers
{
    public static class PedidoExtensions
    {
        public static AnalisisMuestra ToAnalisisMuestra(this AnalisisMuestraHelp amh)
        {
            return new AnalisisMuestra
            {
                AnalisisID = amh.AnalisisID,
                MuestraID = amh.MuestraID,
                Cantidad = amh.Cantidad,
                Nombre = amh.Nombre,
                Tecnica = amh.Tecnica,
                Valor = amh.Valor,
                TieneIVA = amh.TieneIVA,
                AcreditacionSAE = amh.AcreditacionSAE,
                Monitoreo = amh.Monitoreo,
                Ticks = amh.Ticks,
                SubAnalisisMuestra = amh.SubAnalisisMuestra
            };
        }

        public static void Completar(this AnalisisMuestraHelp amh, Analisis analisis)
        {
            amh.AreaID = analisis.GrupoAnalisis.AreaID;
            amh.Nombre = analisis.Nombre;
            amh.Tecnica = analisis.Tecnica;
            amh.Valor = analisis.Valor;
            amh.TieneIVA = analisis.TieneIVA;
            amh.AcreditacionSAE = analisis.AcreditacionSAE;
            amh.Monitoreo = analisis.Monitoreo;
            amh.BioquimicoID = analisis.GrupoAnalisis.Area.BioquimicoID;
            amh.SubAnalisisMuestra = analisis.SubAnalisis.Select(x => new SubAnalisisMuestra { Nombre = x.Nombre }).ToList();
        }
    }
}

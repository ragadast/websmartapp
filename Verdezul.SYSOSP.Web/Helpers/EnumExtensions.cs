﻿using System.ComponentModel.DataAnnotations;
using System.Linq;

namespace Verdezul.SYSOSP.Web.Helpers
{
    public static class EnumExtensions
    {
        public static string ToDisplayName<T>(this T enumItem) where T : struct
        {
            var displayAttributes = typeof(T)
                .GetMember(enumItem.ToString())[0]
                .GetCustomAttributes(typeof(DisplayAttribute), false);

            return (displayAttributes.Count() > 0) ? ((DisplayAttribute)displayAttributes[0]).Name : enumItem.ToString();
        }

        public static string ToDisplayShortName<T>(this T enumItem) where T : struct
        {
            var displayAttributes = typeof(T)
                .GetMember(enumItem.ToString())[0]
                .GetCustomAttributes(typeof(DisplayAttribute), false);

            return (displayAttributes.Count() > 0) ? ((DisplayAttribute)displayAttributes[0]).ShortName ?? enumItem.ToString() : enumItem.ToString();
        }
    }
}
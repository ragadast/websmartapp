﻿using System;
using Verdezul.SYSOSP.Web.Entities;

namespace Verdezul.SYSOSP.Web.Helpers
{
    public static class TimeZoneExtensions
    {
        public static DateTime ToEcuadorDate(this DateTime fechaUTC)
        {
            return TimeZoneInfo.ConvertTimeFromUtc(fechaUTC, Constantes.TimeZoneEcuador);
        }

        public static DateTime TransformarFecha(this string ddMMyyyy, bool inicioDia)
        {
            int y = Convert.ToInt32(ddMMyyyy.Substring(4, 4));
            int m = Convert.ToInt32(ddMMyyyy.Substring(2, 2));
            int d = Convert.ToInt32(ddMMyyyy.Substring(0, 2));
            return (inicioDia ?
                new DateTime(y, m, d, 0, 0, 1) :
                new DateTime(y, m, d, 23, 59, 59))
                .ToUniversalTime();
        }

        public static String ToString(this DateTime fecha, TipoSalida tipoSalida)
        {
            switch (tipoSalida)
            {
                case TipoSalida.Fecha:
                    return fecha.ToString("dd/MM/yyyy");
                case TipoSalida.FechaHoraMinuto:
                    return fecha.ToString("dd/MM/yyyy HH:mm");
                case TipoSalida.FechaHoraMinutoSegundo:
                    return fecha.ToString("dd/MM/yyyy HH:mm:ss");
                default:
                    return fecha.ToString("dd/MM/yyyy");
            }
        }

        public static DateTime EcuadorNow { get { return DateTime.UtcNow.ToEcuadorDate(); } }
    }
}
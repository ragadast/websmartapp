﻿using System.Web;

namespace Verdezul.SYSOSP.Web.Helpers
{
    public static class SYSOSPHtmlHelper
    {
        public static IHtmlString RawTableBegin(int? idArea)
        {
            return new HtmlString(string.Format("<table class='table analisisDisponibles area_{0}'>", idArea));
        }

        public static IHtmlString RawTableEnd()
        {
            return new HtmlString("</table>");
        }
    }
}
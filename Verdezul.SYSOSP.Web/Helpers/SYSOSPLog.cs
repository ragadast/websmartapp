﻿using System;
using System.Diagnostics;
using System.IO;
using System.Web;

namespace Verdezul.SYSOSP.Web.Log
{
    public class SYSOSPLog
    {
        public static void Grabar(object obj, Exception ex)
        {
            string fecha = System.DateTime.UtcNow.ToString("yyyyMMdd");
            string hora = System.DateTime.UtcNow.ToString("HH:mm:ss");
            string path = HttpContext.Current.Request.MapPath("~/log/" + fecha + ".txt");

            StreamWriter sw = new StreamWriter(path, true);

            StackTrace stacktrace = new StackTrace();
            sw.WriteLine(obj.GetType().FullName + " " + hora);
            sw.WriteLine(stacktrace.GetFrame(1).GetMethod().Name + " - " + ex.Message);
            sw.WriteLine("");

            sw.Flush();
            sw.Close();
        }
    }
}
﻿using MailKit.Net.Smtp;
using MailKit.Security;
using MimeKit;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net.Configuration;
using Verdezul.SYSOSP.Web.Entities;
using Verdezul.SYSOSP.Web.Environment;
using Verdezul.SYSOSP.Web.Log;
using Verdezul.SYSOSP.Web.Helpers;

namespace Verdezul.SYSOSP.Web.Helpers
{
    public class EmailHelper
    {
        public static SendMailResult SendProforma(Pedido pedido, List<Parametro> parametros, String UrlPedido)
        {
            if (pedido.Cliente.CorreoElectronico == null || pedido.Cliente.CorreoElectronico == "")
                return SendMailResult.DireccionInvalida;

            var tipoNotificacion = TipoNotificacion.Proforma;

            string asunto = $"Proforma {pedido.NumeroProforma} : {tipoNotificacion.ToDisplayName()}";

            string cuerpo = PlantillaBasica(pedido, parametros, tipoNotificacion, UrlPedido, null);

            return EmailHelper.SendMail(cuerpo, new MailboxAddress(pedido.Cliente.Nombre, pedido.Cliente.CorreoElectronico), asunto);
        }

        public static SendMailResult SendResultados(Pedido pedido, List<Parametro> parametros, String UrlPedido, int idArchivo)
        {
            if (pedido.Cliente.CorreoElectronico == null || pedido.Cliente.CorreoElectronico == "")
                return SendMailResult.DireccionInvalida;

            var tipoNotificacion = TipoNotificacion.InformeResultados;

            Archivo archivoInforme = null;

            if (idArchivo > 0)
            {
                archivoInforme = pedido.Archivos.SingleOrDefault(x => x.ID == idArchivo);
                if (archivoInforme != null)
                {
                    switch(archivoInforme.Tipo)
                    {
                        case TipoArchivo.InformeResultados:
                            tipoNotificacion = TipoNotificacion.InformeResultados;
                            break;
                        case TipoArchivo.InformeSuplementario:
                            tipoNotificacion = TipoNotificacion.InformeResultadosSuplementario; 
                            break;
                    }
                }
                else
                    return SendMailResult.ArchivoInexistente;
            }
            
            string asunto = $"Proforma {pedido.NumeroProforma} : {tipoNotificacion.ToDisplayName()}";

            string cuerpo = PlantillaBasica(pedido, parametros, tipoNotificacion, UrlPedido, archivoInforme);

            return EmailHelper.SendMail(cuerpo, new MailboxAddress(pedido.Cliente.Nombre, pedido.Cliente.CorreoElectronico), asunto);
        }

        public static SendMailResult SendRecepcionPago(Pedido pedido, List<Parametro> parametros, String UrlPedido)
        {
            if (pedido.Cliente.CorreoElectronico == null || pedido.Cliente.CorreoElectronico == "")
                return SendMailResult.DireccionInvalida;

            var tipoNotificacion = TipoNotificacion.PagoReceptado;

            string asunto = $"Proforma {pedido.NumeroProforma} : {tipoNotificacion.ToDisplayName()}";

            string cuerpo = PlantillaBasica(pedido, parametros, tipoNotificacion, UrlPedido, null);

            return EmailHelper.SendMail(cuerpo, new MailboxAddress(pedido.Cliente.Nombre, pedido.Cliente.CorreoElectronico), asunto);
        }

        public static SendMailResult SendMail(string cuerpo, MailboxAddress to, string subject)
        {
            bool UsarCorreoVerificacion = Server.GetServerEnvironment() != ServerEnvironment.Produccion;

            var builder = new BodyBuilder();
            builder.HtmlBody = cuerpo;

            var mailMessage = new MimeMessage
            {
                Subject = UsarCorreoVerificacion ? $"{Server.GetServerEnvironment()}-Laboratorio OSP - {subject}" : $"Laboratorio OSP - {subject}",
                Body = builder.ToMessageBody(),
                Priority = MessagePriority.Normal
            };

            if (UsarCorreoVerificacion)
                to.Address = ConfigurationManager.AppSettings["CorreoVerificacion"];

            SmtpSection smtpSection = (SmtpSection)ConfigurationManager.GetSection("system.net/mailSettings/smtp");

            string from = smtpSection.From;
            string host = smtpSection.Network.Host;
            int port = smtpSection.Network.Port;
            string userName = smtpSection.Network.UserName;
            string password = smtpSection.Network.Password;

            mailMessage.From.Add(new MailboxAddress(from, userName));
            mailMessage.To.Add(to);

            using (var smtpClient = new SmtpClient())
            {
                try
                {
                    smtpClient.CheckCertificateRevocation = false;
                    smtpClient.Connect(host, port, SecureSocketOptions.StartTls);
                    smtpClient.Authenticate(userName, password);
                    smtpClient.Send(mailMessage);
                    smtpClient.Disconnect(true);
                    return SendMailResult.Enviado;
                }
                catch (Exception ex)
                {
                    SYSOSPLog.Grabar(smtpClient, ex);
                    return SendMailResult.Error;
                }
            }
        }

        public static string PlantillaBasica(Pedido pedido, List<Parametro> parametros, TipoNotificacion tipoNotificacion, string UrlPedido, Archivo archivoInforme)
        {
            var parametroCorreoSYSOSP = parametros.SingleOrDefault(x => x.Tipo == TipoParametro.Correo);
            var CorreoSYSOSP = parametroCorreoSYSOSP != null ? parametroCorreoSYSOSP.Valor : "fcq.osp@uce.edu.ec";

            var plantilla = CabeceraCorreo(pedido.Cliente.Nombre);

            switch (tipoNotificacion)
            {
                case TipoNotificacion.Proforma:
                    plantilla += $@"
<span>Le informamos que se encuentra EMITIDA la PROFORMA No. {pedido.NumeroProforma}</span><br /><br />
<span>Para descargarla revise la siguiente dirección:</span><br /><br />
<a href='{UrlPedido}'>{UrlPedido}</a><br /><br />
<span>Por favor revisar las CONDICIONES DE SERVICIO y el PROCEDIMIENTO PARA EL PAGO Y ENTREGA DE MUESTRAS, información que se encuentra al final del documento.</span><br />";
                    break;

                case TipoNotificacion.PagoReceptado:

                    var fechaPago = pedido.Fechas.SingleOrDefault(x => x.Estado == EstadoPedido.Pagado);

                    string fecha = fechaPago != null ? fechaPago.EntityDate.ToEcuadorDate().ToString(TipoSalida.FechaHoraMinuto) : "";

                    plantilla += $@"
<span>Le informamos que se ha REGISTRADO EL PAGO segun el siguiente detalle:</span><br />
<ul>
    <li>Proforma: <b>{pedido.NumeroProforma}</b></li>
    <li>Fecha: <b>{fecha}</b></li>
    <li>Pedido: <b>{pedido.NumeroPedido}</b></li>
</ul>
<span>Por favor acérquese al Laboratorio OSP a entregar la muestra respectiva.</span><br /><br />
<span>Cuando se genere la factura electrónica se le notificará a su correo.</span><br />";
                    break;

                case TipoNotificacion.InformeResultados:
                case TipoNotificacion.InformeResultadosSuplementario:

                    plantilla += $@"
<span>Le informamos que se encuentra disponible el {tipoNotificacion.ToDisplayName().ToUpper()} según el siguiente detalle:</span><br />
<ul>
    <li>Proforma: <b>{pedido.NumeroProforma}</b></li>
    <li>Pedido: <b>{pedido.NumeroPedido}</b></li>";

                    if (archivoInforme != null)
                    {
                        var muestraInforme = pedido.Muestras.SingleOrDefault(x => x.ID == archivoInforme.IdMuestra);

                        var orderInforme = pedido.Ordenes.SingleOrDefault(x => x.ID == archivoInforme.IdOrdenTrabajo);

                        plantilla += $@"
    <!-- <li>{archivoInforme.Tipo.ToDisplayName()} {muestraInforme.Numero}: {muestraInforme.Detalle}</li> -->
    <li>Area: <b>{orderInforme.Area.Nombre}</b></li>
    <!-- <li>Número de Informe: <b>{archivoInforme.NumeroInforme}</b></li> -->";
                    }

                    plantilla += $@"
</ul>
<span>Para descargarlo le pedimos que ingrese a la siguiente dirección:</span><br /><br />
<a href='{UrlPedido}'>{UrlPedido}</a><br /><br />";
                    break;
            }

            plantilla += PieCorreo(CorreoSYSOSP);

            return plantilla;
        }

        public static string PlantillaTest()
        {
            return @"
<html>
<body>
    <h3>Probando Correo</h3>
    <div>Este es un correo de prueba.</div>
    <div>Si le llegó es por error, por favor notifique a SYSOSP.</div>
    <div>Gracias</div>
</body>
</html>
";
        }

        public static string ForgotPasswordEmail(DateTime fecha, string usuario, string link)
        {
            return $@"
<html>
<body>
    Estimado Usuario:<br />
    <br />
    Hemos recibido una petición para resetear la contraseña.<br />
    <br />
    Usuario: {usuario}<br />
    Fecha: {fecha.ToString(TipoSalida.FechaHoraMinutoSegundo)}<br />
    <br />
    Para hacerlo por favor ingresae al siguiente enlance.<br />
    <a href='{link}'>{link}</a><br />
    <br />
    Si Ud. no realizó esta petición, ignore este correo.<br />
    <br />   
</body>
</html>
";
        }

        public static string CabeceraCorreo(string NombreCliente)
        {
            return $@"
<html>
<body>
    Estimado/a {NombreCliente}:<br /><br />
    Un saludo cordial de parte del equipo del Laboratorio OSP.<br /><br />";

        }

        public static string PieCorreo(string CorreoSYSOSP)
        {
            return $@"<br />
    Estamos atentos a cualquier duda que pueda surgir.<br /><br />
    Nuestro correo oficial es: <a href='mailto:{CorreoSYSOSP}'>{CorreoSYSOSP}</a>.<br /><br />
    Saludos Cordiales.<br /> <br />
    El equipo de <a href='http://sysosp.com'>Laboratorio OSP.</a><br />
</body>
</html>
";
        }
    }

    public enum SendMailResult
    {
        Enviado = 0,
        DireccionInvalida = 1,
        ArchivoInexistente = 2,
        Error = 3
    }
}
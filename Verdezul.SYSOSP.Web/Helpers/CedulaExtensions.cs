﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using Verdezul.SYSOSP.Web.Entities;

namespace Verdezul.SYSOSP.Web.Helpers
{
    public static class CedulaExtensions
    {
        public static List<int> CoeficientesRUCNatural = new List<int> { 2, 1, 2, 1, 2, 1, 2, 1, 2 };
        public static List<int> CoeficientesRUCSectPub = new List<int> { 3, 2, 7, 6, 5, 4, 3, 2 };
        public static List<int> CoeficientesRUCSocieda = new List<int> { 4, 3, 2, 7, 6, 5, 4, 3, 2 };
        public static int numeroProvincias = 24;

        /// <summary>
        /// Validador de Cédula y RUC en Ecuador
        /// https:medium.com/@bryansuarez/c%C3%B3mo-validar-c%C3%A9dula-y-ruc-en-ecuador-b62c5666186f
        /// </summary>
        /// <param name = "numeroIdentificacion" ></param>
        /// <param name="tipo"></param>
        /// <returns></returns>
        public static ErrorValidacionNumeroIdentificacion ValidarIdentificacion(this string numeroIdentificacion, TipoIdentificacion tipo)
        {
            if (tipo == TipoIdentificacion.Pasaporte)
                return ErrorValidacionNumeroIdentificacion.Correcto;

            if (tipo == TipoIdentificacion.Cédula && numeroIdentificacion.Length != 10)
                return ErrorValidacionNumeroIdentificacion.LongitudCedula;

            if ((tipo == TipoIdentificacion.RUC || tipo == TipoIdentificacion.RUCEspecial) && numeroIdentificacion.Length != 13)
                return ErrorValidacionNumeroIdentificacion.LongitudRUC;

            if (tipo == TipoIdentificacion.RUCEspecial)
                return ErrorValidacionNumeroIdentificacion.Correcto;

            Int64 ni = 0;
            if (!Int64.TryParse(numeroIdentificacion, out ni))
                return ErrorValidacionNumeroIdentificacion.CaracteresNoValidos;

            int numeroProvincia = Convert.ToInt32(numeroIdentificacion.Substring(0, 2));
            if (numeroProvincia > numeroProvincias || numeroProvincia < 1)
                return ErrorValidacionNumeroIdentificacion.ProvinciaNoValida;

            var numeros = numeroIdentificacion.ToArray().Select(y => Convert.ToInt32(y.ToString()));
            switch (tipo)
            {
                case TipoIdentificacion.Cédula:

                    var residuo10C = numeros.Take(9)
                            .Select((x, y) => x * CoeficientesRUCNatural[y])
                            .Select(x => (x > 9 ? x - 9 : x))
                            .Sum() % 10;

                    return ((((residuo10C == 0) ? 0 : 10 - residuo10C) == numeros.Skip(9).Take(1).Single()) ?
                        ErrorValidacionNumeroIdentificacion.Correcto :
                        ErrorValidacionNumeroIdentificacion.DigitoVerificadorCedulaInvalido);

                case TipoIdentificacion.RUC:

                    var digitoIntermedio = numeros.Skip(2).Take(1).Single();

                    switch (digitoIntermedio)
                    {
                        case 0:
                        case 1:
                        case 2:
                        case 3:
                        case 4:
                        case 5:
                            // RUC Persona Natural
                            if (numeroIdentificacion.Substring(10) != "001")
                                return ErrorValidacionNumeroIdentificacion.RUCNaturalInvalido;

                            var a = numeroIdentificacion.Substring(0, 10).ValidarIdentificacion(TipoIdentificacion.Cédula);
                            if (a != ErrorValidacionNumeroIdentificacion.Correcto)
                                a = ErrorValidacionNumeroIdentificacion.DigitoVerificadorRUCNaturalInvalido;

                            return a;
                        case 6:
                            // RUC Publico
                            if (numeroIdentificacion.Substring(9) != "0001")
                                return ErrorValidacionNumeroIdentificacion.RUCPublicoInvalido;

                            var residuo11RP = numeros.Take(8)
                                    .Select((x, y) => x * CoeficientesRUCSectPub[y])
                                    .Sum() % 11;

                            return (((residuo11RP == 0) ? 0 : 11 - residuo11RP) == numeros.Skip(8).Take(1).Single()) ?
                                ErrorValidacionNumeroIdentificacion.Correcto :
                                ErrorValidacionNumeroIdentificacion.DigitoVerificadorRUCPublicoInvalido;
                        case 9:
                            // RUC Sociedad
                            if (numeroIdentificacion.Substring(10, 1) != "0")
                                return ErrorValidacionNumeroIdentificacion.RUCSociedadInvalido;

                            var residuo11RS = numeros.Take(9)
                                    .Select((x, y) => x * CoeficientesRUCSocieda[y])
                                    .Sum() % 11;

                            return (((residuo11RS == 0) ? 0 : 11 - residuo11RS) == numeros.Skip(9).Take(1).Single()) ?
                                ErrorValidacionNumeroIdentificacion.Correcto :
                                ErrorValidacionNumeroIdentificacion.DigitoVerificadorRUCSociedadInvalido;
                        default:
                            return ErrorValidacionNumeroIdentificacion.Desconocido;
                    }
            }

            return ErrorValidacionNumeroIdentificacion.Desconocido;
        }
    }

    public enum ErrorValidacionNumeroIdentificacion
    {
        Correcto = 0,

        [Display(Name = "Digitio Verificador de Cédula no coincide")]
        DigitoVerificadorCedulaInvalido = 1,
        [Display(Name = "Digitio Verificador de RUC-N no coincide")]
        DigitoVerificadorRUCNaturalInvalido = 2,
        [Display(Name = "Digitio Verificador de RUC-P no coincide")]
        DigitoVerificadorRUCPublicoInvalido = 3,
        [Display(Name = "Digitio Verificador de RUC-S no coincide")]
        DigitoVerificadorRUCSociedadInvalido = 4,

        [Display(Name = "Longitud de Cédula debe ser de 10")]
        LongitudCedula = 11,
        [Display(Name = "Longitud de RUC debe ser de 13")]
        LongitudRUC = 12,
        [Display(Name = "Cédula o RUC sólo deben tener números")]
        CaracteresNoValidos = 13,
        [Display(Name = "Provincia no válida")]
        ProvinciaNoValida = 14,

        [Display(Name = "RUC-N no tiene el formato esperado")]
        RUCNaturalInvalido = 21,
        [Display(Name = "RUC-P no tiene el formato esperado")]
        RUCPublicoInvalido = 22,
        [Display(Name = "RUC-S no tiene el formato esperado")]
        RUCSociedadInvalido = 23,
        [Display(Name = "No es un número valido")]
        Desconocido = 30
    }
}
﻿using iTextSharp.text.pdf;
using System;
using System.Collections.Generic;
using System.IO;
using System.Security.Cryptography;
using System.Security.Cryptography.Pkcs;
using SysX509 = System.Security.Cryptography.X509Certificates;

namespace Verdezul.SYSOSP.Web.Helpers
{
    public static class SigningPDF
    {
        private static int intCSize = 4000;

        public static PdfSignatureAppearance CrearFirmaPDF(string source, string target, string motivo, string ubicacion)
        {
            PdfReader objReader = new PdfReader(source);
            FileStream objFileStream = new FileStream(target, FileMode.Create);
            PdfStamper objStamper = PdfStamper.CreateSignature(objReader, objFileStream, '\0');

            PdfSignatureAppearance apariencia = objStamper.SignatureAppearance;
            apariencia.SetVisibleSignature(new iTextSharp.text.Rectangle(0, 0, 200, 50), 1, null);
            apariencia.SignDate = DateTime.Now;
            apariencia.Reason = motivo;
            apariencia.Location = ubicacion;
            apariencia.Acro6Layers = true;
            apariencia.CryptoDictionary = new PdfSignature(PdfName.ADOBE_PPKMS, PdfName.ADBE_PKCS7_SHA1)
            {
                Date = new PdfDate(apariencia.SignDate),
                Reason = motivo,
                Location = ubicacion
            };

            apariencia.PreClose(new Dictionary<PdfName, int>
            {
                [PdfName.CONTENTS] = intCSize * 2 + 2
            });

            return apariencia;
        }

        public static void FirmarPDF(PdfSignatureAppearance apariencia, string fileName, string password)
        {
            HashAlgorithm objSHA1 = new SHA1CryptoServiceProvider();

            Stream objStream = apariencia.GetRangeStream();

            int intRead = 0;
            byte[] bytBuffer = new byte[8192];
            while ((intRead = objStream.Read(bytBuffer, 0, 8192)) > 0)
                objSHA1.TransformBlock(bytBuffer, 0, intRead, bytBuffer, 0);
            objSHA1.TransformFinalBlock(bytBuffer, 0, 0);

            SysX509.X509Certificate2 certificado = new SysX509.X509Certificate2(fileName, password);
            byte[] bytPK = SignMsg(objSHA1.Hash, certificado, false);
            byte[] bytOut = new byte[intCSize];
            Array.Copy(bytPK, 0, bytOut, 0, bytPK.Length);

            PdfDictionary objDict = new PdfDictionary();
            objDict.Put(PdfName.CONTENTS, new PdfString(bytOut).SetHexWriting(true));
            apariencia.Close(objDict);
        }

        private static byte[] SignMsg(byte[] Message, SysX509.X509Certificate2 SignerCertificate, bool Detached)
        {
            ContentInfo contentInfo = new ContentInfo(Message);
            SignedCms objSignedCms = new SignedCms(contentInfo, Detached);
            CmsSigner objCmsSigner = new CmsSigner(SignerCertificate);
            objCmsSigner.IncludeOption = SysX509.X509IncludeOption.EndCertOnly;
            objSignedCms.ComputeSignature(objCmsSigner, false);
            return objSignedCms.Encode();
        }
    }
}

﻿using System.Web.Mvc;
using System.Web.Routing;

namespace Verdezul.SYSOSP.Web
{
    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");

            routes.MapRoute(
                name: "ResultadoDescarga",
                url: "Resultado/Descargar/{i}",
                defaults: new { controller = "Resultado", action = "Descargar", i = UrlParameter.Optional }
            );

            routes.MapRoute(
                name: "ResultadoLista",
                url: "Resultado/{i}",
                defaults: new { controller = "Resultado", action = "Index", i = UrlParameter.Optional }
            );

            routes.MapRoute(
                name: "Default",
                url: "{controller}/{action}/{id}",
                defaults: new { controller = "Home", action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}

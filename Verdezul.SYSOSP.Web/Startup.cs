﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(Verdezul.SYSOSP.Web.Startup))]
namespace Verdezul.SYSOSP.Web
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}

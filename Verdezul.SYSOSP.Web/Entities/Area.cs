﻿using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Verdezul.SYSOSP.Web.Entities
{
    [Table("Areas")]
    public class Area : Entidad
    {
        public Area()
        {
            GruposAnalisis = new List<GrupoAnalisis>();
        }

        [Required(ErrorMessage = "{0} es requerido")]
        [MaxLength(32)]
        public string Nombre { get; set; }

        [Required(ErrorMessage = "Bioquímico es requerido")]
        public int BioquimicoID { get; set; }
        [DisplayName("Bioquímico")]
        public virtual Bioquimico Bioquimico { get; set; }

        public virtual ICollection<GrupoAnalisis> GruposAnalisis { get; set; }

        public virtual ICollection<OrdenTrabajo> OrdenesTrabajo { get; set; }

        public string ObjetoJS { get { return string.Format("{0} nombre : '{2}', grupo : '{3}', nivel : 1 {1}", "{", "}", this.Nombre.Replace("'", ""), ""); } }
    }
}
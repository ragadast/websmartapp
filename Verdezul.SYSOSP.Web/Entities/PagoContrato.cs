﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Verdezul.SYSOSP.Web.Entities
{
    [Table("PagosContrato")]
    public class PagoContrato : Entidad
    {
        public PagoContrato()
        {
            Pedidos = new List<Pedido>();
        }

        [Required(ErrorMessage = "{0} es requerido")]
        public DateTime Fecha { get; set; }

        [Required(ErrorMessage = "{0} es requerido")]
        public decimal Valor { get; set; }

        [Required(ErrorMessage = "{0} es requerido")]
        public int ClienteID { get; set; }
        public Cliente Cliente { get; set; }

        [Required(ErrorMessage = "{0} es requerido")]
        [DisplayName("Forma de Pago")]
        public FormaPago FormaPago { get; set; }

        [Required(ErrorMessage = "{0} es requerido")]
        [DisplayName("Información de Pago")]
        [MaxLength(16)]
        public string InformacionPago { get; set; }

        public virtual ICollection<Pedido> Pedidos { get; set; }

        public string NombreContrato { get { return $"{Fecha}-{InformacionPago}"; } }

        public override IEnumerable<ValidationResult> Validate(ValidationContext validationContext)
        {
            if (((int)FormaPago) == 0)
            {
                yield return new ValidationResult("Forma de Pago es requerido", new[] { "FormaPago" }); /* TODO: Verificar la condición de pago */
            }
        }
    }
}
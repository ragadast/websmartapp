﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Verdezul.SYSOSP.Web.Entities
{
    [Table("Analisis")]
    public class Analisis : Entidad
    {
        public Analisis()
        {
            SubAnalisis = new List<SubAnalisis>();
        }

        [Required(ErrorMessage = "{0} es requerido")]
        [MaxLength(128)]
        public string Nombre { get; set; }

        [DisplayName("Técnica")]
        [MaxLength(128)]
        public string Tecnica { get; set; }

        [Required(ErrorMessage = "{0} es requerido")]
        public decimal Valor { get; set; }

        [Required(ErrorMessage = "{0} es requerido")]
        [DisplayName("Tiene IVA")]
        public Boolean TieneIVA { get; set; }

        [DisplayName("Acreditación SAE")]
        [Required(ErrorMessage = "{0} es requerida")]
        public Boolean AcreditacionSAE { get; set; }

        [Required(ErrorMessage = "{0} es requerido")]
        public Boolean Monitoreo { get; set; }

        [Required(ErrorMessage = "{0} es requerido")]
        public Boolean TieneSubAnalisis { get; set; }

        [DisplayName("Grupo")]
        [Required(ErrorMessage = "{0} es requerido")]
        public int GrupoAnalisisID { get; set; }
        public GrupoAnalisis GrupoAnalisis { get; set; }

        public virtual ICollection<SubAnalisis> SubAnalisis { get; set; }

        public string ObjetoJS
        {
            get
            {
                return string.Format("{0} nombre : '{2}', grupo : '{3}', area: '{4}', nivel : 3 {1}", 
                    "{", 
                    "}", 
                    this.Nombre.Replace("'", ""), 
                    this.GrupoAnalisis.Nombre,
                    this.GrupoAnalisis.Area.Nombre);
            }
        }
    }

    [Table("SubAnalisis")]
    public class SubAnalisis : Entidad
    {
        [Required(ErrorMessage = "{0} es requerido")]
        [MaxLength(128)]
        public string Nombre { get; set; }

        [DisplayName("Análisis")]
        [Required(ErrorMessage = "{0} es requerido")]
        public int AnalisisID { get; set; }
        public Analisis Analisis { get; set; }
    }
}
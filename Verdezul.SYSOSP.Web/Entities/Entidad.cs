﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Web;

namespace Verdezul.SYSOSP.Web.Entities
{
    public class Entidad : IValidatableObject
    {
        protected Entidad()
        {
            Status = EstadoEntidad.Activo;
            SetDataLog();
        }

        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int ID { get; set; }
        public DateTime EntityDate { get; set; }
        public EstadoEntidad Status { get; set; }
        [MaxLength(32)]
        public string User { get; set; }

        public void SetDataLog()
        {
            EntityDate = DateTime.UtcNow;

            if (HttpContext.Current != null && HttpContext.Current.User != null && HttpContext.Current.User.Identity.IsAuthenticated)
                User = HttpContext.Current.User.Identity.Name;
        }

        public virtual IEnumerable<ValidationResult> Validate(ValidationContext validationContext)
        {
            yield return null;
        }
    }
}
﻿using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Verdezul.SYSOSP.Web.Entities
{
    [Table("GruposAnalisis")]
    public class GrupoAnalisis : Entidad
    {
        public GrupoAnalisis()
        {
            Analisis = new List<Analisis>();
        }

        [Required(ErrorMessage = "{0} es requerido")]
        [MaxLength(64)]
        public string Nombre { get; set; }

        [Required(ErrorMessage = "Área es requerida")]
        [DisplayName("Área")]
        public int AreaID { get; set; }
        public Area Area { get; set; }

        public virtual ICollection<Analisis> Analisis { get; set; }

        public string ObjetoJS { get { return string.Format("{0} nombre : '{2}', grupo : '{3}', nivel : 2 {1}", "{", "}", this.Nombre.Replace("'", ""), this.Area.Nombre); } }
    }
}
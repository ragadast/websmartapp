﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Verdezul.SYSOSP.Web.Helpers;

namespace Verdezul.SYSOSP.Web.Entities
{
    [Table("FechasPedido")]
    public class FechaPedido : Entidad
    {
        public FechaPedido()
        {

        }

        public FechaPedido(Pedido pedido) : base()
        {
            this.Estado = pedido.Estado;
            this.PedidoID = pedido.ID;
        }

        [Index("IX_FechasPedido", 1, IsUnique = true)]
        [Required]
        public EstadoPedido Estado { get; set; }

        [Index("IX_FechasPedido", 2, IsUnique = true)]
        [Required]
        public int PedidoID { get; set; }
        public Pedido Pedido { get; set; }

        [NotMapped]
        public DateTime Fecha { get { return this.EntityDate; } }

        [NotMapped]
        public string EstadoPedidoTexto { get { return this.Estado.ToDisplayName(); } }

        [NotMapped]
        public string NombreUsuario { get; set; }
    }
}
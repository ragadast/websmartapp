﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Verdezul.SYSOSP.Web.Entities
{
    [Table("Parametros")]
    public class Parametro : Entidad
    {
        public Parametro()
        {

        }
        /// <summary>
        /// Creación de Parámetro usado únicamente para adicionales
        /// </summary>
        /// <param name="tipo"></param>
        /// <param name="valor"></param>
        public Parametro(TipoParametroReporte tipo, string valor)
        {
            ID = (int)tipo;
            Valor = valor;
        }

        [Required(ErrorMessage = "{0} es requerido")]
        [DisplayName("Nombre")]
        public TipoParametro Tipo { get; set; }

        [Required(ErrorMessage = "{0} es requerido")]
        [MaxLength(64)]
        public string Valor { get; set; }

        [Required(ErrorMessage = "{0} es requerido")]
        public TipoDato TipoDato { get; set; }
    }

    public enum TipoParametro
    {
        [Display(Name = "Nombre Facultad")]
        NombreFacultad = 1,
        [Display(Name = "RUC Facultad")]
        RUCFacultad = 2,
        [Display(Name = "Representante Legal")]
        RepresentanteLegal = 3,
        [Display(Name = "Dirección Facultad")]
        DireccionFacultad = 4,
        [Display(Name = "Teléfono Facultad")]
        TelefonoFacultad = 5,
        [Display(Name = "Correo Electrónico")]
        Correo = 6,
        [Display(Name = "Porcentaje del IVA")]
        PorcentajeIVA = 7,
        [Display(Name = "Validez de oferta")]
        ValidezOferta = 8,
        [Display(Name = "URL Ubicación")]
        URLUbicacion = 9,
        [Display(Name = "URL Formulario Encuesta")]
        URLFormulario = 10
    }
}
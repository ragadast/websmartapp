﻿using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace Verdezul.SYSOSP.Web.Entities
{
    public class Plantilla : Entidad
    {
        [DisplayName("Nombre")]
        [Required(ErrorMessage = "{0} es requerido")]
        public string Nombre { get; set; }

        public bool Activo { get; set; }

        public virtual ICollection<DetallePlantilla> ListaAnalisis { get; set; }
    }
}
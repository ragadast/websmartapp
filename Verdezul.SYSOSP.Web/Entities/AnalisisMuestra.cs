﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Verdezul.SYSOSP.Web.Entities
{
    [Table("AnalisisMuestra")]
    public class AnalisisMuestra : Entidad
    {
        public AnalisisMuestra()
        {
            SubAnalisisMuestra = new List<SubAnalisisMuestra>();
        }

        [Required]
        public int AnalisisID { get; set; }
        public Analisis Analisis { get; set; }

        [Required]
        public int MuestraID { get; set; }
        public Muestra Muestra { get; set; }

        [Required]
        public int OrdenTrabajoID { get; set; }
        public OrdenTrabajo OrdenTrabajo { get; set; }

        [Required(ErrorMessage = "{0} es requerida")]
        [Range(1, Constantes.CANTIDAD_MUESTRAS, ErrorMessage = "{0} debe ser mayor que 0")]
        public int Cantidad { get; set; }

        /* Campos no-natos, se copian del Analsis a la fecha */
        [Required]
        [MaxLength(128)]
        public string Nombre { get; set; }

        [DisplayName("Técnica")]
        [MaxLength(128)]
        public string Tecnica { get; set; }

        [Required]
        public decimal Valor { get; set; }

        [Required]
        public Boolean TieneIVA { get; set; }

        [DisplayName("Acreditación SAE")]
        [Required]
        public Boolean AcreditacionSAE { get; set; }

        [Required]
        public Boolean Monitoreo { get; set; }

        public virtual ICollection<SubAnalisisMuestra> SubAnalisisMuestra { get; set; }

        /* Campos Calculados */
        public decimal ValorIVA { get { return this.TieneIVA ? Valor * this.OrdenTrabajo.Pedido.IVA : 0; } }
        public decimal ValorConIVA { get { return Valor + ValorIVA; } }
        public decimal TotalSinIVA { get { return Cantidad * Valor; } }
        public decimal TotalConIVA { get { return Cantidad * ValorConIVA; } }
        public decimal TotalValorIVA { get { return Cantidad * ValorIVA; } }

        [NotMapped]
        public int? Ticks { get; set; }

        public AnalisisMuestra Duplicar(Muestra muestra)
        {
            if (muestra == null)
                throw new Exception("No se puede duplicar un AnalisisMuestra sin tener una muestra válida (muestra is null)");

            var am = new AnalisisMuestra
            {
                AnalisisID = this.AnalisisID,
                Muestra = muestra,
                OrdenTrabajoID = this.OrdenTrabajoID,
                Cantidad = this.Cantidad
            };

            // Se duplican solo si no existe el Análisis actualizado
            if (this.Analisis != null)
            {
                am.Nombre = Analisis.Nombre;
                am.Tecnica = Analisis.Tecnica;
                am.Valor = Analisis.Valor;
                am.TieneIVA = Analisis.TieneIVA;
                am.AcreditacionSAE = Analisis.AcreditacionSAE;
                am.Monitoreo = Analisis.Monitoreo;
            }
            else
            {
                am.Nombre = this.Nombre;
                am.Tecnica = this.Tecnica;
                am.Valor = this.Valor;
                am.TieneIVA = this.TieneIVA;
                am.AcreditacionSAE = this.AcreditacionSAE;
                am.Monitoreo = this.Monitoreo;
            }

            foreach (var sam in SubAnalisisMuestra)
            {
                am.SubAnalisisMuestra.Add(sam.Duplicar());
            }

            am.SetDataLog();
            return am;
        }
    }

    [Table("SubAnalisisMuestra")]
    public class SubAnalisisMuestra : Entidad
    {
        [Required(ErrorMessage = "{0} es requerido")]
        [MaxLength(128)]
        public string Nombre { get; set; }

        [DisplayName("Análisis")]
        [Required(ErrorMessage = "{0} es requerido")]
        public int AnalisisMuestraID { get; set; }
        public AnalisisMuestra AnalisisMuestra { get; set; }

        public SubAnalisisMuestra Duplicar()
        {
            var sam = new SubAnalisisMuestra
            {
                Nombre = this.Nombre,
                AnalisisMuestraID = this.AnalisisMuestraID,
            };

            sam.SetDataLog();
            return sam;
        }
    }

    public class AnalisisMuestraHelp
    {
        public int MuestraID { get; set; }
        public int AnalisisID { get; set; }
        public int AreaID { get; set; }
        public int BioquimicoID { get; set; }
        public int Cantidad { get; set; }
        public decimal Valor { get; set; }
        public String Tecnica { get; set; }
        public int? Ticks { get; set; }
        public List<SubAnalisisMuestra> SubAnalisisMuestra { get; set; }

        public int Orden { get; set; }
        public int OrdenTrabajoID { get; set; }
        public decimal TotalSinIVA { get; set; }
        public decimal TotalConIVA { get; set; }
        public bool AcreditacionSAE { get; set; }
        public string Nombre { get; set; }
        public string NombreArea { get; set; }
        public string NombreGrupo { get; set; }
        public int NumeroOrdenTrabajo { get; set; }
        public int NumeroMuestra { get; set; }
        public bool TieneIVA { get; set; }
        public bool Monitoreo { get; set; }
        public string NombreNivel { get; set; }
        public int GrupoAnalisisID { get; set; }
    }
}
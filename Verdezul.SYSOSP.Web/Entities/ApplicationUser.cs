﻿using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Security.Claims;
using System.Threading.Tasks;

namespace Verdezul.SYSOSP.Web.Entities
{
    public class ApplicationUser : IdentityUser
    {
        public async Task<ClaimsIdentity> GenerateUserIdentityAsync(UserManager<ApplicationUser> manager)
        {
            // Note the authenticationType must match the one defined in CookieAuthenticationOptions.AuthenticationType
            var userIdentity = await manager.CreateIdentityAsync(this, DefaultAuthenticationTypes.ApplicationCookie);
            // Add custom user claims here
            return userIdentity;
        }

        [DisplayName("Cédula")]
        [Required(ErrorMessage = "{0} es requerido")]
        [MaxLength(10)]
        public string Cedula { get; set; }
        [Required(ErrorMessage = "{0} es requerido")]
        [MaxLength(32)]
        public string Nombres { get; set; }
        [Required(ErrorMessage = "{0} es requerido")]
        [MaxLength(32)]
        public string Apellidos { get; set; }
        [Required(ErrorMessage = "{0} es requerido")]
        [MaxLength(32)]
        public string Cargo { get; set; }
        [DisplayName("Título")]
        [MaxLength(8)]
        public string Titulo { get; set; }
        [Required(ErrorMessage = "{0} es requerido")]
        public bool Activo { get; set; }

        [NotMapped]
        public string NombresCompletos { get { return $"{(Titulo != null ? Titulo.Trim() : "")} {Nombres.Trim()} {Apellidos.Trim()}".Trim(); } }

        [NotMapped]
        public string NombresApellidos { get { return $"{Nombres.Trim()} {Apellidos.Trim()}".Trim(); } }

        [NotMapped]
        public bool EsBioquimico { get; set; }

        [NotMapped]
        public bool Bloqueada { get { return (this.LockoutEndDateUtc > DateTime.UtcNow); } }
    }
}
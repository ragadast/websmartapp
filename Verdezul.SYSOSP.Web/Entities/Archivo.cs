﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Verdezul.SYSOSP.Web.Helpers;
using System.ComponentModel;

namespace Verdezul.SYSOSP.Web.Entities
{
    [Table("Archivos")]
    public class Archivo : Entidad
    {
        public Archivo()
        {
            this.Codigo = Guid.NewGuid();
        }

        public Archivo(int pedidoID)
        {
            this.PedidoID = pedidoID;
            this.Codigo = Guid.NewGuid();
            this.Subido = false;
        }

        public int PedidoID { get; set; }
        public Pedido Pedido { get; set; }

        public Guid Codigo { get; set; }

        public TipoArchivo Tipo { get; set; }
        public int? IdOrdenTrabajo { get; set; }
        public int? IdMuestra { get; set; }
        public int? NumeroInforme { get; set; }
        public bool Subido { get; set; }
        [MaxLength(8)]
        public String Extension { get; set; }
        public DateTime? Fecha { get; set; }

        [DefaultValue(false)]
        public bool NecesitaSuplementario { get; set; }

        public string NombreArchivo
        {
            get
            {
                if (!Subido)
                    return null;

                switch (Tipo)
                {
                    case TipoArchivo.Solicitud:
                    case TipoArchivo.Proforma:
                    case TipoArchivo.AceptacionCliente:
                        return $"{Tipo.ToDisplayShortName()}-{PedidoID}{Extension}";
                    case TipoArchivo.OrdenTrabajo:
                        return $"{Tipo.ToDisplayShortName()}-{PedidoID}-{IdOrdenTrabajo}{Extension}";
                    case TipoArchivo.RecepcionMuestra:
                        return $"{Tipo.ToDisplayShortName()}-{PedidoID}-{IdMuestra}{Extension}";
                    case TipoArchivo.InformeResultados:
                    case TipoArchivo.InformeSuplementario:
                        return $"{Tipo.ToDisplayShortName()}-{PedidoID}-{IdOrdenTrabajo}-{IdMuestra}-{NumeroInforme}{Extension}";
                    default:
                        return null;
                }
            }
        }

        public override IEnumerable<ValidationResult> Validate(ValidationContext validationContext)
        {
            if (Tipo == TipoArchivo.InformeResultados && (!this.IdOrdenTrabajo.HasValue || !this.IdMuestra.HasValue))
            {
                yield return new ValidationResult("El InformeResultados debe tener asociado OrdenTrabajo y Muestra", new[] { "InformeResultados" });
            }
        }

        public static Archivo SetArchivoFromName(string nombreArchivo)
        {
            var data = nombreArchivo.Split('.');
            var ids = data[0].Substring(2).Split('-').Select(x => Convert.ToInt32(x)).ToList();

            var archivo = new Archivo
            {
                PedidoID = ids[0],
                Extension = $".{data[1]}",
                Subido = true,
                Fecha = null
            };

            switch (data[0].Substring(0, 1))
            {
                case "S":
                    archivo.Tipo = TipoArchivo.Solicitud;
                    break;
                case "P":
                    archivo.Tipo = TipoArchivo.Proforma;
                    break;
                case "A":
                    archivo.Tipo = TipoArchivo.AceptacionCliente;
                    break;
                case "O":
                    archivo.Tipo = TipoArchivo.OrdenTrabajo;
                    archivo.IdOrdenTrabajo = ids[1];
                    break;
                case "M":
                    archivo.Tipo = TipoArchivo.RecepcionMuestra;
                    archivo.IdMuestra = ids[1];
                    break;
                case "R":
                    archivo.Tipo = TipoArchivo.InformeResultados;
                    archivo.IdOrdenTrabajo = ids[1];
                    archivo.IdMuestra = ids[2];
                    archivo.NumeroInforme = ids[3];
                    break;
                case "U":
                    archivo.Tipo = TipoArchivo.InformeSuplementario;
                    archivo.IdOrdenTrabajo = ids[1];
                    archivo.IdMuestra = ids[2];
                    archivo.NumeroInforme = ids[3];
                    break;
                default:
                    return null;
            }
            return archivo;
        }
    }

    public class ArchivoHelp
    {
        public int IdPedido { get; set; }
        public int IdOrdenTrabajo { get; set; }
        public int IdMuestra { get; set; }
        public int CantidadInformes { get; set; }
    }
}

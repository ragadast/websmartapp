﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Verdezul.SYSOSP.Web.Entities
{
    [Table("Bioquimicos")]
    public class Bioquimico : Entidad
    {
        public Bioquimico()
        {

        }

        public Bioquimico(string UserName)
        {
            this.UserName = UserName;
            SetDataLog();
        }

        [Required]
        [MaxLength(32)]
        public string UserName { get; set; }

        [NotMapped]
        public ApplicationUser ApplicationUser { get; set; }

        [NotMapped]
        public string NombresCompletos { get { return (ApplicationUser != null) ? ApplicationUser.NombresCompletos : ""; } }

        public virtual ICollection<Area> Area { get; set; }
        public virtual ICollection<OrdenTrabajo> OrdenesTrabajo { get; set; }
    }
}
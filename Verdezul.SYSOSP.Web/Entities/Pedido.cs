﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;

namespace Verdezul.SYSOSP.Web.Entities
{
    [Table("Pedidos")]
    public class Pedido : Entidad
    {
        public Pedido()
        {
            Ordenes = new List<OrdenTrabajo>();
            Muestras = new List<Muestra>();
            Archivos = new List<Archivo>();
            EnviosCorreo = new List<EnvioCorreo>();
            Proforma = 0;
            IVA = -1;
        }

        [Required(ErrorMessage = "Cliente es requerido")]
        public int ClienteID { get; set; }
        public Cliente Cliente { get; set; }

        [Required(ErrorMessage = "{0} es requerido")]
        [DisplayName("Estado")]
        public EstadoPedido Estado { get; set; }

        [Required(ErrorMessage = "{0} es requerido")]
        [DisplayName("Número de Proforma")]
        [Index(IsUnique = true)]
        public int Proforma { get; set; }

        [DisplayName("Número de Pedido")]
        public int? Numero { get; set; }

        [DisplayName("Persona Contacto")]
        [MaxLength(64)]
        public string PersonaContacto { get; set; }

        [DisplayName("Información Adicional")]
        [MaxLength(32)]
        public string InformacionAdicional { get; set; }

        [Required(ErrorMessage = "{0} es requerida")]
        [DisplayName("Validez de oferta")]
        [DefaultValue(15)]
        public int ValidezOferta { get; set; }

        [Required(ErrorMessage = "{0} es requerido")]
        public Guid Codigo { get; set; }

        [Required(ErrorMessage = "{0} es requerido")]
        [DisplayName("Forma de Pago")]
        public FormaPago FormaPago { get; set; }

        [Required(ErrorMessage = "{0} es requerido")]
        [DisplayName("Doc. de Recaudación")]
        public DocumentoRecaudacion DocumentoRecaudacion { get; set; }

        #region Pago del Contrato al Pedido (si tuviera FormaPago.Contrato)
        [DisplayName("Información de Pago")]
        public string InformacionPago { get; set; }
        public int? PagoContratoID { get; set; }
        public PagoContrato PagoContrato { get; set; }
        [DisplayName("Fecha de Recaudación")]
        public DateTime? FechaRecaudacion { get; set; }
        [MaxLength(32)]
        public string Factura { get; set; }
        #endregion

        [DisplayName("Comentario")]
        public string Comentario { get; set; }

        public int? IDAnterior { get; set; }

        public virtual ICollection<OrdenTrabajo> Ordenes { get; set; }

        public virtual ICollection<Muestra> Muestras { get; set; }

        public virtual ICollection<FechaPedido> Fechas { get; set; }

        public virtual ICollection<Archivo> Archivos { get; set; }

        public virtual ICollection<EnvioCorreo> EnviosCorreo { get; set; }

        public decimal IVA { get; set; }

        public string NumeroProforma { get { return Proforma.ToString().Insert(4, "-"); } }
        public string NumeroPedido { get { return (Numero.HasValue) ? Numero.Value.ToString().Insert(4, "-") : ""; } }
        public string NombreOficial
        {
            get
            {
                string nombre = $"{(Estado >= EstadoPedido.Pagado ? "Pedido" : "Proforma")} {(Estado >= EstadoPedido.Pagado ? NumeroPedido : NumeroProforma)}";
                if (nombre.Trim() == "Pedido")
                    nombre = $"Proforma {NumeroProforma} (sin uso)";
                return nombre;
            }
        }

        [NotMapped]
        public string InformacionPagoOficial { get { return (FormaPago == FormaPago.Contrato && PagoContrato != null) ? this.PagoContrato.NombreContrato : InformacionPago; } }

        public decimal CostoTotalSinIVA
        {
            get
            {
                if (this.Ordenes.Any())
                {
                    return this.Ordenes.Sum(x => x.AnalisisMuestras.Sum(y => y.TotalSinIVA));
                }
                return 0;
            }
        }

        public decimal CostoTotalConIVA
        {
            get
            {
                if (this.Ordenes.Any())
                {
                    return this.Ordenes.Sum(x => x.AnalisisMuestras.Sum(y => y.TotalConIVA));
                }
                return 0;
            }
        }

        public decimal SumaTotalValorIVA
        {
            get
            {
                if (this.Ordenes.Any())
                {
                    return this.Ordenes.Sum(x => x.AnalisisMuestras.Sum(y => y.TotalValorIVA));
                }
                return 0;
            }
        }

        public bool TieneAlgunArea(IEnumerable<int> idsAreas)
        {
            if (this.Ordenes != null && this.Ordenes.Count > 0)
            {
                foreach (var a in idsAreas)
                {
                    if (this.Ordenes.Select(x => x.AreaID).Contains(a))
                        return true;
                }
            }
            return false;
        }

        public override IEnumerable<ValidationResult> Validate(ValidationContext validationContext)
        {
            if (Estado == EstadoPedido.Pagado && (this.InformacionPago == null || this.InformacionPago == ""))
            {
                yield return new ValidationResult("La Información de Pago es requerida", new[] { "InformacionPago" });
            }
            if (Estado == EstadoPedido.Pagado && !this.FechaRecaudacion.HasValue)
            {
                yield return new ValidationResult("La Fecha de Pago es requerida", new[] { "FechaRecaudacion" });
            }
        }

        [NotMapped]
        public MotivoCierrePedido? MotivoCierre
        {
            get
            {
                if (this.Fechas != null)
                {
                    if (this.Fechas.Any(x => x.Estado == EstadoPedido.Cerrado))
                    {
                        if (!this.Fechas.Any(x => x.Estado == EstadoPedido.Aprobado))
                            return MotivoCierrePedido.NoAprobado;

                        if (!this.Fechas.Any(x => x.Estado == EstadoPedido.Aceptado))
                            return MotivoCierrePedido.Caducado;

                        if (!this.Fechas.Any(x => x.Estado == EstadoPedido.Pagado))
                            return MotivoCierrePedido.NoPagado;

                        return MotivoCierrePedido.Entregado;
                    }
                }
                return null;
            }
        }

        [NotMapped]
        public FechaPedido FechaEstado
        {
            get
            {
                return (Fechas != null) ? this.Fechas.Single(x => x.Estado == this.Estado) : null;
            }
        }

        [NotMapped]
        public int NumeroCorreosPendientes
        {
            get
            {
                int numeroNotificacionesPendientes = 0;

                switch (Estado)
                {
                    case EstadoPedido.Aprobado:
                        // Verificar proforma enviada
                        if (EnviosCorreo == null ||
                            !EnviosCorreo.Any(x => x.TipoNotificacion == TipoNotificacion.Proforma))
                        {
                            numeroNotificacionesPendientes++;
                        }
                        break;
                    case EstadoPedido.Pagado:
                    case EstadoPedido.Entregado:
                        // Archivos de Resultados
                        var IDArchivosInformePrin =
                            from Prin in Archivos
                            where Prin.Tipo == TipoArchivo.InformeResultados
                            && Prin.Subido
                            select Prin.ID;

                        // Archivos de Resultados Suplementarios
                        var IDArchivosInformeSupl =
                            from Supl in Archivos
                            join Prin in Archivos
                            on new { Supl.IdOrdenTrabajo, Supl.IdMuestra, Supl.NumeroInforme }
                            equals new { Prin.IdOrdenTrabajo, Prin.IdMuestra, Prin.NumeroInforme }
                            where Prin.Tipo == TipoArchivo.InformeResultados
                            && Supl.Tipo == TipoArchivo.InformeSuplementario
                            && Prin.NecesitaSuplementario
                            && Supl.Subido
                            select Supl.ID;

                        var IDArchivosInforme = IDArchivosInformePrin.Union(IDArchivosInformeSupl);

                        //Notificaciones de Resultados (incluidos los suplementarios)
                        var IDArchivosNotificados =
                            (from ec in EnviosCorreo
                             where ec.TipoNotificacion == TipoNotificacion.InformeResultados
                             || ec.TipoNotificacion == TipoNotificacion.InformeResultadosSuplementario
                             select ec.ArchivoID).Distinct();

                        var diferencia = IDArchivosInforme.Count() - IDArchivosNotificados.Count();

                        numeroNotificacionesPendientes += diferencia;

                        break;
                }
                return numeroNotificacionesPendientes;
            }
        }

        public EnvioCorreo GetNotificacionxArchivo(int idArchivo)
        {
            Archivo archivo = this.Archivos.SingleOrDefault(x => x.ID == idArchivo && x.Subido);

            if (archivo == null || this.EnviosCorreo == null || this.EnviosCorreo.Count == 0)
                return null;

            switch (archivo.Tipo)
            {
                case TipoArchivo.Proforma:
                    return this.EnviosCorreo.FirstOrDefault(x => x.TipoNotificacion == TipoNotificacion.Proforma);
                case TipoArchivo.AceptacionCliente:
                    return this.EnviosCorreo.FirstOrDefault(x => x.TipoNotificacion == TipoNotificacion.Proforma);
                case TipoArchivo.InformeResultados:
                    return this.EnviosCorreo.FirstOrDefault(x => x.TipoNotificacion == TipoNotificacion.InformeResultados && x.ArchivoID == idArchivo);
                case TipoArchivo.InformeSuplementario:
                    return this.EnviosCorreo.FirstOrDefault(x => x.TipoNotificacion == TipoNotificacion.InformeResultadosSuplementario && x.ArchivoID == idArchivo);
                default:
                    //case TipoArchivo.Solicitud:
                    //case TipoArchivo.OrdenTrabajo:
                    //case TipoArchivo.RecepcionMuestra:
                    return null;
            }
        }
    }
}

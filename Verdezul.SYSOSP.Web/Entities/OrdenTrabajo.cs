﻿using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;

namespace Verdezul.SYSOSP.Web.Entities
{
    [Table("OrdenesTrabajo")]
    public class OrdenTrabajo : Entidad
    {
        public OrdenTrabajo()
        {
            AnalisisMuestras = new List<AnalisisMuestra>();
        }

        [Required]
        public int Numero { get; set; }

        [Required]
        public int AreaID { get; set; }
        public Area Area { get; set; }

        [Required]
        public int BioquimicoID { get; set; }
        public Bioquimico Bioquimico { get; set; }

        [Required]
        public int PedidoID { get; set; }
        public Pedido Pedido { get; set; }

        [DisplayName("Tiempo de Entrega")]
        public int TiempoEntrega { get; set; }

        [DisplayName("Cantidad de Informes")]
        [DefaultValue(1)]
        public int CantidadInformes { get; set; }

        public string NumeroOrden { get { return $"{this.Pedido.NumeroPedido}-{Numero}"; } }

        public string NombreOrden { get { return $"{this.NumeroOrden} | {this.Area.Nombre}"; } }

        public virtual ICollection<AnalisisMuestra> AnalisisMuestras { get; set; }

        public OrdenTrabajo Duplicar(ICollection<Muestra> muestras)
        {
            var orden = new OrdenTrabajo
            {
                Numero = this.Numero,
                AreaID = this.AreaID,  
                BioquimicoID = this.BioquimicoID,
                TiempoEntrega = this.TiempoEntrega,
                CantidadInformes = this.CantidadInformes,
            };

            foreach(var am in AnalisisMuestras)
            {
                var muestra = muestras.SingleOrDefault(x => x.Numero == am.Muestra.Numero);
                orden.AnalisisMuestras.Add(am.Duplicar(muestra));
            }

            orden.SetDataLog();
            return orden;
        }

        public bool SePuedeEliminar()
        {
            if (AnalisisMuestras != null && AnalisisMuestras.Count > 0)
            {
                return !AnalisisMuestras.Select(x => x.Muestra).Any(Muestra => Muestra.Estado == EstadoMuestra.Recibida);
            }
            return true;
        }
    }
}
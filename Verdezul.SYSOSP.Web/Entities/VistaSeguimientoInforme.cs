﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations.Schema;

namespace Verdezul.SYSOSP.Web.Entities
{
    [Table("VistaSeguimientosInformes")]
    public class VistaSeguimientoInforme
    {
        public int ID { get; set; }
        public int IdProforma { get; set; }
        public int IdCliente { get; set; }
        public int Proforma { get; set; }
        public int Numero { get; set; }
        public DateTime FechaPago { get; set; }
        public string ClienteIdentificacion { get; set; }
        public string ClienteNombre { get; set; }
        public EstadoPedido EstadoPedido { get; set; }
        public DateTime? FechaRecepcionMuestra { get; set; }
        public string NombreArea { get; set; }
        public int TiempoEntrega { get; set; }
        public string NombreMuestra { get; set; }
        public EstadoMuestra EstadoMuestra { get; set; }
        public bool ArchivoSubido { get; set; }
        public DateTime? FechaCarga { get; set; }
        public DateTime? FechaNotificacion { get; set; }
    }
}
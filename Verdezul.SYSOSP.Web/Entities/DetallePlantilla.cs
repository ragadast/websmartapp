﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Verdezul.SYSOSP.Web.Entities
{
    [Table("DetallesPlantilla")]
    public class DetallePlantilla : Entidad
    {
        [DisplayName("Plantilla")]
        [Required(ErrorMessage = "{0} es requerido")]
        public int PlantillaID { get; set; }
        public Plantilla Plantilla { get; set; }

        [DisplayName("Analisis")]
        [Required(ErrorMessage = "{0} es requerido")]
        public int AnalisisID { get; set; }
        public Analisis Analisis { get; set; }

        [NotMapped]
        public int? Ticks { get; set; }
    }
}
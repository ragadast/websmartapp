﻿using System.ComponentModel.DataAnnotations.Schema;

namespace Verdezul.SYSOSP.Web.Entities
{
    [Table("EnviosCorreo")]
    public class EnvioCorreo : Entidad
    {
        public TipoNotificacion TipoNotificacion { get; set; }
        public string Direccion { get; set; }
        public string Destinatario { get; set; }
        public int? PedidoID { get; set; }
        public Pedido Pedido { get; set; }
        public int? ArchivoID { get; set; }
        public Archivo Archivo { get; set; }
    }
}
﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using Verdezul.SYSOSP.Web.Helpers;

namespace Verdezul.SYSOSP.Web.Entities
{
    public class Enumerado
    {
        public string Tipo { get; set; }
        public int Codigo { get; set; }
        public string Texto { get; set; }

        public string Valor { get; set; }

        public static List<Enumerado> GetEnumerados()
        {
            return GetEnumerado<FormaPago>()
                .Concat(GetEnumerado<DocumentoRecaudacion>())
                .Concat(GetEnumerado<TipoIdentificacion>())
                .ToList();
        }

        public static List<Enumerado> GetEnumerado<T>() where T : struct
        {
            return GetEnumerado<T>(true);
        }

        public static List<Enumerado> GetEnumerado<T>(bool IncluirNinguno) where T : struct
        {
            var enumerados = ((T[])Enum.GetValues(typeof(T))).Select(enumerado =>
                new Enumerado { Tipo = typeof(T).Name, Codigo = Convert.ToInt32(enumerado), Texto = enumerado.ToDisplayName(), Valor = enumerado.ToString() }
            ).ToList();

            if (IncluirNinguno)
            {
                enumerados.Insert(0, new Enumerado { Tipo = typeof(T).Name, Codigo = 0, Texto = "(Ninguno)", Valor = "" });
            }

            return enumerados;
        }
    }

    public enum EstadoPedido
    {
        Ingresado = 1,
        [Display(Name = "Asignado Muestras")]
        Muestras = 2,
        Parametrizado = 3,
        [Display(Name = "Asignado Tiempos")]
        Tiempos = 4,
        [Display(Name = "Aprobado Hoja Técnica")]
        Aprobado = 5,
        Aceptado = 6,
        Pagado = 7,
        Entregado = 8,
        Cerrado = 9
    }

    public enum FormaPago
    {
        [Display(Name = "Efectivo")]
        Efectivo = 1,
        [Display(Name = "Pago en Ventanilla")]
        Ventanilla = 2,
        [Display(Name = "Tarjeta de Crédito o Débito")]
        Tarjeta = 3,
        [Display(Name = "Cheque Certificado")]
        Cheque = 4,
        [Display(Name = "Transferencia Sector Privado")]
        TransferenciaPrivado = 5,
        [Display(Name = "Transferencia Sector Público")]
        TransferenciaPublico = 6,
        [Display(Name = "Contrato")]
        Contrato = 7,
        [Display(Name = "Certificación presupuestaria")]
        CertificacionPresupuestaria = 8
    }

    public enum DocumentoRecaudacion
    {
        [Display(Name = "Recibo de Recaudación")]
        Recibo = 1,
        [Display(Name = "Comprobante de Depósito")]
        Comprobante = 2,
        [Display(Name = "Recibo de Transferencia")]
        Transferencia = 3
    }

    public enum TipoArchivo
    {
        [Display(Name = "Solicitud para Pedido", ShortName = "S")]
        Solicitud = 1,
        [Display(Name = "Proforma", ShortName = "P")]
        Proforma = 2,
        [Display(Name = "Proforma Aceptada de Cliente", ShortName = "A")]
        AceptacionCliente = 3,
        [Display(Name = "Orden de Trabajo", ShortName = "O")]
        OrdenTrabajo = 4,
        [Display(Name = "Recepción de Muestra", ShortName = "M")]
        RecepcionMuestra = 5,
        [Display(Name = "Informe de Resultados de Muestra", ShortName = "R")]
        InformeResultados = 6,
        [Display(Name = "Informe Suplementario de Resultados de Muestra", ShortName = "U")]
        InformeSuplementario = 7,
    }

    public enum TipoNotificacion
    {
        [Display(Name = "Proforma Emitida")]
        Proforma = 1,
        [Display(Name = "Informe de Resultados")]
        InformeResultados = 2,
        [Display(Name = "Suplemento Informe de Resultados")]
        InformeResultadosSuplementario = 3,
        [Display(Name = "Pago Receptado")]
        PagoReceptado = 4,
        ForgotPasswordEmail = 5,
        Otros = 6
    }

    public enum ReportePedido
    {
        [Display(Name = "Hoja Técnica")]
        HojaTecnica = 1,
        [Display(Name = "Proforma de Pedido")]
        Proforma = 2,
        [Display(Name = "Recepción de Muestra")]
        RecepcionMuestra = 3,
        [Display(Name = "Orden de Trabajo")]
        OrdenTrabajo = 4,
        [Display(Name = "Plantilla de Apoyo a la Orden de Trabajo")]
        Plantilla = 5
    }

    public enum ReporteProforma
    {
        [Display(Name = "Proformas Emitidas")]
        ProformasIngresadas = 1,
        [Display(Name = "Proformas Autorizadas")]
        ProformasAprobadas = 2,
        [Display(Name = "Proformas Pagadas - Listado")]
        ProformasPagadasListado = 3,
        [Display(Name = "Proformas Pagadas - Detalle")]
        ProformasPagadasDetalle = 4,
        [Display(Name = "Proformas Pagadas - Resumen")]
        ProformasPagadasResumen = 5,
        [Display(Name = "Proformas Cerradas - Listado")]
        ProformasCerradasListado = 6,
        [Display(Name = "Proformas Cerradas - Detalle")]
        ProformasCerradasDetalle = 7,
        [Display(Name = "Proformas Cerradas - Resumen")]
        ProformasCerradasResumen = 8,
        [Display(Name = "Seguimiento de Informes")]
        SeguimientoInformes = 9
    }

    public enum ReporteGeneral
    {
        [Display(Name = "Listado de Proformas")]
        ListadoProformas = 1,
        [Display(Name = "Listado de Recepción Muestras")]
        ListadoRecepcionMuestras = 2,
        [Display(Name = "Listado de Pagos")]
        ListadoPagos = 3,
        [Display(Name = "Catálogos de Análisis")]
        CatalogoAnalisis = 4,
        [Display(Name = "Listado de Clientes")]
        ListadoClientes = 5
    }

    public enum TipoDato
    {
        Texto = 1,
        Entero = 2,
        Fecha = 3,
        Boleano = 4,
        Correo = 5,
        URL = 6
    }

    public enum EstadoMuestra
    {
        Pendiente = 1,
        Recibida = 2
    }

    public enum TipoIdentificacion
    {
        Cédula = 1,
        RUC = 2,
        [Display(Name = "RUC Especial", ShortName = "RUC")]
        RUCEspecial = 4,
        Pasaporte = 3
    }

    public enum EstadoCliente
    {
        Activo = 1,
        Inactivo = 2
        //Moroso = 3
    }

    public enum ErrorCliente
    {
        [Display(Name = "Error no reconocido")]
        NoReconocido = 0,
        [Display(Name = "Identificacion de Cliente repetida")]
        IdentificacionRepetida = 1,
        [Display(Name = "Nombre de Cliente repetido")]
        NombreRepetido = 2,
    }

    public enum TipoMantenimiento
    {
        Creación = 1,
        Modificación = 2,
        Eliminación = 3,
        Listado = 4,
        ListadoAsignados = 5,
        Visualización = 6,
        Duplicación = 7,
        [Display(Name = "Revisión de Factura")]
        RevisionFactura = 8,
        AnluacionMasiva = 9
    }

    public enum EstadoEntidad
    {
        Activo = 1,
        Eliminado = 2
    }

    public enum TipoListadoPedido
    {
        Todos = 1,
        SoloCerrados = 2,
        NoCerrados = 3
    }

    public enum MotivoCierrePedido
    {
        [Display(Name = "Entregado")]
        Entregado = 1,
        [Display(Name = "No Aprobado")]
        NoAprobado = 2,
        [Display(Name = "Caducado")]
        Caducado = 3,
        [Display(Name = "No Pagado")]
        NoPagado = 4,
    }

    public enum TipoSalida
    {
        Fecha = 1,
        FechaHoraMinuto = 2,
        FechaHoraMinutoSegundo = 3,
    }

    public enum TipoParametroReporte
    {
        FechaInicial = 101,
        FechaFinal = 102,
        EstadoPedido = 103,
        NombreArea = 104,
        NombreUsuario = 105,
        NombreCliente = 106,
    }
}
﻿using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace Verdezul.SYSOSP.Web.Entities
{
    [Table("LogDB")]
    public class LogDB
    {
        public int ID { get; set; }
        public String Tabla { get; set; }
        public int EntityID { get; set; }
        public int? PedidoID { get; set; }
        public int? OrdenTrabajoID { get; set; }
        public int? AnalisisMuestraID { get; set; }
        public int? ClienteID { get; set; }
        public DateTime EntityDate { get; set; }
        public EstadoEntidad Status { get; set; }
        public String User { get; set; }
    }
}
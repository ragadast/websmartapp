﻿using Verdezul.SYSOSP.Web.Helpers;

namespace Verdezul.SYSOSP.Web.Entities
{
    public class MensajePostBack
    {
        public string Texto { get; set; }
        public TipoMensaje Tipo { get; set; }
        public string CodigoMensaje { get; set; }

        public enum TipoMensaje
        {
            Informacion = 1,
            Error = 2,
            Advertencia = 2
        }

        public string ObjetoJS()
        {
            return string.Format("{0} Texto : '{2}', Tipo : '{3}', CodigoMensaje: '{4}' {1}",
                "{",
                "}",
                this.Texto.Replace("'", ""),
                this.Tipo.ToDisplayName(),
                this.CodigoMensaje);
        }
    }
}
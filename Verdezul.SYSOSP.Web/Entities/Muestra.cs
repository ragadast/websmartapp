﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;

namespace Verdezul.SYSOSP.Web.Entities
{
    public class MuestraVirtual : Entidad
    {
        [DisplayName("#")]
        public int Numero { get; set; }
        public string Detalle { get; set; }
        public int Cantidad { get; set; }
        [DisplayName("Cantidad Requerida")]
        public string CantidadRequerida { get; set; }

        [MaxLength(150)]
        [DisplayName("Observación")]
        public string ObservacionInicial { get; set; }

        [DisplayName("Descripción")]
        [MaxLength(64)]
        public string Descripcion { get; set; }
        [DisplayName("Código")]
        [MaxLength(64)]
        public string Codigo { get; set; }
        [MaxLength(64)]
        public string Lote { get; set; }
        [MaxLength(64)]
        public string Contenido { get; set; }
        [MaxLength(256)]
        [DisplayName("Observación")]
        public string Observaciones { get; set; }

        public EstadoMuestra Estado { get; set; }

        [DisplayName("Fecha de Elaboración")]
        [MaxLength(64)]
        public string FechaElaboracion { get; set; }
        [DisplayName("Fecha de Vencimiento")]
        [MaxLength(64)]
        public string FechaVencimiento { get; set; }

        [NotMapped]
        public DateTime? FechaRecepcion
        {
            get
            {
                if (this.Estado == EstadoMuestra.Recibida)
                    return this.EntityDate;
                return null;
            }
        }
    }

    [Table("Muestras")]
    public class Muestra : MuestraVirtual
    {
        public Muestra()
        {
            AnalisisMuestras = new List<AnalisisMuestra>();
        }

        [Required(ErrorMessage = "{0} es requerido")]
        public new int Numero { get; set; }
        [Required(ErrorMessage = "{0} de Muestra es requerido")]
        [MaxLength(150)]
        public new string Detalle { get; set; }
        [Range(1, Constantes.CANTIDAD_MUESTRAS, ErrorMessage = "{0} debe ser mayor que 0")]
        [Required(ErrorMessage = "{0} es requerido")]
        public new int Cantidad { get; set; }
        [MaxLength(150)]
        public new String CantidadRequerida { get; set; }

        [Required]
        public int PedidoID { get; set; }
        public Pedido Pedido { get; set; }

        public virtual ICollection<AnalisisMuestra> AnalisisMuestras { get; set; }

        public String NombreMuestra { get { return (Detalle.Length < 32) ? $"{Numero} | {Detalle}" : $"{Numero} | {Detalle.Substring(0, 32)}"; } }

        public String UsuarioElaboracion { get { return this.User; } }

        [NotMapped]
        public int? Ticks { get; set; }

        public Muestra Duplicar()
        {
            var muestra = new Muestra
            {
                Numero = Numero,
                Detalle = Detalle,
                Cantidad = Cantidad,
                ObservacionInicial = ObservacionInicial,
                CantidadRequerida = CantidadRequerida,
                Estado = EstadoMuestra.Pendiente
            };

            muestra.SetDataLog();
            return muestra;
        }

        public bool OrdenesPendientes(List<int> IdsArchivosOTSubidos)
        {
            var ordenesMuestra = AnalisisMuestras.Select(x => x.OrdenTrabajoID).Distinct();
            var ordenesSubidas = IdsArchivosOTSubidos.Where(x => ordenesMuestra.Contains(x));
            return (ordenesSubidas.Count() != ordenesMuestra.Count());
        }
    }

    public class MuestraHelp : MuestraVirtual
    {
        public MuestraHelp()
        {

        }

        public MuestraHelp(Muestra muestra)
        {
            this.Numero = muestra.Numero;
            this.Detalle = muestra.Detalle;
            this.Cantidad = muestra.Cantidad;
            this.CantidadRequerida = muestra.CantidadRequerida;
            this.ObservacionInicial = muestra.ObservacionInicial;

            this.Descripcion = muestra.Descripcion;
            this.Codigo = muestra.Codigo;
            this.Lote = muestra.Lote;
            this.Contenido = muestra.Contenido;
            this.Observaciones = muestra.Observaciones;
            this.FechaElaboracion = muestra.FechaElaboracion;
            this.FechaVencimiento = muestra.FechaVencimiento;

            this.ID = muestra.ID;
            this.Pedido = muestra.Pedido;
        }

        [Required(ErrorMessage = "{0} es requerida")]
        public new string Descripcion { get; set; }
        public new string Codigo { get; set; }
        public new string Lote { get; set; }
        public new string Contenido { get; set; }
        public new string Observaciones { get; set; }
        public new string FechaElaboracion { get; set; }
        public new string FechaVencimiento { get; set; }

        public Pedido Pedido { get; set; }
    }
}
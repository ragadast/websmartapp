﻿using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Verdezul.SYSOSP.Web.Helpers;

namespace Verdezul.SYSOSP.Web.Entities
{
    [Table("Clientes")]
    public class Cliente : Entidad
    {
        public Cliente()
        {
            Pedidos = new List<Pedido>();
            PagosContrato = new List<PagoContrato>();
            TipoIdentificacion = TipoIdentificacion.Cédula;
            Estado = EstadoCliente.Activo;
        }

        [Required(ErrorMessage = "{0} es requerido")]
        [DisplayName("Tipo Identificación")]
        public TipoIdentificacion TipoIdentificacion { get; set; }

        [Required(ErrorMessage = "{0} es requerido")]
        [DisplayName("Número Identificación")]
        [Index(IsUnique = true)]
        [MaxLength(13)]
        public string NumeroIdentificacion { get; set; }

        [Required(ErrorMessage = "{0} es requerido")]
        [Index(IsUnique = true)]
        [MaxLength(64)]
        public string Nombre { get; set; }

        [Required(ErrorMessage = "{0} es requerido")]
        [DisplayName("Dirección")]
        [MaxLength(128)]
        public string Direccion { get; set; }

        [Required(ErrorMessage = "{0} es requerido")]
        [DisplayName("Teléfono")]
        [MaxLength(64)]
        public string Telefono { get; set; }

        [Required(ErrorMessage = "{0} es requerido")]
        [DisplayName("Email")]
        [EmailAddress(ErrorMessage = "El Email no tiene el formato correcto")]
        [MaxLength(64)]
        public string CorreoElectronico { get; set; }

        [DisplayName("Email Facturación")]
        [DefaultValue("")]
        [EmailAddress(ErrorMessage = "El Email no tiene el formato correcto")]
        [MaxLength(64)]
        public string CorreoElectronicoFacturacion { get; set; }

        [Required(ErrorMessage = "{0} es requerido")]
        public EstadoCliente Estado { get; set; }

        public virtual ICollection<Pedido> Pedidos { get; set; }

        public virtual ICollection<PagoContrato> PagosContrato { get; set; }

        [NotMapped]
        public string NombreReferencial {
            get { return $"{NumeroIdentificacion} - {Nombre}"; }
        }

        [NotMapped]
        public string NumeroIdentificacionReferencial
        {
            get { return $"{TipoIdentificacion.ToDisplayShortName()} - {NumeroIdentificacion}"; }
        }

        [NotMapped]
        public string TipoIdentificacionNombre
        {
            get { return TipoIdentificacion.ToDisplayShortName(); }
        }

        public override IEnumerable<ValidationResult> Validate(ValidationContext validationContext)
        {
            var validacion = NumeroIdentificacion.ValidarIdentificacion(this.TipoIdentificacion);
            if (validacion != ErrorValidacionNumeroIdentificacion.Correcto)
            {
                yield return new ValidationResult($"Número Identificación no es válido ({validacion.ToDisplayName()})", new[] { "NumeroIdentificacion" });
            }
        }
    }
}
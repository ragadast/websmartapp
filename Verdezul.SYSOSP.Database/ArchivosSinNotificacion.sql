--Script de comprobaci�n de archivos subidos que no se han notificado

--insert into EnviosCorreo (TipoNotificacion,	Direccion, Destinatario, PedidoID, EntityDate, [Status], [User], ArchivoID)
select	2 TipoNotificacion, 
		Arc.CorreoElectronico, 
		Arc.Cliente, 
		Arc.PedidoID, 
		DATEADD (HOUR , 1, Arc.FechaPago), 
		1, 'verdezul', 
		Arc.ArchivoID
		--, Env.ArchivoID
from (
	select a.ID ArchivoID, a.PedidoID, c.CorreoElectronico, c.Nombre Cliente, a.Fecha FechaPago
	from Pedidos p
	inner join Clientes c on p.ClienteID = c.ID
	inner join FechasPedido f on p.ID = f.PedidoID and p.Estado = f.Estado
	inner join Archivos a on p.ID = a.PedidoID
	where p.Estado in (7, 8) --Pagado, Entregado
	and a.Subido = 1 --Subido
	and a.Tipo = 6 -- InformeResultados
	and year(f.EntityDate) = 2023 and month(f.EntityDate) = 4 --and day(f.EntityDate) = 21
) Arc
left join (	
	select distinct PedidoID, ArchivoID, TipoNotificacion
	from EnviosCorreo
	where ArchivoID is not null
	and TipoNotificacion = 2 --InformeResultados
) Env
on Arc.ArchivoID = Env.ArchivoID and Arc.PedidoID = Env.PedidoID
where Env.ArchivoID is null


--Script de Correcci�n de Env�os de Correo/Notificaciones que por error se pusieron antes de la subida del archivo

--update ec set EntityDate = ecc.FechaNuevaNotificacion
select ec.ID, ec.EntityDate, ecc.FechaNuevaNotificacion
from EnviosCorreo ec
inner join (
	--select a.ID, e.ID, a.Fecha FechaSubido, DATEADD(hour, 1, a.Fecha) FechaNotificadoNueva, e.EntityDate FechaNotificado
	select e.ID EnvioCorreoID, DATEADD(hour, 1, a.Fecha) FechaNuevaNotificacion
	from Archivos a
	inner join (
		select Env.*
		from (
			select ArchivoID, max(EntityDate) EntityDate
			from EnviosCorreo
			where ArchivoID is not null
			group by ArchivoID
		) EnvU
		inner join EnviosCorreo Env
		on EnvU.ArchivoID = Env.ArchivoID and EnvU.EntityDate = Env.EntityDate
	) e on a.ID = E.ArchivoID
	where a.Fecha > e.EntityDate
	--order by 1
) ecc on ecc.EnvioCorreoID = ec.ID 
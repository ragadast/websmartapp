DROP TABLE LogDB
go

CREATE TABLE LogDB ( Id int )
GO

DROP VIEW LogDB
go

CREATE VIEW LogDB as 
select 0 as ID,				'' as Tabla,			0 as EntityID,	0 as PedidoID,	0 as OrdenTrabajoID,	0 as AnalisisMuestraID,	0 as ClienteID,			getdate() EntityDate,	1 Status,	'' as [User] union
select 10000000 + sm.ID,	'SubAnalisisMuestra' ,	sm.ID,			PedidoId,		OrdenTrabajoID,			AnalisisMuestraID,		null,					sm.EntityDate,			sm.Status,	sm.[User]	from SubAnalisisMuestra sm inner join AnalisisMuestra am on am.ID = sm.AnalisisMuestraID inner join OrdenesTrabajo ot on am.OrdenTrabajoID = ot.ID union
select 20000000 + am.ID,	'AnalisisMuestra',		am.ID,			PedidoId,		OrdenTrabajoID,			null,					null,					am.EntityDate,			am.Status,	am.[User]	from AnalisisMuestra am inner join OrdenesTrabajo ot on am.OrdenTrabajoID = ot.ID union
select 30000000 + ID,		'OrdenesTrabajo',		ot.ID,			PedidoId,		null,					null,					null,					ot.EntityDate,			ot.Status,	ot.[User]	from OrdenesTrabajo ot union
select 40000000 + ID,		'Archivos',				ar.ID,			PedidoId,		null,					null,					null,					ar.EntityDate,			ar.Status,	ar.[User]	from Archivos ar union
select 50000000 + ID,		'FechasPedido',			fp.ID,			PedidoId,		null,					null,					null,					fp.EntityDate,			fp.Status,	fp.[User]	from FechasPedido fp union
select 60000000 + ID,		'Muestras',				mu.ID,			PedidoId,		null,					null,					null,					mu.EntityDate,			mu.Status,	mu.[User]	from Muestras mu union
select 70000000 + ID,		'Pedidos',				pe.ID,			pe.ID,			null,					null,					ClienteID,				pe.EntityDate,			pe.Status,	pe.[User]	from Pedidos pe union
select 80000000 + ID,		'EnviosCorreo',			ID,				PedidoId,		null,					null,					null,					EntityDate,				Status,		[User]		from EnviosCorreo union

select 90000000 + ID,		'Clientes',				cl.ID,			null,			null,					null,					cl.ID,					cl.EntityDate,			cl.Status,	cl.[User]	from Clientes cl union
select 100000000 + ID,		'PagosContrato',		pg.ID,			null,			null,					null,					ClienteID,				pg.EntityDate,			pg.Status,	pg.[User]	from PagosContrato pg union

select 110000000 + ID,		'Parametros',			ID,				null,			null,					null,					null,					EntityDate,				Status,		[User]		from Parametros union
select 120000000 + ID,		'Bioquimicos',			ID,				null,			null,					null,					null,					EntityDate,				Status,		[User]		from Bioquimicos union
select 130000000 + ID,		'Areas',				ID,				null,			null,					null,					null,					EntityDate,				Status,		[User]		from Areas union
select 140000000 + ID,		'GruposAnalisis',		ID,				null,			null,					null,					null,					EntityDate,				Status,		[User]		from GruposAnalisis union
select 150000000 + ID,		'Analisis',				ID,				null,			null,					null,					null,					EntityDate,				Status,		[User]		from Analisis union
select 160000000 + ID,		'SubAnalisis',			ID,				null,			null,					null,					null,					EntityDate,				Status,		[User]		from SubAnalisis union
select 170000000 + ID,		'Plantillas',			ID,				null,			null,					null,					null,					EntityDate,				Status,		[User]		from Plantillas union
select 180000000 + ID,		'DetallesPlantilla',	ID,				null,			null,					null,					null,					EntityDate,				Status,		[User]		from DetallesPlantilla 
GO

select Tabla, EntityID,												 EntityDate, Status, [User] from LogDB where Tabla = 'Pedidos' --Tabla
select Tabla, EntityId, PedidoId, OrdenTrabajoID, AnalisisMuestraID, EntityDate, Status, [User] from LogDB where PedidoId = 50 --Pedido
select Tabla, EntityId, ClienteID,								 	 EntityDate, Status, [User] from LogDB where ClienteId = 1 --Cliente


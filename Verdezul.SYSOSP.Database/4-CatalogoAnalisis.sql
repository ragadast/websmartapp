﻿SET IDENTITY_INSERT GruposAnalisis ON; 
SET IDENTITY_INSERT Analisis ON; 
SET IDENTITY_INSERT SubAnalisis ON; 

-- Migración Manual GruposAnalisis y Analisis -Enable Identity insert
-- Migración Manual SubAnalisis 
--(DANNY-IM\SQLEXPRESS)

SET IDENTITY_INSERT GruposAnalisis OFF;
SET IDENTITY_INSERT Analisis OFF;
SET IDENTITY_INSERT SubAnalisis OFF;

update Analisis set TieneSubAnalisis = 1 where ID in (select distinct AnalisisID from SubAnalisis)
GO

select ID, Nombre, AreaID, EntityDate, Status, [User] from GruposAnalisis
select ID, Nombre, Tecnica, Valor, TieneIVA, AcreditacionSAE, Monitoreo, GrupoAnalisisID, EntityDate, Status, [User] from Analisis
select ID, Nombre, AnalisisID, EntityDate, Status, [User] from SubAnalisis
GO

--update User in Analisis
--update User in SubAnalisis

update ga set ga.[User] = b.UserName
--select ga.[User] , b.UserName
from GruposAnalisis ga
	inner join Areas a on ga.AreaID = a.ID
	inner join Bioquimicos b on a.BioquimicoID = b.id

update a set a.[User] = ga.[User]
--select a.[User], ga.[User]
from Analisis a 
	inner join GruposAnalisis ga on a.GrupoAnalisisID = ga.ID

update sa set sa.[User] = a.[User]
--select sa.[User], a.[User]
from SubAnalisis sa
	inner join Analisis a on sa.AnalisisID = a.ID

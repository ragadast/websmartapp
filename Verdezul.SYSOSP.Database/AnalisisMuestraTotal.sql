IF OBJECT_ID('dbo.AnalisisMuestraTotal', 'V') IS NOT NULL
    DROP VIEW dbo.AnalisisMuestraTotal
GO

CREATE VIEW dbo.AnalisisMuestraTotal AS	
	
	select	cl.NumeroIdentificacion, 
			cl.Nombre Cliente, 
			pe.Proforma, 
			pe.Estado,
			case pe.Estado
				when 1 then 'Ingresado'
				when 2 then 'Muestras'
				when 3 then 'Parametrizado'
				when 4 then 'Tiempos'
				when 5 then 'Aprobado'
				when 6 then 'Aceptado'
				when 7 then 'Pagado'
				when 8 then 'Entregado'
				when 9 then 'Cerrado'
			end EstadoNombre,
			pe.InformacionPago,
			pe.DocumentoRecaudacion,
			case pe.DocumentoRecaudacion
				when 1 then 'Recibo de Recaudaci�n'
				when 2 then 'Comprobante de Dep�sito'
				when 3 then 'Recibo de Transferencia'
			end DocumentoRecaudacionNombre,
			pe.FormaPago,
			case pe.FormaPago
				when 1 then 'Efectivo'
				when 2 then 'Pago en Ventanilla'
				when 3 then 'Tarjeta de Cr�dito o D�bito'
				when 4 then 'Cheque Certificado'
				when 5 then 'Transferencia Sector Privado'
				when 6 then 'Transferencia Sector P�blico'
				when 7 then 'Contrato'
				when 8 then 'Certificaci�n presupuestaria'
			end FormaPagoNombre,
			pe.FechaRecaudacion,
			fp.EntityDate FechaPago,
			case when fp.ID is not null then 1 else 0 end Pagado,
			ar.Nombre Area,
			ot.Numero OrdenTrabajo,
			an.Nombre Analisis,
			am.Valor * am.Cantidad 'Subtotal',
			case am.TieneIVA when 1 then am.Valor * pe.IVA else 0 end * am.Cantidad 'IVA',
			(case am.TieneIVA when 1 then am.Valor * pe.IVA else 0 end + am.Valor) * am.Cantidad 'Total'
	from AnalisisMuestra am
	inner join Analisis an on am.AnalisisID = an.ID
	inner join OrdenesTrabajo ot on am.OrdenTrabajoID = ot.ID
	inner join Areas ar on ot.AreaID = ar.ID
	inner join Pedidos pe on ot.PedidoID = pe.ID
	inner join Clientes cl on pe.ClienteID = cl.ID
	left outer join FechasPedido fp on fp.PedidoID = pe.ID and fp.Estado = 7
GO

select * from AnalisisMuestraTotal
GO

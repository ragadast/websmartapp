Declare @fecha datetime
set @fecha = GETDATE()

-- Bioqu�micos
SET IDENTITY_INSERT Bioquimicos ON; 
INSERT INTO Bioquimicos (ID, UserName,EntityDate,Status,[User])     VALUES
(1, 'mggarofalo', @fecha, 1, 'jairog'),
(2, 'abcepa', @fecha, 1, 'jairog'),
(3, 'lmchasi', @fecha, 1, 'jairog'),
(4, 'dcroldan', @fecha, 1, 'jairog'),
(5, 'raacosta', @fecha, 1, 'jairog')
SET IDENTITY_INSERT Bioquimicos OFF; 
--select * from Bioquimicos

-- Areas
SET IDENTITY_INSERT Areas ON; 
insert into Areas (ID, Nombre, BioquimicoID, EntityDate, Status, [User]) values 
(1, 'ALIMENTOS', 1, @fecha, 1, 'jairog'),
(2, 'AMBIENTAL', 2, @fecha, 1, 'jairog'),
(3, 'MICROBIOLOG�A', 3, @fecha, 1, 'jairog'),
(4, 'QU�MICA FARMAC�UTICA', 4, @fecha, 1, 'jairog'),
(5, 'CL�NICOS', 5, @fecha, 1, 'jairog')
SET IDENTITY_INSERT Areas OFF; 
--select * from Areas
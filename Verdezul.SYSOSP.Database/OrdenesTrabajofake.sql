select * from Pedidos where ID = 10
select * from Muestras where PedidoID = 10
select * from AnalisisMuestra where MuestraID in (select ID from Muestras where PedidoID = 10)

--update AnalisisMuestra set Tecnica = (select Tecnica from Analisis a where a.ID = AnalisisID), Valor = (select Valor from Analisis a where a.ID = AnalisisID)

-- Generando Ordenes de Trabajo fake
select LaboratorioID, GrupoAnalisisID, count(*) 
from Analisis a
inner join GruposAnalisis ga on a.GrupoAnalisisID = ga.ID
group by LaboratorioID, GrupoAnalisisID

select * from GruposAnalisis where Id in (5, 14, 27)

Insert into OrdenesTrabajo values
('Observacion', 0, 'NombreArchivo', NEWID(),  1, 1, 3, getdate(), 1, 'fake'),
('Observacion', 0, 'NombreArchivo', NEWID(),  2, 1, 3, getdate(), 1, 'fake'),
('Observacion', 0, 'NombreArchivo', NEWID(),  3, 1, 3, getdate(), 1, 'fake')

select * from OrdenesTrabajo

insert into AnalisisMuestra
select a.Id AnalisisID, m.Id MuestraID, ot.ID, 3, a.Tecnica, a.Valor, getdate(), 1, 'fake'
from Muestras m
cross join OrdenesTrabajo ot
inner join Laboratorios l on l.id = ot.LaboratorioID
inner join GruposAnalisis ga on ga.LaboratorioID = l.ID
inner join Analisis a on a.GrupoAnalisisID = ga.ID
where m.PedidoID = 3
and ot.PedidoID = 3
and ga.ID in (5, 14, 27)

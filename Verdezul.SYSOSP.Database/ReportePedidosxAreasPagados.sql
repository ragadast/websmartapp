--select sum(Subtotal) Subtotal, sum(IVA) IVA, sum(Total) Total, count(1) Numero from (
	select 
	NumeroIdentificacion,
	Cliente,
	Proforma,
	Area,
	InformacionPago Informaci�n,
	DocumentoRecaudacionNombre Documento,
	FormaPagoNombre Forma,
	FechaPago,
	datepart(month, FechaPago) 'Mes Pago',
	datepart(year, FechaPago) 'A�o Pago',
	SUM(Subtotal) 'Subtotal',
	SUM(IVA) 'IVA',
	SUM(Total) 'Total'
	from AnalisisMuestraTotal
	where Estado in (7, 8, 9)
	and Pagado = 1
	--and datepart(year, FechaPago)  = 2022
	--and datepart(month, FechaPago) = 1
	group by 
	NumeroIdentificacion,
	Cliente,
	Proforma,
	Area,
	InformacionPago,
	DocumentoRecaudacionNombre,
	FormaPagoNombre,
	FechaPago
order by 2,3,4
--) P

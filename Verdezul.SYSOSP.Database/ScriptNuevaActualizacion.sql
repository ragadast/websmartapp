USE SYSOSP
GO

-- PREVIO A INICIAR LA MIGRACION
-- Posible problema
update Muestras set CantidadRequerida = left(CantidadRequerida, 150) where len(CantidadRequerida) > 150
GO


-- LUEGO DE FINALIZAR LA MIGRACION
-- Actualiza la Cantidad de Informes de Ordenes Antiguas
update OrdenesTrabajo 
set CantidadInformes = 1 
where CantidadInformes = 0

-- Actualiza los Informes de los creados a 1
update Archivos
set NumeroInforme = 1
where Tipo = 6
and NumeroInforme is null
GO

--OJO: Es necesario actualizar los nombres de los archivos
-- Se debe usar la pantalla de la Aplicación Verdezul

-- Crea el espacio de archivos suplementarios basando en los archivos de Informe de Resultados
insert into Archivos (
PedidoID,	Codigo,		Tipo,	Subido,	Extension,	EntityDate,	[Status],	[User],		IdOrdenTrabajo,	IdMuestra,	Fecha,		NumeroInforme, NecesitaSuplementario
) 
select
PedidoID,	NEWID(),	7,		0,		null,		getdate(),	[Status],	'update',	IdOrdenTrabajo,	IdMuestra,	getdate(),	NumeroInforme, 0
from Archivos where Tipo = 6
GO

-- Se necesita para quitar las fechas en archivos no subidos
update Archivos
set Fecha = null
where Tipo in ( 7 ) and Extension is null
GO


-- Actualización de texto de FechaVencimiento en Muestras
update Muestras 
set FechaVencimiento = replace(replace(replace(replace(replace(left(FechaVencimiento, 11), 'Jan', 'Ene') , 'Apr', 'Abr'), 'Aug', 'Ago'), 'Dec', 'Dic'), '  ', ' ')
where FechaVencimiento is not null
GO

-- Actualización de texto de FechaElaboracion en Muestras
update Muestras 
set FechaElaboracion = replace(replace(replace(replace(replace(left(FechaElaboracion, 11), 'Jan', 'Ene') , 'Apr', 'Abr'), 'Aug', 'Ago'), 'Dec', 'Dic'), '  ', ' ')
where FechaElaboracion is not null
GO

-- Actualizacion de Envios de Correo con los nuevos enumerados:
-- Anteriormente los tipos de notificaciones eran:
-- 2-InformeResultadosParcia, 3-InformeResultadosFinal
-- Ahora el cambio funciona diferente:
-- 2-InformeResultados, 3-InformeResultadosSuplementario
-- Solución previa a paso a Producción
update EnviosCorreo set TipoNotificacion = 2 where TipoNotificacion = 3

-- Luego se puede revisar los datos con el siguiente script de actualizacion
update ec
set ArchivoID = (select top 1 ID from Archivos a where Tipo in (6) and a.Subido = 1 and a.PedidoID = ec.PedidoID) 
from EnviosCorreo ec
where ec.TipoNotificacion = 2

﻿Declare @fecha datetime
set @fecha = GETDATE()

INSERT INTO [Parametros] ([Tipo],[Valor],[TipoDato],[EntityDate],[Status],[User]) VALUES
(1,'Facultad de Ciencias Químicas',1,@fecha,1,'seed'),
(2,'1768092050001',1,@fecha,1,'seed'),
(3,'Ing. Juan Casares',1,@fecha,1,'seed'),
(4,'Francisco Viteri s/n Y Gilberto Gatto Soral',1,@fecha,1,'seed'),
(5,'3214661',1,@fecha,1,'seed'),
(6,'fcq.osp@uce.edu.ec',5,@fecha,1,'seed'),
(7,'12',2,@fecha,1,'seed'),
(8,'15',2,@fecha,1,'seed'),
(9,'https://goo.gl/maps/hCEdLE697Jwz9QzS8',6,@fecha,1,'seed'),
(10,'https://forms.office.com/Pages/ResponsePage.aspx?id=xyz',6,@fecha,1,'seed')
GO


